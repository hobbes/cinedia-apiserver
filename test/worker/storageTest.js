'use strict';

var Promise = require('bluebird'),
    fs = require('fs-extra'),
    path = require('path'),
    util = require('util'),
    config = require('config'),
    emptyDir = Promise.promisify(fs.emptyDir),
    mkdirs = Promise.promisify(fs.mkdirs),
    testFileDir = './test/worker/inputFiles',
    tmpDir = config.get('tmpDir'),
    storage = require('../../lib/worker/storage');

describe('storage', function () {
    this.timeout(720000);
    beforeEach(function () {
        return emptyDir(tmpDir);
    });
    describe('upload', function() {
        it('should upload', function() {
            var inputFile = path.join(testFileDir, 'image.jpg');
                // outputFile = path.join(tmpDir, 'vidSoundConverted.mp4');

            return storage.upload(inputFile, 'testuploadfile')
                .then(console.log);
        });
        it('should upload with ContentDisposition set', function() {
            var inputFile = path.join(testFileDir, 'image.jpg');

            return storage.upload(inputFile, 'testuploadfile', 'filen.zip')
                .then(console.log);
        });
    });
    describe('download', function() {
        it('should download', function() {
            var remoteFile = 'image.jpg',
                tmpLocalFile = path.join(tmpDir, 'downloaded.jpg');

            return storage.download(remoteFile, tmpLocalFile)
                .then(console.log);
        });
    });
});