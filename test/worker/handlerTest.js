'use strict';

var Promise = require('bluebird'),
  fs = require('fs-extra'),
  config = require('config'),
  emptyDir = Promise.promisify(fs.emptyDir),
  mkdirs = Promise.promisify(fs.mkdirs),
  storage = require('../../lib/worker/storage'),
  handlers = require('../../lib/worker/handlers'),
  tmpDir = config.get('tmpDir'),
  SUJET_INTERMEDIATE_DIA = 'sujet.intermediate.dia',
  SUJET_INTERMEDIATE_SPOT_MUTE = 'sujet.intermediate.spotMute',
  SUJET_INTERMEDIATE_SPOT_SOUND = 'sujet.intermediate.spotSound',
  BLOCK_DCP = 'block.dcp';

describe('cineworker', function () {
  beforeEach(function () {
    return emptyDir(tmpDir);
  });
  describe('intermediateDia()', function () {
    this.timeout(12000);
    it('should succeed', function() {
      return handlers[SUJET_INTERMEDIATE_DIA]({
        source: 'image.jpg',
        duration: 2
      }, function() {});
    });

    it('should reject with ValidationError', function() {
      return handlers[SUJET_INTERMEDIATE_DIA]({
        source: 'grid.gif',
        duration: 3
      }, function() {})
        .catch(console.log);
    });

    it('should reject with ', function() {
      return handlers[SUJET_INTERMEDIATE_DIA]({
        source: 'noImage.txt',
        duration: 3
      }, function() {})
        .catch(console.log);
    });
  });
  describe('intermediateSpotMute()', function () {
    this.timeout(720000);
    it('should', function() {
      return handlers[SUJET_INTERMEDIATE_SPOT_MUTE]({
        source: 'videoMute.mov'
      }, function() {});
    });
  });
  describe('intermediateSpotSound()', function () {
    this.timeout(720000);
    it('should succeed', function() {
      return handlers[SUJET_INTERMEDIATE_SPOT_SOUND]({
        source: 'videoSound.mov'
      }, function() {}).catch(console.log);
    });

    it('should reject with missing sound', function() {
      return handlers[SUJET_INTERMEDIATE_SPOT_SOUND]({
        source: 'videoMute.mov'
      }, function() {})
        .catch(console.log);
    });

    it('should reject with SourceValidationError', function() {
      return handlers[SUJET_INTERMEDIATE_SPOT_SOUND]({
        source: 'noImage.txt'
      }, function() {})
        .catch(console.log);
    });
  });
  describe('block()', function () {
    this.timeout(720000);
    it('should', function() {
      return handlers[BLOCK_DCP]({
        blockId: 'KASJDFKAJSHD',
        blockhash: 'sdfnsbdlfksnfvsdjf',
        issuer: 'cinediaissuername',
        spaceFrames: 24,
        fps: 25,
        name: 'blockname',
        segments: {
          mute: {
            keys: [
              '01.tar',
              '02.tar'
              // '03.tar'
            ],
            name: 'mutename'
          },
          audio: {  
            keys: [
              'ba61be56ad81484e99eb1616fd12ca4c.tar',
              'd5aa04f70e424fcf8a0e13e722a4b443.tar',
              'd265301194d0432cb0a615f02e455a44.tar'
            ],
            name: 'soundname'
          } 
        }
      }, function() {}).catch(console.log);
    });
  });
});