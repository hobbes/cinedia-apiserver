'use strict';

var Promise = require('bluebird'),
  fs = require('fs-extra'),
  path = require('path'),
  util = require('util'),
  config = require('config'),
  emptyDir = Promise.promisify(fs.emptyDir),
  mkdirs = Promise.promisify(fs.mkdirs),
  storage = require('../../lib/worker/storage'),
  testFileDir = './test/worker/inputFiles',
  tmpDir = config.get('tmpDir'),
  tools = require('../../lib/worker/tools');

describe('tools', function () {
  this.timeout(720000);
  beforeEach(function () {
    return emptyDir(tmpDir);
  });
  describe('generateMP4Preview', function() {
    it('should generate preview of inputFile with sound', function() {
      var inputFile = path.join(testFileDir, 'vidSound.mov'),
        outputFile = path.join(tmpDir, 'vidSoundConverted.mp4');

      return tools.generateMP4Preview(inputFile, outputFile, 999, 540, true, {
        input_i: '-17.83',
        input_tp: '-0.01',
        input_lra: '10.60',
        input_thresh: '-27.83',
        output_i: '-15.69',
        output_tp: '-1.50',
        output_lra: '3.40',
        output_thresh: '-25.82',
        normalization_type: 'dynamic',
        target_offset: '-0.31'
      });
    });
    it('should generate preview of inputFile without sound', function() {
      var inputFile = path.join(testFileDir, 'vidMute.mov'),
        outputFile = path.join(tmpDir, 'vidMuteConverted.mp4');

      return tools.generateMP4Preview(inputFile, outputFile, 999, 540, false);
    });
  });
  describe('validateVideo', function () {
    it('should', function() {
      var inputFile = path.join(testFileDir, 'vidSound.mov');

      return tools.validateVideo(inputFile, true, [24, 25], 95)
        .then(function (result) {
            console.log(util.inspect(result, {
            depth: null
          }));
        });
    });
  });
  describe('ffmpegExtractWav', function () {
    it('should', function() {
      var inputFile = path.join(testFileDir, 'vidSound.mov'),
        outputFile = path.join(tmpDir, 'extractedaudio.wav');

      return tools.ffmpegExtractWav(inputFile, outputFile, 2, 144000, null, {
        input_i: '-17.83',
        input_tp: '-0.01',
        input_lra: '10.60',
        input_thresh: '-27.83',
        output_i: '-15.69',
        output_tp: '-1.50',
        output_lra: '3.40',
        output_thresh: '-25.82',
        normalization_type: 'dynamic',
        target_offset: '-0.31'
      }).then(function (result) {
          console.log(util.inspect(result, {
          depth: null
        }));
      });
    });
  });
});