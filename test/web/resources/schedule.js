'use strict';

var util = require('util'),
    chai = require('chai'),
    expect = chai.expect,
    scheduleResource = require('../../lib/resource/schedule');

describe('resource schedule', function () {
    describe('schedule collection', function () {
        it('should create a schedule collection resource from modeldata', function () {
            var data = JSON.parse('[{"startdate":"2016-08-31T22:00:00.000Z","enddate":"2016-09-06T22:00:00.000Z","stageName":"Megacity 1","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"E5V4LZ48GL","dateslotId":"7KJ4YQE4DB"},{"startdate":"2016-09-07T22:00:00.000Z","enddate":"2016-09-13T22:00:00.000Z","stageName":"Megacity 1","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"E5V4LZ48GL","dateslotId":"E5V4LRZ48G"},{"startdate":"2016-09-14T22:00:00.000Z","enddate":"2016-09-20T22:00:00.000Z","stageName":"Megacity 1","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"E5V4LZ48GL","dateslotId":"V5LADZRADN"},{"startdate":"2016-08-31T22:00:00.000Z","enddate":"2016-09-06T22:00:00.000Z","stageName":"Megacity 2","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"V5LADRADN9","dateslotId":"7KJ4YQE4DB"},{"startdate":"2016-09-07T22:00:00.000Z","enddate":"2016-09-13T22:00:00.000Z","stageName":"Megacity 2","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"V5LADRADN9","dateslotId":"E5V4LRZ48G"},{"startdate":"2016-09-14T22:00:00.000Z","enddate":"2016-09-20T22:00:00.000Z","stageName":"Megacity 2","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"V5LADRADN9","dateslotId":"V5LADZRADN"},{"startdate":"2016-09-14T22:00:00.000Z","enddate":"2016-09-20T22:00:00.000Z","stageName":"Megacity 3","locationName":"Major Cinema Megacity","companyName":"Major Cinema","sujetId":"E5V4LZ48GL","locationId":"E5V4LZ48GL","stageId":"QNGAEZ4XLV","dateslotId":"V5LADZRADN"}]');

            console.log(util.inspect(scheduleResource.buildScheduleCollection({
                scheduleCollection: data
            }), {
                depth: 5
            }));
        });
    });
});
