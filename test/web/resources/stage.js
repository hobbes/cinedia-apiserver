'use strict';

var util = require('util'),
    chai = require('chai'),
    expect = chai.expect,
    stageResource = require('../../lib/resource/stage');

describe('resource stage', function () {
    describe('stage collection', function () {
        it('should create a stage collection resource from modeldata', function () {
            var data = JSON.parse('[{"stageId":"E5V4LZ48GL","stageName":"Megacity 1","stageNameShort":"MEC1","locationId":"E5V4LZ48GL","locationName":"Major Cinema Megacity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72},{"stageId":"V5LADRADN9","stageName":"Megacity 2","stageNameShort":"MEC2","locationId":"E5V4LZ48GL","locationName":"Major Cinema Megacity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72},{"stageId":"QNGAEZ4XLV","stageName":"Megacity 3","stageNameShort":"MEC3","locationId":"E5V4LZ48GL","locationName":"Major Cinema Megacity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72},{"stageId":"8VX4954N6K","stageName":"Megacity 4","stageNameShort":"MEC4","locationId":"E5V4LZ48GL","locationName":"Major Cinema Megacity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72},{"stageId":"GQLAGJ4V6B","stageName":"Minicity 1","stageNameShort":"MIC1","locationId":"V5LADRADN9","locationName":"Major Cinema Minicity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72},{"stageId":"7WJAR8A96Z","stageName":"Minicity 2","stageNameShort":"MIC2","locationId":"V5LADRADN9","locationName":"Major Cinema Minicity","companyId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","lockoffset":72}]');

            console.log(util.inspect(stageResource.buildStageCollection({
                stageCollection: data
            }), {
                depth: 5
            }));
        });
    });
});
