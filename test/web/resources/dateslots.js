'use strict';

var chai = require('chai'),
    expect = chai.expect,
    dateslotResource = require('../../lib/resource/dateslot');

describe('resource dateslot', function () {
    describe('dateslot item', function () {
        it('should create a dateslot item resource from modeldata', function () {
            var data = JSON.parse('{"startdate":"2017-01-04T23:00:00.000Z","enddate":"2017-01-10T23:00:00.000Z","kw":2,"dateslotId":"7WJARGR496"}');

            console.log(dateslotResource.buildDateslotItem({
                dateslotItem: data
            }));
        });
    });
    describe('dateslot collection', function () {
        it('should create a dateslot collection resource from modeldata', function () {
            var data = JSON.parse('[{"startdate":"2017-01-04T23:00:00.000Z","enddate":"2017-01-10T23:00:00.000Z","kw":2,"dateslotId":"7WJARGR496"},{"startdate":"2017-01-11T23:00:00.000Z","enddate":"2017-01-17T23:00:00.000Z","kw":3,"dateslotId":"JMWAWZK4LK"},{"startdate":"2017-01-18T23:00:00.000Z","enddate":"2017-01-24T23:00:00.000Z","kw":4,"dateslotId":"DRE47NQAK8"},{"startdate":"2017-01-25T23:00:00.000Z","enddate":"2017-01-31T23:00:00.000Z","kw":5,"dateslotId":"KR74NQNAQ9"},{"startdate":"2017-02-01T23:00:00.000Z","enddate":"2017-02-07T23:00:00.000Z","kw":6,"dateslotId":"8B5AVNXAYZ"},{"startdate":"2017-02-08T23:00:00.000Z","enddate":"2017-02-14T23:00:00.000Z","kw":7,"dateslotId":"MWX4J8E4VL"},{"startdate":"2017-02-15T23:00:00.000Z","enddate":"2017-02-21T23:00:00.000Z","kw":8,"dateslotId":"6Z54KK84QK"},{"startdate":"2017-02-22T23:00:00.000Z","enddate":"2017-02-28T23:00:00.000Z","kw":9,"dateslotId":"JVQAMZQ4E8"},{"startdate":"2017-03-01T23:00:00.000Z","enddate":"2017-03-07T23:00:00.000Z","kw":10,"dateslotId":"BMVA8V5AWK"},{"startdate":"2017-03-08T23:00:00.000Z","enddate":"2017-03-14T23:00:00.000Z","kw":11,"dateslotId":"DW64Z664ZR"}]');

            console.log(dateslotResource.buildDateslotCollection({
                dateslotCollection: data
            }));
        });
    });
});
