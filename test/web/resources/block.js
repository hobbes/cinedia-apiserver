'use strict';

var util = require('util'),
    chai = require('chai'),
    expect = chai.expect,
    blockResource = require('../../lib/resource/block');

describe('resource block', function () {
    describe('block collection', function () {
        it('should create a block collection resource from modeldata', function () {
            var data = JSON.parse('[{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"ddc0c5e8420874b63ff26915215ef077","blockId":"1.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"E5V4LZ48GL","stageName":"Megacity 1","stageNameShort":"MEC1","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"eb67fd965138a324a0104e3560ee7777","blockId":"2.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"V5LADRADN9","stageName":"Megacity 2","stageNameShort":"MEC2","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"9386c4a64e02f573ce3c2bd1cf26eb98","blockId":"3.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"QNGAEZ4XLV","stageName":"Megacity 3","stageNameShort":"MEC3","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"ac23ea051c0664197643be726c014880","blockId":"4.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"8VX4954N6K","stageName":"Megacity 4","stageNameShort":"MEC4","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"f5a32cc90aec3c6cf3ca487d855e980e","blockId":"5.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"GQLAGJ4V6B","stageName":"Minicity 1","stageNameShort":"MIC1","stageLockoffsethours":72,"locationName":"Major Cinema Minicity","locationId":"V5LADRADN9","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"337d8721f6863b402ba5ac3d9e62cacf","blockId":"6.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"7WJAR8A96Z","stageName":"Minicity 2","stageNameShort":"MIC2","stageLockoffsethours":72,"locationName":"Major Cinema Minicity","locationId":"V5LADRADN9","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false}]');

            console.log(util.inspect(blockResource.buildBlockCollection({
                blockCollection: data,
                user: {
                    customer: {
                        type: 'cinema'
                    }
                }
            }), {
                depth: 5
            }));
        });
    });
    describe('block item', function () {
        it('should create a block collection resource from modeldata', function () {
            // var data = JSON.parse('{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"ddc0c5e8420874b63ff26915215ef077","blockId":"1.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"E5V4LZ48GL","stageName":"Megacity 1","stageNameShort":"MEC1","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false},{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"now":"2017-08-19T22:00:00.000Z","blockhash":"eb67fd965138a324a0104e3560ee7777","blockId":"2.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"V5LADRADN9","stageName":"Megacity 2","stageNameShort":"MEC2","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false}');
            var data = JSON.parse('{"kw":37,"startdate":"2017-09-06T22:00:00.000Z","enddate":"2017-09-12T22:00:00.000Z","stage_fps":25,"blockhash":"ddc0c5e8420874b63ff26915215ef077","now":"2017-08-19T22:00:00.000Z","blockId":"1.141","dateslotId":"J69A5654B7","dcpStatus":null,"dcpBlockhash":null,"dcpStoragekey":null,"stageId":"E5V4LZ48GL","stageName":"Megacity 1","stageNameShort":"MEC1","stageLockoffsethours":72,"locationName":"Major Cinema Megacity","locationId":"E5V4LZ48GL","companyName":"Major Cinema","companyNameShort":"MJRC","companyId":"E5V4LZ48GL","locked":false,"customers":{"E5V4LZ48GL":{"name":"Major Cinema Company","nameshort":"MCC","type":"cinema","customerId":"E5V4LZ48GL"},"GQLAGJ4V6B":{"name":"Cinema Advertiser","nameshort":"CNA","type":"advertiser","customerId":"GQLAGJ4V6B"}},"segments":[{"customerId":"E5V4LZ48GL","audio":false,"sujets":[{"position":1,"segment_position":1,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","sujetId":"E5V4LZ48GL","sujetName":"sujetname1","sujetDuration":7,"sujetType":"dia"},{"position":2,"segment_position":1,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","sujetId":"V5LADRADN9","sujetName":"sujetname2","sujetDuration":7,"sujetType":"dia"},{"position":3,"segment_position":1,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","sujetId":"QNGAEZ4XLV","sujetName":"sujetname3","sujetDuration":7,"sujetType":"dia"},{"position":4,"segment_position":1,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","sujetId":"8VX4954N6K","sujetName":"sujetname4","sujetDuration":11,"sujetType":"spot_mute"}]},{"customerId":"GQLAGJ4V6B","audio":false,"sujets":[{"position":1,"segment_position":2,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"GQLAGJ4V6B","sujetId":"E5V4LZZ48G","sujetName":"sujetname23","sujetDuration":7,"sujetType":"dia"},{"position":2,"segment_position":2,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"GQLAGJ4V6B","sujetId":"QNGAEPZ4XL","sujetName":"sujetname25","sujetDuration":10,"sujetType":"spot_mute"}]},{"customerId":"E5V4LZ48GL","audio":true,"sujets":[{"position":5,"segment_position":4,"dateslotId":"J69A5654B7","stageId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","sujetId":"GQLAGJ4V6B","sujetName":"sujetname5","sujetDuration":20,"sujetType":"spot_sound"}]}]}')
            console.log(util.inspect(blockResource.buildBlockItem({
                blockItem: data,
                user: {
                    customer: {
                        type: 'cinema'
                    }
                }
            }), {
                depth: 5
            }));
        });
    });
});
