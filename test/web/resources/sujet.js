'use strict';

var util = require('util'),
    chai = require('chai'),
    expect = chai.expect,
    sujetResource = require('../../lib/resource/sujet');

describe('resource sujet', function () {
    describe('sujet collection', function () {
        it('should create a sujet collection resource from modeldata', function () {
            var data = JSON.parse('[{"archived":false,"name":"one","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage": "new error message","sujetId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":true,"ready":false},{"archived":false,"name":"two","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage":null,"sujetId":"V5LADRADN9","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":false,"ready":false},{"archived":false,"name":"three","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage":null,"sujetId":"QNGAEZ4XLV","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":false,"ready":false},{"archived":false,"name":"four","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage":null,"sujetId":"8VX4954N6K","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":false,"ready":false},{"archived":false,"name":"five","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage":null,"sujetId":"GQLAGJ4V6B","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":false,"ready":false}]');

            console.log(util.inspect(sujetResource.buildSujetCollection({
                sujetCollection: data
            }), {
                depth: 5
            }));
        });
    });
    describe('sujet item', function () {
        it('should create a sujet item resource from modeldata', function () {
            var data = JSON.parse('{"archived":false,"name":"one","type":"dia","duration":7,"storagekey_intermediatefile":null,"errormessage":null,"sujetId":"E5V4LZ48GL","customerId":"E5V4LZ48GL","createdAt":"2017-08-13T09:41:01.772Z","updatedAt":"2017-08-13T09:41:01.772Z","version":"4PY6DBPWYA","error":false,"ready":false}');

            console.log(util.inspect(sujetResource.buildSujetItem({
                sujetItem: data
            }), {
                depth: 5
            }));
        });
    });
});
