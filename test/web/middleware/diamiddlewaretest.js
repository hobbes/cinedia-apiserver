// 'use strict';

// var Promise = require('bluebird'),
//     express = require('express'),
//     proxyquire = require("proxyquire"),
//     request = require('supertest'),
//     chai = require('chai'),
//     chaiAsPromised = require('chai-as-promised'),
//     diaMiddleware = require('../../lib/middleware/sujet'),
//     queryMiddleware = require('../../lib/middleware/query');

// chai.use(chaiAsPromised);
// chai.should();

// describe('middleware dia', function () {
//     describe('ensureMinOneFile', function () {
//         it('should call next', function (done) {
//             var app = express();
//             app.use(function (req, res, next) {
//                 req.files = [{}, {}];
//                 next();
//             });
//             app.use(diaMiddleware.mid.ensureMinOneFile);
//             app.use(function (req, res, next) {
//                 res.end();
//             });

//             request(app)
//                 .get('/')
//                 .expect(200)
//                 .end(function (err, res) {
//                     done(err);
//                 });
//         });
//         it('should call next with err if no req.files and req.files.length < 1', function (done) {
//             var app = express();
//             app.use(function (req, res, next) {
//                 // req.files = [];
//                 next();
//             });
//             app.use(diaMiddleware.mid.ensureMinOneFile);
//             app.use(function (err, req, res, next) {
//                 err.should.be.instanceof(Error);
//                 err.message.should.equal('no files');
//                 res.end();
//             });

//             request(app)
//                 .get('/')
//                 .expect(200)
//                 .end(function (err, res) {
//                     done(err);
//                 });
//         });
//     });
//     describe.skip('loadAll', function () {
//         var diaCtrlStub = {},
//             diaMiddleware = proxyquire('../../lib/middleware/dia', {
//                 '../../controllers/dia': diaCtrlStub
//             });
//         it('should add the loaded dias to req.dias', function (done) {
//             var app = express();

//             diaCtrlStub.readAllFromUser = function (userid, options) {
//                 return Promise.resolve([{
//                     id: 1,
//                     link: {}
//                 }, {
//                     id: 2,
//                     link: {}
//                 }]);
//             };

//             app.use(function (req, res, next) {
//                 req.user = {
//                     id: 1
//                 };
//                 req.renderdata = {};
//                 next();
//             });
//             app.use(queryMiddleware.sanitize);
//             app.use(diaMiddleware.mid.loadAll);
//             app.use(function (req, res) {
//                 req.dias.should.have.length(2);
//                 res.end();
//             });
//             request(app)
//                 .get('/')
//                 .expect(200)
//                 .end(function (err, res) {
//                     done(err);
//                 });
//         });
//         it('should set the options', function (done) {
//             var app = express();

//             diaCtrlStub.readAllFromUser = function (userid, options) {
//                 options.should.eql({
//                     limit: 5,
//                     offset: 5,
//                     sort: ['updated_at', 'asc']
//                 });
//                 return Promise.resolve([]);
//             };

//             app.use(function (req, res, next) {
//                 req.renderdata = {};
//                 req.query.per_page = 5;
//                 req.query.page = 1;
//                 req.query.sort = 'updated';
//                 req.user = {
//                     id: 1
//                 };
//                 next();
//             });
//             app.use(queryMiddleware.sanitize);
//             app.use(diaMiddleware.mid.loadAll);
//             app.use(function (req, res) {
//                 res.end();
//             });

//             request(app)
//                 .get('/')
//                 .expect(200)
//                 .end(function (err, res) {
//                     done(err);
//                 });
//         });
//     });
//     describe.skip('loadOne', function () {
//         var diaCtrlStub = {},
//             diaMiddleware = proxyquire('../../lib/middleware/dia', {
//                 '../../controllers/dia': diaCtrlStub
//             });
//         it('should add the loaded dia to req.dia', function (done) {
//             var app = express();

//             diaCtrlStub.readOneFromUser = function (userid, diaid) {
//                 console.log('stub')
//                 return Promise.resolve({
//                     id: 1,
//                     link: {}
//                 });
//             };

//             app.param('diaid', diaMiddleware.param.loadOne);
//             app.use(function (req, res, next) {
//                 req.renderdata = {};
//                 req.user = {
//                     id: 1
//                 };
//                 next();
//             });
//             app.get('/dias/:diaid/');
//             // app.use(function (req, res) {
//             //     console.log(req.dia)
//             //         // req.dia.should.eql({
//             //         //     id: 1
//             //         // });
//             //     res.end();
//             // });
//             // app.use(function (err, req, res, next) {
//             //     // console.log(err.stack)
//             //     next()
//             // })
//             request(app)
//                 .get('/dias/khkjg')
//                 .expect(200)
//                 .end(function (err, res) {
//                     done(err);
//                 });
//         });
//     });

//     // describe('make', function(){
//     //     describe('newFromSource', function () {
//     //         var diaCtrlStub = {},
//     //             diaMiddleware = proxyquire('../../lib/routes/dia', {
//     //                 '../../controllers/dia': diaCtrlStub
//     //             });
//     //         it('should add the loaded dias to req.dias', function (done) {
//     //             var app = express();

//     //             diaCtrlStub.createFromSourceFile = function (files, userid) {
//     //                 return Promise.resolve([1, 2]);
//     //             };

//     //             app.use(function (req, res, next) {
//     //                 req.user = {
//     //                     id: 1
//     //                 };
//     //                 next();
//     //             });
//     //             app.use(diaMiddleware.mid.loadAll);
//     //             app.use(function (req, res) {
//     //                 req.dias.should.have.length(2);
//     //                 req.dias.should.eql([{
//     //                     id: 1
//     //                 }, {
//     //                     id: 2
//     //                 }]);
//     //                 res.end();
//     //             });

//     //             request(app)
//     //                 .get('/')
//     //                 .expect(200)
//     //                 .end(function (err, res) {
//     //                     done(err);
//     //                 });
//     //         });
//     //     });
//     // });
// });
