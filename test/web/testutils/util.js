'use strict';

var Promise = require('bluebird'),
    path = require('path'),
    QueryFile = require('pg-promise').QueryFile,
    resetDbSql = QueryFile(path.join(__dirname, 'resetDb.sql')),
    db = require('../../lib/db'),
    fs = require('fs-extra'),
    remove = Promise.promisify(fs.remove),
    mkdirs = Promise.promisify(fs.mkdirs);

exports.cleanUpTmpFolder = function (path) {
    return remove(path)
        .then(function () {
            return mkdirs(path);
        });
};

exports.resetDb = function () {
    return db.none(resetDbSql);
};