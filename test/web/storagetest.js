// 'use strict';

// var Promise = require('bluebird'),
//     chai = require('chai'),
//     chaiAsPromised = require('chai-as-promised'),
//     path = require('path'),
//     fs = require('fs'),
//     cleanTmpfolder = require('./testutils/util').cleanTmpfolder,
//     storage = require('../lib/storage');

// chai.use(chaiAsPromised);
// chai.should();

// describe('slow storage', function () {
//     describe('getFileStream()', function () {
//         var key = 'seeroseremotekey.jpg';

//         before(cleanTmpfolder);
//         before(function () {
//             var data = {
//                 localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                 remoteFilekey: key,
//                 metadata: {
//                     user: 'user'
//                 },
//                 headers: {}
//             };

//             return storage.uploadFile(data);
//         });
//         it('should resolve with a filestream', function (done) {
//             this.timeout(5000);
//             var writer = fs.createWriteStream(path.join(__dirname, 'tmp/seerose.jpg'));
//             writer.on('finish', function () {
//                 done();
//             });
//             storage.getFileStream(key)
//                 .then(function (res) {
//                     res.pipe(writer);
//                 });
//         });
//         it('should reject if key not found', function () {
//             return storage.getFileStream('imAmSpecial')
//                 .should.eventually.be.rejectedWith(Error, "404");
//         });
//     });
//     describe('getFileHeader()', function () {
//         var key = 'seeroseremotekey.jpg';

//         before(cleanTmpfolder);
//         before(function () {
//             this.timeout(5000);

//             var data = {
//                 localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                 remoteFilekey: key,
//                 metadata: {
//                     user: 'user'
//                 },
//                 headers: {}
//             };

//             return storage.uploadFile(data);
//         });
//         it('should resolve with file head', function () {
//             this.timeout(5000);
//             return storage.getFileHeader(key)
//                 .should.eventually.have.property('etag', '"ab371e22f7c9298f68da65a831028e2a"');
//         });
//         it('should reject if key not found', function () {
//             return storage.getFileHeader('imAmSpecial')
//                 .should.eventually.be.rejectedWith(Error, "404");
//         });
//     });
//     describe('getSignedGETUrl()', function () {
//         var key = 'seeroseremotekeyExpire.jpg';

//         before(cleanTmpfolder);
//         before(function () {
//             var data = {
//                 localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                 remoteFilekey: key,
//                 metadata: {
//                     user: 'user'
//                 },
//                 headers: {}
//             };

//             return storage.uploadFile(data);
//         });
//         it('should return a signed GET url', function () {
//             var url = storage.getSignedGETUrl(key, 60000);
//             console.log(url)
//             url.should.be.a('string');
//             url.should.have.string('https://');
//             url.length.should.be.above(200);
//         });
//     });
//     describe('uploadFile()', function () {
//         it('should upload a file and report progress', function () {
//             this.timeout(5000);
//             var data = {
//                     localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                     remoteFilekey: 'seeroseremotekey.jpg',
//                     metadata: {
//                         user: 'user'
//                     },
//                     headers: {}
//                 },
//                 progressHandler;

//             progressHandler = function (progress) {
//                 console.log('progress: %d', progress.percent);
//             };

//             return storage.uploadFile(data, progressHandler);
//         });
//     });
//     describe('uploadStream()', function () {
//         it('should streamupload the given stream and resolve with the responsebody', function () {
//             this.timeout(5000);
//             var stream = fs.createReadStream(path.join(__dirname, 'files/seerose.jpg')),
//                 key = 'seeroseremotekey.jpg',
//                 metadata = {
//                     user: 'user'
//                 },
//                 headers = {
//                     'Content-Type': 'image/jpeg'
//                 };
//             return storage.uploadStream(stream, key, metadata, headers);
//         });
//         it('reject if stream not worin', function () {
//             this.timeout(5000);
//             var stream = fs.createReadStream(path.join(__dirname, 'files/seerose.jpg')),
//                 key = 'seeroseremotekey.jpg',
//                 metadata = {
//                     user: 'user'
//                 },
//                 headers = {
//                     'Content-Type': 'image/jpeg'
//                 };
//             return storage.uploadStream(stream, key, metadata, headers);
//         });
//     });
//     describe('copy files', function () {
//         var key = ['seeroseremotekey.jpg'];

//         before(function () {
//             var data = {
//                 localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                 remoteFilekey: key[0],
//                 metadata: {
//                     user: 'user'
//                 },
//                 headers: {}
//             };

//             return storage.uploadFile(data);
//         });
//         it('should copy a files and return the new key', function () {
//             this.timeout(5000);

//             return storage.copyFile(key[0])
//                 .then(console.log);
//         });
//     });
//     describe('deleteFiles()', function () {
//         var key = ['seeroseremotekey.jpg'];

//         before(function () {
//             var data = {
//                 localFilepath: path.join(__dirname, 'files/seerose.jpg'),
//                 remoteFilekey: key[0],
//                 metadata: {
//                     user: 'user'
//                 },
//                 headers: {}
//             };

//             return storage.uploadFile(data);
//         });
//         it('should delete a file', function () {
//             this.timeout(5000);

//             return storage.deleteFiles(key);
//         });
//         it('should reject if key not found', function () {
//             return storage.getFileStream('imAmSpecial')
//                 .should.eventually.be.rejectedWith(Error, "404");
//         });
//     });
//     describe('newUniqueKey', function () {
//         it('should', function () {
//             var key = storage.newUniqueKey('file.jpg');
//             key.should.be.a('string');
//             console.log(key)
//             key.length.should.equal(45)
//         });
//     });
// });
