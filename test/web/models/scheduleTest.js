var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    scheduleModel = proxyquire('../../lib/models/schedule', {
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model schedule', function () {
    describe('load schedule', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });

        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 88, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 89, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 90, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 2, 88, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 2, 89, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 2, 90, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 3, 90, '1');
        });
        it('should load a sujet', function () {
            return scheduleModel.get('1', '1')
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
    describe('book slot', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });

        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 88, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 89, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 90, '1');
        });
        it('book a slot', function () {
            return scheduleModel.bookDateslot(1, 1, 91, '1')
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
    describe('cancel slot', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });

        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 89, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(1, 1, 90, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(2, 1, 90, '1');
        });
        beforeEach(function () {
            return scheduleModel.bookDateslot(3, 1, 90, '1');
        });
        it('cancel a slot', function () {
            return scheduleModel.cancelDateslot(1, 1, 90, '1')
                .then(console.log);
        });
    });
});
