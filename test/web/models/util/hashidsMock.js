'use strict';

module.exports = {
    raw: {
        encode: function () {
            return arguments[0] + '.' + arguments[1];
        },
        decode: function(hashid) {
            return hashid.split('.');
        }
    },
    encode: function (id) {
        return id;
    },
    decode: function(hashid) {
        return hashid;
    }
};
