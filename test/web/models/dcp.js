var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire').noCallThru(),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    queueStub = {},
    dcpModel = proxyquire('../../lib/models/dcp', {
        '../queueJob': queueStub,
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model cron', function () {
    describe('getCurrentRenderBlocks', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });
        beforeEach(function () {
            return db.none(seed.baseSchedule);
        });


        it('should getCurrentRenderBlocks', function () {
            return dcpModel.getCurrentRenderBlocks()
                .then(function(data){
                    console.log(data);
                });
        });
    });
    describe('renderBlock', function () {
        queueStub.blockDcp = function(taskdata) {
            console.log(taskdata)
            return Promise.resolve();
            // return Promise.reject(new Error('queue error'));
        };
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });
        beforeEach(function () {
            return db.none(seed.baseSchedule);
        });

        it('should getCurrentRenderBlocks', function () {
            return dcpModel.renderBlock('1.141');
        });
    });

});
