var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    authenticationModel = proxyquire('../../lib/models/authentication', {
        './util/hashids': hashidsStub,
        './util/util': {
            encodeRowIdProperties: function(row, idProps) {
                return row;
            }
        }
    });

chai.use(chaiAsPromised);

describe('model user', function () {
    describe('load by email', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        it('should load a sujet', function () {
            return authenticationModel.findUserByEmail('user01@majorcinemacompany.com')
                .then(console.log);
        });
    });
    describe('get pwhash', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneUser);
        });
        it('should load a sujet', function () {
            return authenticationModel.getPwdHash('email@email')
                .then(console.log);
        });
    });
    describe('set pw hash', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneUser);
        });
        it('should load a sujet', function () {
            return authenticationModel.setPwd('email@email', 'sdfasdaf')
                .then(console.log);
        });
    });
});
