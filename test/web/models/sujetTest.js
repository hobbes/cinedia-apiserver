var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire').noCallThru(),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    queueStub = {},
    hashidsStub = require('./util/hashidsMock'),
    sujetModel = proxyquire('../../lib/models/sujet', {
        '../queueJob': queueStub,
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model sujet', function () {
    describe('create', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });

        it('should create sujet and push tasks to queue', function () {
            var createdSujet;

            queueStub.diaIntermediate = function(){
                return new Promise.resolve();
            };

            createdSujet = sujetModel.create({
                name: 'testname',
                sujettype: 'dia',
                filekey: 'filekey',
                duration: 7
            }, '1');

            return createdSujet.then(console.log);
        });
        
        it('should not create a sujet if push to taskqueue is unsuccessful', function(){
            var createdSujet;

            queueStub.diaIntermediate = function(){
                return new Promise.reject();
            };

            createdSujet = sujetModel.create({
                name: 'testname',
                sujettype: 'dia',
                filekey: 'filekey',
                duration: 7
            }, '1');

            return expect(createdSujet)
                .to.eventually.be.rejected;
        });
    });
    describe('load item', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should load a sujet', function () {
            return sujetModel.get('1', '1')
                .then(console.log);
        });
    });
    describe('load collection', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.multipleSujets);
        });
        it('should load a sujet', function () {
            return sujetModel.findByCustomer({
                    direction: 'prev',
                    limit: 5,
                    type: 'dia',
                    archived: 'all'
                }, '1')
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
    describe('update sujet item name', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should update a sujet', function () {
            return sujetModel.updateName('1', 'updatedname', '1')
                .then(console.log);
        });
    });
    describe('set archived status of sujet', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should update a sujet', function () {
            return sujetModel.archive('1', true, '1')
                .then(console.log);
        });
    });
    describe('delete sujet', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should update a sujet', function () {
            return sujetModel.del('1', '1')
                .then(console.log);
        });
    });
});
