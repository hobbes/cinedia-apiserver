var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    taskResultModel = proxyquire('../../lib/models/taskresult', {
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model taskresult', function () {
    describe('add preview', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should add preview', function () {
            return taskResultModel.addPreview('1', {
                    size: 500,
                    type: 'still',
                    key: 'asdfasdfas',
                    etag: 'SFUISKHSDKJF'
                })
                .then(console.log);
        });
    });
    describe('add intermediat storage key', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should add intermediat', function () {
            return taskResultModel.addIntermediate('1', {
                    key: 'sadfasdf',
                    duration: 7
                })
                .then(console.log);
        });
    });
    describe('add intermediat error', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.oneSujet);
        });
        it('should mark intermediat as error', function () {
            return taskResultModel.addIntermediateError('1', 'error')
                .then(console.log);
        });
    });
    describe('complete dcp with error', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.oneDcpPending);
        });
        it('should add intermediat', function () {
            return taskResultModel.completeDcp('1.1')
            .then(console.log);
        });
    });
    describe('complete dcp with result', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.base);
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.oneDcpPending);
        });
        it('should add intermediat', function () {
            return taskResultModel.completeDcp('1.1', {
                etag: '234234',
                key: 'ewgweg',
                hash: 'adfaaf'
            })
            .then(console.log);
        });
    });
});
