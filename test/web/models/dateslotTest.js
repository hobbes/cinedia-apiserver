var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    dateslotModel = proxyquire('../../lib/models/dateslot', {
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model dateslot', function () {
    describe('load dateslot', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        it('should load the current dateslot', function () {
            return dateslotModel.now()
                .then(console.log);
        });
    });
    describe('load dateslot by id', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        it('should load the current dateslot', function () {
            return dateslotModel.get('100')
                .then(console.log);
        });
    });
    describe('load dateslots by startdate, enddate', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        it('should load the current dateslot', function () {
            return dateslotModel.find(new Date('2017-01-01'), new Date('2017-03-15'))
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
});
