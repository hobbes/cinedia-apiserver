var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    stageModel = proxyquire('../../lib/models/stage', {
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model stage', function () {
    describe('load all stages for customer', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        it('should load a user', function () {
            return stageModel.findByCostomerId('1')
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
    describe('load stage by id', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        it('should load a user', function () {
            return stageModel.get('1', '1')
                .then(console.log);
        });
    });    
});
