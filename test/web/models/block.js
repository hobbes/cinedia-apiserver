var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    proxyquire = require('proxyquire'),
    Promise = require('bluebird'),
    expect = chai.expect,
    db = require('../../lib/db'),
    seed = require('../seed/sql'),
    testutil = require('../testutils/util'),
    migrations = require('../../lib/migrations'),
    hashidsStub = require('./util/hashidsMock'),
    blockModel = proxyquire('../../lib/models/block', {
        './util/hashids': hashidsStub
    });

chai.use(chaiAsPromised);

describe('model block', function () {
    describe('find', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });
        beforeEach(function () {
            return db.none(seed.baseSchedule);
        });


        it('should find blocks for kw', function () {
            var dateslotId = 141,
                customerIdsReadAccessEnc = [1, 5],
                stageIdsReadAccessEnc = [1, 2, 3, 4, 5, 6];

            return blockModel.find(dateslotId, customerIdsReadAccessEnc, stageIdsReadAccessEnc)
                .then(function(data){
                    console.log(JSON.stringify(data));
                });
        });
    });
    describe('get', function () {
        beforeEach(function () {
            return testutil.resetDb();
        });
        beforeEach(function () {
            return migrations.up();
        });
        beforeEach(function () {
            return db.none(seed.baseStages);
        });
        beforeEach(function () {
            return db.none(seed.baseCustomers);
        });
        beforeEach(function () {
            return db.none(seed.baseUsers);
        });
        beforeEach(function () {
            return db.none(seed.baseReadySujets);
        });
        beforeEach(function () {
            return db.none(seed.baseSchedule);
        });


        it('should get block', function () {
            var blockIdEnc = '1.141',
                customerIdsReadAccessEnc = [1, 5];

            return blockModel.get(blockIdEnc, customerIdsReadAccessEnc)
                .then(function(data){
                    console.log(data);
                });
        });
    });
});
