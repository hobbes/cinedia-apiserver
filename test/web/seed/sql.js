var pgp = require('pg-promise'),
    path = require('path'),
    QueryFile = pgp.QueryFile,
    enumSql = pgp.utils.enumSql;

module.exports = enumSql(path.join(__dirname, '../seed'), {
        recursive: true
    }, function(file) {
        return new QueryFile(file);
    });