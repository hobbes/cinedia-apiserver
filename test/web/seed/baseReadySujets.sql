INSERT INTO
  sujets (sujet_id, customer_id, name, duration, type, storagekey_sourcefile, storagekey_intermediatefile)
VALUES
  (1, 1, 'sujetname1', 7, 'dia', 'sourcekey345', 'interm2345ideiatekey'),
  (2, 1, 'sujetname2', 7, 'dia', 'sourcekeywer45', 'intermi345deiatekey'),
  (3, 1, 'sujetname3', 7, 'dia', 'sourcekwertey', 'intermi234deiatekey'),
  (4, 1, 'sujetname4', 11, 'spot_mute', 'souyetrcekey', 'interermideiatekey'),
  (5, 1, 'sujetname5', 20, 'spot_sound', 'sourtyrcekey', 'inter34mideiatekey'),

  (6, 2, 'sujetname6', 7, 'dia', 'sourewrtcekey', 'intermideweiatekey'),
  (7, 2, 'sujetname7', 7, 'dia', 'sourcekewrtey', 'interm3ri3wedeiatekey'),
  (8, 2, 'sujetname8', 10, 'spot_sound', 'souytwrcekey', 'inte534rmideiatekey'),
  (9, 2, 'sujetname9', 15, 'spot_sound', 'soerurcekey', 'intermid34eiatekey'),

  (10, 3, 'sujetname10', 7, 'dia', 'sourcqweekey', 'inte4rrmideiatekey'),
  (11, 3, 'sujetname11', 7, 'dia', 'sourfdcekey', 'interr32mideiatekey'),
  (12, 3, 'sujetname12', 7, 'dia', 'souedfrcekey', 'interemid23eiatekey'),
  (13, 3, 'sujetname13', 7, 'dia', 'souvdrcekey', 'intermewid23eiatekey'),
  (14, 3, 'sujetname14', 10, 'spot_mute', 'sou3bdfrcekey', 'intewrmideiatekey'),
  (15, 3, 'sujetname15', 10, 'spot_mute', 'sougrcdsfekey', 'intermw6ideiatekey'),
  (16, 3, 'sujetname16', 10, 'spot_sound', 'sourfgscekey', 'intermtideiatekey'),
  (17, 3, 'sujetname17', 10, 'spot_sound', 'sou45rcekey', 'intermidefiatekey'),
  (18, 3, 'sujetname18', 10, 'spot_sound', 'soutrrtyrcekey', 'intefermideiatekey'),

  (19, 4, 'sujetname19', 7, 'dia', 'souresdfdswrtcekey', 'intermidewsdgbeiatekey'),
  (20, 4, 'sujetname20', 7, 'dia', 'sourcekedsgfdwrtey', 'interm3ri3wsgedeiatekey'),
  (21, 4, 'sujetname21', 10, 'spot_sound', 'ssdfgouytwrcekey', 'inte53dhdn4rmideiatekey'),
  (22, 4, 'sujetname22', 15, 'spot_mute', 'sosdfgerurcekey', 'intermisbfd34eiatekey'),

  (23, 5, 'sujetname23', 7, 'dia', 'souvsdgdrcekey', 'intermdfgewid23eiatekey'),
  (24, 5, 'sujetname24', 10, 'spot_mute', 'sousdfg3bdfrcekey', 'integdfwrmideiatekey'),
  (25, 5, 'sujetname25', 10, 'spot_mute', 'sougrcdsfadekey', 'intersdfgmw6ideiatekey'),
  (26, 5, 'sujetname26', 10, 'spot_sound', 'souafrfgscekey', 'intermtideifdgsatekey'),

  (27, 6, 'sujetname27', 7, 'dia', 'souvs3456346dey', 'inter345346mdfgewkey'),
  (28, 6, 'sujetname28', 10, 'spot_mute', 'sou3456bdfrcekey', 'inte34563gdeiatekey'),
  (29, 6, 'sujetname29', 10, 'spot_mute', 'sougrfa456dekey', 'inte3456rsdfgmw6tekey'),
  (30, 6, 'sujetname30', 10, 'spot_sound', 'soua456cekey', 'inte34564rmtiatekey');