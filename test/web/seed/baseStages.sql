INSERT INTO
  companies (company_id, name, nameshort, fps, lockoffsethours)
VALUES
  (1, 'Major Cinema', 'MJRC', 25, '72'),
  (2, 'Indpendent', 'INDP', 24, '72'),
  (3, 'Local Lighthouse', 'LCLH', 25, '72'),
  (4, 'Live Events', 'LVEV', 25, '72');

INSERT INTO
  locations (location_id, company_id, name)
VALUES
  (1, 1, 'Major Cinema Megacity'),
  (2, 1, 'Major Cinema Minicity'),

  (3, 2, 'Indpendent East'),
  (4, 2, 'Indpendent West'),
  (5, 2, 'Indpendent South'),

  (6, 3, 'Lighthouse RED'),
  (7, 3, 'Lighthouse BLUE'),

  (8, 4, 'Live in City');

INSERT INTO
  stages (stage_id, location_id, name, nameshort)
VALUES
  (1, 1, 'Megacity 1', 'MEC1'),
  (2, 1, 'Megacity 2', 'MEC2'),
  (3, 1, 'Megacity 3', 'MEC3'),
  (4, 1, 'Megacity 4', 'MEC4'),

  (5, 2, 'Minicity 1', 'MIC1'),
  (6, 2, 'Minicity 2', 'MIC2'),

  (7, 3, 'East 1', 'EST1'),
  (8, 3, 'East 2', 'EST2'),

  (10, 4, 'West 1', 'WST1'),
  (11, 4, 'West 2', 'WST2'),

  (12, 5, 'South 1', 'STH1'),
  (13, 5, 'South 2', 'STH2'),

  (14, 6, 'RED 1', 'RD1'),
  (15, 6, 'RED 2', 'RD2'),
  (16, 6, 'RED 3', 'RD3'),

  (17, 7, 'BLUE 1', 'BL1'),
  (18, 7, 'BLUE 2', 'BL2'),

  (19, 8, 'Saal 1', 'SL1'),
  (20, 8, 'Saal 2', 'SL2');