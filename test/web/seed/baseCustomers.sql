INSERT INTO
  customers (customer_id, type, name, nameshort)
VALUES
  (1, 'cinema', 'Major Cinema Company', 'MCC'),
  (2, 'cinema', 'Independent Group', 'EDGR'),
  (3, 'cinema', 'Lighthouse Features', 'LHFT'),
  (4, 'cinema', 'Live Events INC', 'LIVE'),
  (5, 'advertiser', 'Cinema Advertiser', 'CNA'),
  (6, 'advertiser', 'Broadcast Advertiser', 'BA'),
  (7, 'thirdparty', 'Statistics', 'ST');

INSERT INTO
  access_readwrite_customer_to_companyblocksegment (customer_id, company_id, mute_position, audio_position)
VALUES
  -- Major Cinema Company, access Major Cinema
  (1, 1, 1, 4),
  -- Independent Group, access Indepentend
  (2, 2, 1, 4),
  -- Lighthouse Features access Local Lighthouse
  (3, 3, 1, 4),
  -- Live Events INC access Live Events
  (4, 4, 1, 4),

  -- Cinema Advertiser access Major Cinema Company
  (5, 1, 2, 3),
  -- Cinema Advertiser access  Independent East
  (5, 2, 2, 3),
  -- Cinema Advertiser access Local Lighthouse
  (5, 3, 2, 3),

  -- Broadacast Advertiser acccess Live Events
  (6, 4, 2, 3);


-- grants read access to sujets of other customers
INSERT INTO
  access_read_customers_customers (customer_id, access_read_customer_id)
VALUES
  (1, 5),
  (2, 5),
  (3, 5),
  (4, 6);