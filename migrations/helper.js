'use strict';

const fs = require('fs'), 
  path = require('path'),
  pgp = require('pg-promise'),
  schemaInfo = require('./schema.json'),
  QueryFile = pgp.QueryFile,
  db = require('../lib/web/db');

function sqlFilesOfFolder(folder) {
  const object = {},
    files = fs.readdirSync(folder),
    onlySqlFiles = files.filter((file) => {
      return path.extname(file) === '.sql';
    }),
    sequence = onlySqlFiles.map((file) => {
      return path.basename(file, '.sql');
    });

  if (sequence[0] !== '1') {
    throw new Error('sequence does not start with 1');
  }
  if (sequence.length > 1) {
    sequence.forEach((current, index, array) => {
      if (index < array.length - 1 && (Number(array[index + 1]) - Number(current) !== 1)) {
        throw new Error('missing file in sequence');
      } 
    });
  }

  onlySqlFiles.forEach((file) => {
    object[path.basename(file, '.sql')] = new QueryFile(path.join(folder, file));
  });

  return object;
}

async function getCurrentVersionOfDB() {
  try {
    const queryResult = await db.one('SELECT MAX(number) as max_version FROM schema_versions;');
    return queryResult.max_version || 0;
  } catch(error) {
    const undefinedTableErrorCode = '42P01';

    if (error.code === undefinedTableErrorCode) {
      return 0;
    } else {
      throw error;
    }
  }
}

exports.migrateToLatest = async () => {
  const schemasObject = sqlFilesOfFolder(path.join(__dirname, './schemas/')),
    currentVersionOfDB = await getCurrentVersionOfDB(),
    keys = Object.keys(schemasObject),
    maxSchemafileAvailable = Math.max(...keys);

  await db.none('CREATE TABLE IF NOT EXISTS schema_versions (number integer UNIQUE NOT NULL, hash text UNIQUE);');

  if (currentVersionOfDB > maxSchemafileAvailable) {
    throw new Error('possible downgrade detected');
  }

  if (currentVersionOfDB !== maxSchemafileAvailable) {
    await db.tx(async (t) => {
      for (let i = currentVersionOfDB + 1; i <= maxSchemafileAvailable; i++) {
        await t.one('INSERT INTO schema_versions (number) VALUES (${fileVersion}) RETURNING number', {
          fileVersion: i
        });
        await t.none(schemasObject[i]);
      }
    });
  }

  return {
    from: currentVersionOfDB,
    to: maxSchemafileAvailable
  };
};

exports.verifySchemaVersion = async () => {
  const currentVersionOfDB = await getCurrentVersionOfDB(),
    currentVersionOfCode = schemaInfo.version;

  if (currentVersionOfCode === currentVersionOfDB || currentVersionOfCode + 1 === currentVersionOfDB) {
    return {
      code: currentVersionOfCode,
      db: currentVersionOfDB
    };
  }

  throw new Error(`DB schema not compatible with code. Code Version: ${currentVersionOfCode}, DB Version: ${currentVersionOfDB}`);
};