ALTER TABLE sujets
  ADD COLUMN sourcefile_validated boolean NOT NULL DEFAULT false,
  ADD COLUMN sourcefile_isvalid boolean NOT NULL DEFAULT false,
  ADD COLUMN sourcefile_validation_job_id text,
  ADD COLUMN sourcefile_validation_error text,
  ADD COLUMN sourcefile_validation_error_reported boolean NOT NULL DEFAULT false,
  ADD COLUMN intermediate_job_id text UNIQUE,
  ADD COLUMN intermediate_error text UNIQUE;

UPDATE
  sujets
SET
  sourcefile_validated = true,
 	sourcefile_isvalid = true
WHERE
  storagekey_intermediatefile IS NOT NULL;
