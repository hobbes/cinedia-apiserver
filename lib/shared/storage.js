'use strict';

var uuidv4 = require('uuid/v4');

exports.newUniqueKey = function (extension) {
  var key = uuidv4().replace(/-/g,'');

  if (extension) {
    if (extension.startsWith('.')) {
      key = `${key}${extension}`;  
    } else {
      key = `${key}.${extension}`;
    }
  }
  return key;
};