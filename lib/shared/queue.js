'use strict';

const Bull = require('bull'),
  config = require('config'),
  log = require('./logger'),
  redisConfig = config.get('redis'),
  queueConfig = config.get('queue'),
  bullConfig = {
    prefix: queueConfig.prefix,
    redis: {
      host: redisConfig.host,
      port: redisConfig.port
    },
    defaultJobOptions: {
      removeOnComplete: queueConfig.jobsToKeep.completed,
      removeOnFail: queueConfig.jobsToKeep.failed,
      attempts: 3,
      backoff: {
        type: 'exponential',
        delay: 1000
      }
    }
  },
  jobs = new Bull(queueConfig.names.jobs, bullConfig),
  results = new Bull(queueConfig.names.results, bullConfig);

jobs.on('global:error', function (error) {
  log.error('job queue error: ' + error.message);
});
jobs.on('global:progress', function(jobId, progress){
  log.debug(`jobprogress  id: ${jobId}  progress: ${progress}`);
});
jobs.on('global:paused', function() {
  log.info('job queue paused');
});
jobs.on('global:resumed', function() {
  log.info('job queue resumed');
});

results.on('global:error', function (error) {
  log.error('result queue error: ' + error.message);
});
results.on('global:paused', function() {
  log.info('job queue paused');
});
results.on('global:resumed', function() {
  log.info('job queue resumed');
});

exports.jobs = jobs;
exports.results = results;