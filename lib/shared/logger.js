'use strict';

var loglevel = require('loglevel'),
  config = require('config');

loglevel.setLevel(config.get('loglevel'));

module.exports = loglevel;