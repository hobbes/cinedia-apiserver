'use strict';

var Promise = require('bluebird'),
  config = require('config'),
  log = require('../shared/logger'),
  tools = require('./tools'),
  path = require('path'),
  fs = require('fs-extra'),
  readFile = Promise.promisify(fs.readFile),
  writeFile = Promise.promisify(fs.writeFile),
  readdir = Promise.promisify(fs.readdir),
  mkdirs = Promise.promisify(fs.mkdirs),

  FLAT_WIDTH = 1998,
  FLAT_HEIGHT = 1080,
  FLAT_RATIO = 1.85,
  PREVIEW_FRAME = config.get('preview.framePercentage'),
  THUMBNAIL_SIZE = 300,
  MAX_BANDWITH = config.get('dcp.maxBandwith'),
  ACCEPTED_FRAMERATES = [24, 25],
  ACCEPTED_IMG_TYPES = ['JPEG', 'TIFF', 'PNG'],
  PROBE_SCOERE_THRESHOLD = config.get('probeScoreThreshold');


function getPreviewFrame(sequenceFolder) {
  return readdir(sequenceFolder)
    .then(function (files) {
      return path.join(sequenceFolder, files[Number(files.length * PREVIEW_FRAME).toFixed()]);
    });
}

function getNumberOfExtractedFrames(sequenceFolder) {
  return readdir(sequenceFolder)
    .then(function (files) {
      return files.length;
    });
}

function calcSteps(start, end, progress) {
  let range = end - start;

  return start + (range / 100 * progress);
}

exports.entar = tools.entar;
exports.detar = tools.detar;
exports.zip = tools.zip;

exports.validateDia = function (inputFile, reportProgress) {
  reportProgress(0);
  return tools.validateStill(inputFile, ACCEPTED_IMG_TYPES)
    .tap(() => {
      reportProgress(100);
    });
};
exports.validateSpotMute = function (inputFile, reportProgress) {
  reportProgress(0);
  return tools.validateVideo(inputFile, false, ACCEPTED_FRAMERATES, PROBE_SCOERE_THRESHOLD)
    .tap(() => {
      reportProgress(100);
    });
};
exports.validateSpotSound = function (inputFile, reportProgress) {
  reportProgress(0);
  return tools.validateVideo(inputFile, true, ACCEPTED_FRAMERATES, PROBE_SCOERE_THRESHOLD)
    .tap(() => {
      reportProgress(100);
    });
};

exports.createDiaIntermediate = function (inputFile, duration, workdir, reportProgress) {
  var intermediateTiff = path.join(workdir, 'intertiff.tiff'),
    intermediateArchiveDir = path.join(workdir, 'archive'),
    j2cSequenceDir = path.join(intermediateArchiveDir, 'sequence'),
    intermediateArchive = path.join(workdir, 'archive.tar'),
    intermediateJ2c = path.join(intermediateArchiveDir, 'frame.j2c'),
    metadataJSON = path.join(intermediateArchiveDir, 'metadata.json'),
    thumbnail = path.join(workdir, 'thumbnail.jpg'),
    previewStill1080 = path.join(workdir, 'previewStill1080.jpg'),
    previewStill540 = path.join(workdir, 'previewStill540.jpg'),
    previewStill270 = path.join(workdir, 'previewStill270.jpg');

  reportProgress(0);
  return tools.validateStill(inputFile, ACCEPTED_IMG_TYPES)
    .then(function(){
      reportProgress(10);
      return Promise.all([
        mkdirs(intermediateArchiveDir),
        mkdirs(j2cSequenceDir)
      ]);
    })
    .then(function(){
      reportProgress(20);
      return Promise.all([
        tools.stillConvert(inputFile, intermediateTiff, FLAT_WIDTH, FLAT_HEIGHT),
        tools.thumbnail(inputFile, thumbnail, THUMBNAIL_SIZE)
      ]);
    })
    .then(function(){
      reportProgress(30);
      return Promise.all([
        tools.preview(intermediateTiff, previewStill1080, 1080 * FLAT_RATIO, 1080),
        tools.preview(intermediateTiff, previewStill540, 540 * FLAT_RATIO, 540),
        tools.preview(intermediateTiff, previewStill270, 270 * FLAT_RATIO, 270)
      ]);
    })
    .then(function(){
      reportProgress(40);
      return Promise.all([
        tools.opendcp_j2k(intermediateTiff, intermediateJ2c, MAX_BANDWITH, null),
        writeFile(metadataJSON, JSON.stringify({
          duration: duration
        }))
      ]);
    })
    .then(function() {
      reportProgress(80);
      return tools.entar(intermediateArchiveDir, intermediateArchive);
    })
    .then(function() {
      reportProgress(100);
      return {
        metadata: {
          duration: duration
        },
        files: [
          {
            type: 'intermediate',
            path: intermediateArchive
          }, {   
            type: 'preview',
            previewType: 'thumbnail',
            previewFormat: 'image/jpeg',
            size: 400,
            path: thumbnail
          }, {   
            type: 'preview',
            previewType: 'still',
            previewFormat: 'image/jpeg',
            size: 1080,
            path: previewStill1080
          }, {   
            type: 'preview',
            previewType: 'still',
            previewFormat: 'image/jpeg',
            size: 540,
            path: previewStill540
          }, {   
            type: 'preview',
            previewType: 'still',
            previewFormat: 'image/jpeg',
            size: 270,
            path: previewStill270
          }
        ]
      };
    });
};

exports.createSpotMuteIntermediate = function (inputFile, workdir, reportProgress) {
  var intermediateArchiveDir = path.join(workdir, 'archive'),
    tiffSequenceDir = path.join(workdir, 'sequenceTiff'),
    j2cSequenceDir = path.join(intermediateArchiveDir, 'sequence'),
    intermediateArchive = path.join(workdir, 'archive.tar'),
    thumbnail = path.join(workdir, 'thumbnail.jpg'),
    tiles = path.join(workdir, 'tiles.jpg'),
    previewVideo1080 = path.join(workdir, 'previewVideo1080.mp4'),
    previewVideo540 = path.join(workdir, 'previewVideo540.mp4'),
    previewStill1080 = path.join(workdir, 'previewStill1080.jpg'),
    previewStill540 = path.join(workdir, 'previewStill540.jpg'),
    previewStill270 = path.join(workdir, 'previewStill270.jpg');

  return tools.validateVideo(inputFile, false, ACCEPTED_FRAMERATES, PROBE_SCOERE_THRESHOLD)
    .then(function(media) {
      let totalFrames = media.video.fps * media.video.duration;

      log.debug('createSpotMuteIntermediate:', media);
      reportProgress(0);
      return Promise.all([
        mkdirs(tiffSequenceDir),
        mkdirs(j2cSequenceDir),
        mkdirs(intermediateArchiveDir)
      ])
        .then(function () {
          return tools.ffmpegExtractSequence(inputFile, tiffSequenceDir, totalFrames, (progress) => {
            reportProgress(calcSteps(1, 8, progress));
          })
            .then(function () {
              return getPreviewFrame(tiffSequenceDir)
                .then(function (frame) {
                  return tools.thumbnail(frame, thumbnail, THUMBNAIL_SIZE);
                });
            })
            .then(function () {
              return tools.batchExtentToFlat(tiffSequenceDir, totalFrames, (progress) => {
                reportProgress(calcSteps(8, 25, progress));
              });
            })
            .then(function () {
              return tools.createTiles(tiffSequenceDir, tiles, 10, 270);
            })
            .then(function () {
              return getPreviewFrame(tiffSequenceDir)
                .then(function (frame) {
                  return Promise.all([
                    tools.preview(frame, previewStill1080, 1080 * FLAT_RATIO, 1080),
                    tools.preview(frame, previewStill540, 540 * FLAT_RATIO, 540),
                    tools.preview(frame, previewStill270, 270 * FLAT_RATIO, 270)
                  ]);
                });
            })
            .then(function() {
              return tools.generateMP4Preview(inputFile, previewVideo1080, 1080 * FLAT_RATIO, 1080, false);
            })
            .then(function() {
              return tools.generateMP4Preview(inputFile, previewVideo540, 540 * FLAT_RATIO, 540, false);
            })
            .then(function () {
              return tools.opendcp_j2k(tiffSequenceDir, j2cSequenceDir, MAX_BANDWITH, totalFrames, (progress) => {
                reportProgress(calcSteps(25, 95, progress));
              });
            })
            .then(function () {
              return tools.entar(intermediateArchiveDir, intermediateArchive);
            })
            .then(function () {
              reportProgress(100);
              return {
                metadata: {
                  duration: media.video.duration,
                  ffprobeResult: media.meta.ffprobeResult
                },
                files: [
                  {
                    type: 'intermediate',
                    path: intermediateArchive
                  }, {
                    type: 'preview',
                    previewType: 'thumbnail',
                    previewFormat: 'image/jpeg',
                    size: 400,
                    path: thumbnail
                  }, {
                    type: 'preview',
                    previewType: 'sequence',
                    previewFormat: 'image/jpeg',
                    size: 270,
                    path: tiles
                  }, {
                    type: 'preview',
                    previewType: 'video',
                    previewFormat: 'video/mp4',
                    size: 1080,
                    path: previewVideo1080
                  }, {
                    type: 'preview',
                    previewType: 'video',
                    previewFormat: 'video/mp4',
                    size: 540,
                    path: previewVideo540
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 540,
                    path: previewStill540
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 1080,
                    path: previewStill1080
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 270,
                    path: previewStill270
                  }
                ]
              };
            });
        });   
    });
};

exports.createSpotSoundIntermediate = function (inputFile, workdir, reportProgress) {
  var intermediateArchiveDir = path.join(workdir, 'archive'),
    tiffSequenceDir = path.join(workdir, 'sequenceTiff'),
    j2cSequenceDir = path.join(intermediateArchiveDir, 'sequence'),
    audio24 = path.join(intermediateArchiveDir, 'audio24.wav'),
    audio25 = path.join(intermediateArchiveDir, 'audio25.wav'),
    intermediateArchive = path.join(workdir, 'archive.tar'),
    thumbnail = path.join(workdir, 'thumbnail.jpg'),
    tiles = path.join(workdir, 'tiles.jpg'),
    previewVideo1080 = path.join(workdir, 'previewVideo1080.mp4'),
    previewVideo540 = path.join(workdir, 'previewVideo540.mp4'),
    previewStill1080 = path.join(workdir, 'previewStill1080.jpg'),
    previewStill540 = path.join(workdir, 'previewStill540.jpg'),
    previewStill270 = path.join(workdir, 'previewStill270.jpg');

  reportProgress(0);
  return tools.validateVideo(inputFile, true, ACCEPTED_FRAMERATES, PROBE_SCOERE_THRESHOLD)
    .then(function(media) {
      let totalFrames = media.video.fps * media.video.duration;

      log.debug('createSpotSoundIntermediate:', media);
      return Promise.all([
        mkdirs(tiffSequenceDir),
        mkdirs(j2cSequenceDir),
        mkdirs(intermediateArchiveDir)
      ])
        .then(function () {
          return tools.ffmpegExtractSequence(inputFile, tiffSequenceDir, totalFrames, (progress) => {
            reportProgress(calcSteps(1, 8, progress));
          })
            .then(function () {
              return getPreviewFrame(tiffSequenceDir)
                .then(function (frame) {
                  return tools.thumbnail(frame, thumbnail, THUMBNAIL_SIZE);
                });
            })
            .then(function () {
              return tools.batchExtentToFlat(tiffSequenceDir, totalFrames, (progress) => {
                reportProgress(calcSteps(8, 25, progress));
              });
            })
            .then(function () {
              return tools.createTiles(tiffSequenceDir, tiles, 10, 270);
            })
            .then(function () {
              return getPreviewFrame(tiffSequenceDir)
                .then(function (frame) {
                  return Promise.all([
                    tools.preview(frame, previewStill1080, 1080 * FLAT_RATIO, 1080),
                    tools.preview(frame, previewStill540, 540 * FLAT_RATIO, 540),
                    tools.preview(frame, previewStill270, 270 * FLAT_RATIO, 270)
                  ]);
                });
            })
            .then(function() {
              return tools.generateMP4Preview(inputFile, previewVideo1080, 1080 * FLAT_RATIO, 1080, true, media.audio.loudnorm);
            })
            .then(function() {
              return tools.generateMP4Preview(inputFile, previewVideo540, 540 * FLAT_RATIO, 540, true, media.audio.loudnorm);
            })
            .then(function () {
              return tools.opendcp_j2k(tiffSequenceDir, j2cSequenceDir, MAX_BANDWITH, totalFrames, (progress) => {
                reportProgress(calcSteps(25, 91, progress));
              });
            })
            .then(function() {
              return getNumberOfExtractedFrames(tiffSequenceDir);
            })
            .then(function (frames) {
              var duration24 = frames / 24,
                duration25 = frames / 25,
                totSamples24 = duration24 * 48000 + 48000,
                totSamples25 = duration25 * 48000 + 48000,
                slowDown24,
                speedUp25;

              if (media.video.fps === 24) {
                speedUp25 = 25 / 24;
              }
              if (media.video.fps === 25) {
                slowDown24 = 0.96;
              }

              return tools.ffmpegExtractWav(inputFile, audio24, duration24, totSamples24, slowDown24, media.audio.loudnorm)
                .then(function () {
                  return tools.ffmpegExtractWav(inputFile, audio25, duration25, totSamples25, speedUp25, media.audio.loudnorm);
                });
            })
            .then(function () {
              reportProgress(95);
              return tools.entar(intermediateArchiveDir, intermediateArchive);
            })
            .then(function () {
              reportProgress(100);
              return {
                metadata: {
                  duration: media.video.duration
                },
                files: [
                  {
                    type: 'intermediate',
                    path: intermediateArchive
                  }, {
                    type: 'preview',
                    previewType: 'thumbnail',
                    previewFormat: 'image/jpeg',
                    size: 400,
                    path: thumbnail
                  }, {
                    type: 'preview',
                    previewType: 'sequence',
                    previewFormat: 'image/jpeg',
                    size: 270,
                    path: tiles
                  }, {
                    type: 'preview',
                    previewType: 'video',
                    previewFormat: 'video/mp4',
                    size: 540,
                    path: previewVideo540
                  }, {
                    type: 'preview',
                    previewType: 'video',
                    previewFormat: 'video/mp4',
                    size: 1080,
                    path: previewVideo1080
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 1080,
                    path: previewStill1080
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 540,
                    path: previewStill540
                  }, {
                    type: 'preview',
                    previewType: 'still',
                    previewFormat: 'image/jpeg',
                    size: 270,
                    path: previewStill270
                  }
                ]
              };
            });
        });
    });   
};


exports.createBlockMute = function (intermediateDirArray, dcpDir, dcpInfo, blackFrameFile, fps, spaceFrames, workdir) {
  var pictureMxf = path.join(dcpDir, 'picture.mxf'),
    silentAudioWav = path.join(workdir, 'audio.wav'),
    soundMxf = path.join(dcpDir, 'sound.mxf'),
    concatedSujetsSequenceDir = path.join(workdir, 'sequence');

  return Promise.map(intermediateDirArray, function (intermediateDir) {
    return readFile(path.join(intermediateDir, 'metadata.json'))
      .then(function (metadataFile) {
        return JSON.parse(metadataFile);
      })
      .then(function (metadata) {
        var frame = path.join(intermediateDir, 'frame.j2c'),
          sequenceDir = path.join(intermediateDir, 'sequence');

        return tools.duplicate(frame, sequenceDir, metadata.duration * fps, 'j2c');
      })
      .catch(function (error) {
        if (error.code === 'ENOENT') {
          return;
        }
        throw error;
      });
  })
    .then(function () {
      return intermediateDirArray.map(function (intermediateDir) {
        return path.join(intermediateDir, 'sequence');
      });
    })
    .then(function (sequenceFolders) {
      return tools.sequence(sequenceFolders, blackFrameFile, spaceFrames, concatedSujetsSequenceDir);
    })
    .then((totalFrameCount) => {
      return tools.generateSilentWav(silentAudioWav, totalFrameCount / fps);
    })
    .then(function () {
      return mkdirs(dcpDir);
    })
    .then(function () {
      return tools.opendcp_mxf(silentAudioWav, soundMxf, fps);
    })
    .then(function () {
      return tools.opendcp_mxf(concatedSujetsSequenceDir, pictureMxf, fps);
    })
    .then(function () {
      return tools.opendcp_xml(dcpDir, pictureMxf, soundMxf, dcpInfo.issuer, dcpInfo.annotation, dcpInfo.title);
    });
};

exports.createBlockSound = function (intermediateDirArray, dcpDir, dcpInfo, blackFrameFile, fps, spaceFrames, workdir) {
  var tmpTxtFile = path.join(workdir, 'concat.txt'),
    silenceWav = path.join(workdir, 'silence.wav'),
    audioWav = path.join(workdir, 'audio.wav'),
    soundMxf = path.join(dcpDir, 'sound.mxf'),
    pictureMxf = path.join(dcpDir, 'picture.mxf'),
    spaceDuration = Number(spaceFrames / fps),
    concatedSujetsSequenceDir = path.join(workdir, 'sequence');

  return mkdirs(dcpDir)
    .then(function () {
      return intermediateDirArray.map(function (intermediateDir) {
        return path.join(intermediateDir, 'sequence');
      });
    })
    .then(function (sequenceFolders) {
      return tools.sequence(sequenceFolders, blackFrameFile, spaceFrames, concatedSujetsSequenceDir);
    })
    .then(function () {
      return intermediateDirArray.map(function (intermediateDir) {
        return path.join(intermediateDir, 'audio' + fps + '.wav');
      });
    })
    .then(function (audioWavs) {
      return tools.generateSilentWav(silenceWav, spaceDuration)
        .then(function () {
          return tools.concatWav(audioWavs, tmpTxtFile, silenceWav, audioWav);
        });
    })
    .then(function () {
      return tools.opendcp_mxf(audioWav, soundMxf, fps);
    })
    .then(function () {
      return tools.opendcp_mxf(concatedSujetsSequenceDir, pictureMxf, fps);
    })
    .then(function () {
      return tools.opendcp_xml(dcpDir, pictureMxf, soundMxf, dcpInfo.issuer, dcpInfo.annotation, dcpInfo.title);
    });
};