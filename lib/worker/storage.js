'use strict';

var Promise = require('bluebird'),
  s3 = require('s3'),
  config = require('config'),
  objectstorageConfig = config.get('objectstorage'),
  s3client = s3.createClient({
    maxAsyncS3: 20,
    s3RetryCount: 3,
    s3RetryDelay: 1000,
    multipartUploadThreshold: 20971520,
    multipartUploadSize: 15728640,
    s3Options: {
      accessKeyId: objectstorageConfig.key,
      secretAccessKey: objectstorageConfig.secret,
      endpoint: objectstorageConfig.url
    }
  });

function progressCalculator(loader, progress, message, report) {
  return function() {
    let currentProgress = ((loader.progressAmount / loader.progressTotal) * 100).toFixed(3);

    if (currentProgress !== progress) {
      progress = currentProgress;
      if (report) {
        report({
          totalBitSize: loader.progressTotal,
          progressPercent: progress,
          progressBits: loader.progressAmount
        });
      }
    }
  };
}

exports.download = function(remotePath, localPath, reportProgress) {
  return new Promise(function(resolve, reject) {
    var progress = 0,
      downloader = s3client.downloadFile({
        localFile: localPath,
        s3Params: {
          Bucket: objectstorageConfig.bucketname,
          Key: remotePath
        }
      });
    downloader.on('error', function(err) {
      reject(err);
    });
    downloader.on('progress', progressCalculator(downloader, progress, 'download', reportProgress));
    downloader.on('end', function() {
      resolve();
    });
  });
};

exports.upload = function(localPath, remotePath, ContentDisposition, reportProgress) {
  return new Promise(function(resolve, reject) {
    var progress = 0,
      uploader = s3client.uploadFile({
        localFile: localPath,
        s3Params: {
          Bucket: objectstorageConfig.bucketname,
          Key: remotePath,
          ContentDisposition 
        }
      });
    uploader.on('error', function(err) {
      reject(err);
    });
    uploader.on('progress', progressCalculator(uploader, progress, 'upload', reportProgress));
    uploader.on('end', function(result) {
      resolve({
        etag: result.ETag
      });
    });
  });
};
