'use strict';

var createCustomError = require('custom-error-generator'),
  SourceValidationError = createCustomError('SourceValidationError', {
    type: '',
    data: {}
  }),
  ExecBinaryError = createCustomError('ExecBinaryError'),
  StorageError = createCustomError('StorageError');

exports.types = {
  SourceValidationError: SourceValidationError
};

exports.SourceValidationError = function (message, type, data, previousErrors) {
  var error = new SourceValidationError(message, previousErrors);

  error.type = type;
  error.data = data;
  return error;
};

exports.ExecBinaryError = function(message, data) {
  var error = new ExecBinaryError(message);

  error.data = data;
  return error;
};

exports.StorageError = function(message) {
  return new StorageError(message);
};