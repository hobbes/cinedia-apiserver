'use strict';

var Promise = require('bluebird'),
  fs = require('fs-extra'),
  path = require('path'),
  os = require('os'),
  child_process = require('child_process'),
  log = require('../shared/logger'),
  errorLib = require('./error'),
  SourceValidationError = errorLib.SourceValidationError,
  ExecBinaryError = errorLib.ExecBinaryError,
  copy = Promise.promisify(fs.copy),
  writeFile = Promise.promisify(fs.writeFile),
  readdir = Promise.promisify(fs.readdir),
  execFile = child_process.execFile;

const LUFS = -16,
  TP = -1.5,
  LRA = 11;

function execPromise(command, args, options, returnStderr) {
  return new Promise(function (resolve, reject) {
    execFile(command, args, options, function (err, stdout, stderr) {
      if (err) {
        reject(ExecBinaryError(`command ${command} failed`, {
          command: command,
          args: args,
          stdout: stdout,
          stderr: stderr
        }, err));
      } else {
        if (returnStderr) {
          resolve(stderr);
        } else {
          resolve(stdout);
        }
      }
    });
  });
}

function contains (array, item) {
  if (array.indexOf(item) !== -1) {
    return true;
  }
  return false;
}

function watchFolderForNewFiles(folder, report) {
  return fs.watch(folder, (eventType) => {
    if (eventType === 'rename') {
      report();
    }
  });
}

exports.validateStill = function(inputFile, supportedFiletypes) {
  function gmIdentify(file) {
    function includes(string, search, start) {
      if (typeof start !== 'number') {
        start = 0;
      }

      if (start + search.length > string.length) {
        return false;
      } else {
        return string.indexOf(search, start) !== -1;
      }
    }
    return execPromise('gm', [
      'identify',
      '-format', '{"width": %w, "height": %h, "magick": "%m"}',
      file
    ]).then(function(result){
      return JSON.parse(result);
    }).catch(function(error) {
      if (includes(String(error), 'gm identify: Request did not return an image')) {
        throw SourceValidationError('Source is not a image.', 'sourceIsNotImage', {
          source: file
        }, error);
      } else {
        throw error;
      }
    });
  }
  return gmIdentify(inputFile)
    .then(function(gmIdentifyResult) {
      if (!contains(supportedFiletypes, gmIdentifyResult.magick)) {
        throw SourceValidationError('Filetype is not supported.', 'unsupportedFiletype', {
          supportedFiletypes: supportedFiletypes,
          filetype: gmIdentifyResult.magick
        });
      }
      return gmIdentifyResult;
    });
};

exports.validateVideo = function(inputFile, inputFileShouldHaveAudio, supportedFps, probeThreshold) {
  function ffprobe(file) {
    return execPromise('ffprobe', [
      '-v', 'quiet',
      '-print_format', 'json',
      '-show_format',
      '-show_streams',
      file
    ])
      .then(function(result){
        return JSON.parse(result);
      })
      .catch(function(error){
        if (error.code === 1) {
          throw SourceValidationError('File is not readable.', 'fileIsNotReadable', {
            source: file
          }, error);
        } else {
          throw error;
        }
      });
  }
  function ffmpegLoudNormAnalyze(file) {
    return execPromise('ffmpeg', [
      '-nostats',
      '-hide_banner',
      '-i', file,
      '-af', `loudnorm=I=${LUFS}:TP=${TP}:LRA=${LRA}:print_format=json`,
      '-f', 'null',
      '-'
    ], null, true)
      .then(function(result) {
        var lines = result.split('\n'),
          jsonLines = lines.slice(-13),
          jsonString = jsonLines.join('');

        if (jsonLines[0] !== '{') {
          return {};
        } else {
          return JSON.parse(jsonString);
        }
      });

  }
  function loudNormResultIsNotValid (input) {
    function isInfinite(value) {
      return value === 'inf' || value === '-inf';
    }
    return isInfinite(input.input_i) ||
      isInfinite(input.input_tp) ||
      isInfinite(input.output_i) ||
      isInfinite(input.output_tp) ||
      isInfinite(input.target_offset);
  }
  return Promise.join(ffprobe(inputFile), ffmpegLoudNormAnalyze(inputFile), function (ffprobeResult, loudnormResult) {
    var videoStream,
      audioStream,
      video = {},
      audio = {},
      hasAudio = false,
      hasVideo = false;

    log.debug('ffprobeResult', ffprobeResult);
    log.debug('loudnormResult', loudnormResult);
    if (ffprobeResult.format.probe_score < probeThreshold) {
      throw SourceValidationError('Filetype detection is not reliable.', 'filetypeDetectionNotReliable', {
        source: inputFile
      });
    }

    ffprobeResult.streams.forEach(function(stream){
      if (stream.codec_type === 'video') {
        videoStream = stream;
      }
      if (stream.codec_type === 'audio') {
        audioStream = stream;
      }
    });

    if (videoStream) {
      hasVideo = true,
      video.index = videoStream.index;
      video.width = videoStream.width;
      video.height = videoStream.height;
      video.fps = eval(videoStream.r_frame_rate);
      video.duration = Number(videoStream.duration);
    }
    if (audioStream) {
      hasAudio = true;
      audio.warnings = {};
      audio.index = audioStream.index;
      audio.fps = eval(videoStream.r_frame_rate);
      audio.duration = Number(audioStream.duration);
      audio.channels = audioStream.channels;
      audio.channel_layout = audioStream.channel_layout;
      audio.loudnorm = loudnormResult;

      if (audio.channel_layout !== 'stereo' && audio.channel_layout !== '5.1') {
        audio.warnings.channelLayout = true;
      }
    }

    if (!hasVideo) {
      throw SourceValidationError('Missing videostream.', 'missingVideostream', {});
    }
    if (inputFileShouldHaveAudio && !hasAudio) {
      throw SourceValidationError('Missing audiostream.', 'missingAudiostream', {});
    }
    if (inputFileShouldHaveAudio && loudNormResultIsNotValid(loudnormResult)) {
      throw SourceValidationError('Audio volume out of range.', 'audioVolumeOutOfRange', {});
    }
    if (!contains(supportedFps, video.fps)) {
      throw SourceValidationError('Unsupported Framerate.', 'unsupportedFramerate', {
        supportedFps: supportedFps,
        fps: video.fps
      });
    }

    return {
      video: video,
      audio: audio,
      meta: {
        ffprobeResult: ffprobeResult
      }
    };
  });
};

exports.preview = function (inputFile, outputFile, width, height) {
  return execPromise('gm', [
    'convert',
    '-size', `${width}x${height}`,
    inputFile,
    '-thumbnail', `${width}x${height}`,
    '-type', 'TrueColor',
    '-quality', '85',
    'jpg:' + outputFile
  ]);
};

exports.thumbnail = function (inputFile, outputFile, size) {
  return execPromise('gm', [
    'convert',
    '-size', size,
    inputFile,
    '-strip',
    '-thumbnail', `${size}^`,
    '-gravity', 'Center',
    '-background', 'black',
    '-extent', `${size}^`,
    '+matte',
    '-type', 'TrueColor',
    '-quality', '70',
    `jpg:${outputFile}`
  ]);
};

exports.stillConvert = function (inputFile, outputFile, width, height) {
  return execPromise('gm', [
    'convert',
    '-size', `${width}x${height}`,
    inputFile,
    '-compress', 'none',
    '-resize', `${width}x${height}`,
    '-depth', '12',
    '-gravity', 'Center',
    '-background', 'black',
    '-extent', `${width}x${height}`,
    '+matte',
    '-type', 'TrueColor',
    `tif:${outputFile}`
  ]);
};

exports.batchExtentToFlat = function(inputDir, totalFrames, reportProgress) {
  var watcher,
    moddedFiles = 0,
    totalFileModifications = totalFrames * 4;

  watcher = watchFolderForNewFiles(inputDir, () => {
    let progress = 0;

    moddedFiles = moddedFiles + 1;
    progress = Number((moddedFiles / totalFileModifications * 100).toFixed(3));

    log.debug(`batchExtentToFlat progress: ${progress}`);
    if (reportProgress && progress <= 100) {
      reportProgress(progress);
    }
  });

  return execPromise('gm', [
    'mogrify',
    '-resize', '1998x1080',
    '-gravity', 'Center',
    '-background', 'black',
    '-extent', '1998x1080',
    '+matte',
    '*.tiff'
  ], {
    cwd: inputDir
  }).tap(() => {
    watcher.close();
  });
};

exports.createTiles = function(sourceFileFolder, tileOutputFile, numberOfTiles, height) {
  return readdir(sourceFileFolder)
    .then(function(files) {
      var totalFiles = files.length,
        everyXthFile = Number((totalFiles / numberOfTiles).toFixed());

      return files.reduce(function(tileFiles, currentFile, index) {
        if (index % everyXthFile === 0) {
          tileFiles.push(path.join(sourceFileFolder, currentFile));
        }
        return tileFiles;
      }, []);
    })
    .then(function(tileFiles) {
      var args = [
        'montage',
        '-tile', `${numberOfTiles}x1`,
        '-geometry', `^x${height}`,
        '-quality', '70'
      ];

      tileFiles.forEach(function(file) {
        args.push(file);
      });
      args.push(tileOutputFile);

      return execPromise('gm', args);
    });
};

exports.opendcp_j2k = function(inputDir, outputDir, maxBandwith, totalFrames, reportProgress) {
  var watcher,
    threads = os.cpus().length,
    addedFiles = 0;

  if (totalFrames) {
    watcher = watchFolderForNewFiles(outputDir, () => {
      let progress = 0;

      addedFiles = addedFiles + 1;
      progress = progress = Number((addedFiles / totalFrames * 100).toFixed(3));

      log.debug(`opendcp_j2k progress: ${progress}`);
      if (reportProgress && progress <= 100) {
        reportProgress(progress);
      }
    });
  }
    
  return execPromise('opendcp_j2k', [
    '-i', inputDir,
    '-o', outputDir,
    '-r', 25,
    '--bw', maxBandwith,
    '--profile', 'cinema2k',
    '--threads', threads,
    '-l', 0
  ]).tap(() => {
    if (watcher) {
      watcher.close();
    }
  });
};

exports.opendcp_mxf = function(inputFile, outputFile, fps) {
  return execPromise('opendcp_mxf', [
    '-i', inputFile,
    '-o', outputFile,
    '-r', fps,
    '-n', 'smpte',
    '-l', 0
  ]);
};

exports.opendcp_xml = function(dir, pictureMxfFile, soundMxfFile, issuer, annotation, title) {
  var reelArgs = ['--reel', pictureMxfFile],
    infoArgs = [
      '--issuer', issuer,
      '--annotation', annotation,
      '--title', title,
      '--kind', 'ADVERTISEMENT',
      '-l', 0
    ],
    args;

  if (soundMxfFile) {
    reelArgs.push(soundMxfFile);
  }

  args = reelArgs.concat(infoArgs);

  return execPromise('opendcp_xml', args, {
    cwd: dir
  });
};

exports.duplicate = function(inputFile, outputDir, frames, extension) {
  var frameCounter = 1,
    copies = [],
    padding = String(frames).length;

  function digits(n, p) {
    var pad_char = '0',
      pad = Array(1 + p).join(pad_char);
    return (pad + n).slice(-pad.length);
  }

  while (frameCounter <= frames) {
    copies.push(copy(inputFile, path.join(outputDir, 'frame' + digits(frameCounter, padding) + '.' + extension)));
    frameCounter = frameCounter + 1;
  }

  return Promise.all(copies);
};

exports.ffmpegExtractSequence = function(inputFile, outputDir, totalFrames, reportProgress) {
  let watcher,
    addedFiles = 0;

  watcher = watchFolderForNewFiles(outputDir, () => {
    let progress = 0;

    addedFiles = addedFiles + 1;
    progress = Number((addedFiles / totalFrames * 100).toFixed(3));

    log.debug(`ffmpegExtractSequence progress: ${progress}`);
    if (reportProgress && progress <= 100) {
      reportProgress(progress);
    }
  });

  return execPromise('ffmpeg', [
    '-i',  inputFile,
    path.join(outputDir, 'frame%06d.tiff')
  ]).tap(() => {
    watcher.close();
  });
};

exports.generateMP4Preview = function(inputFile, outputFile, width, height, hasAudio, ln) {
  var widthDivBy2 = Number((width / 2).toFixed() * 2),
    heightDivBy2 = Number((height / 2).toFixed() * 2),
    inputArgs = ['-i', inputFile],
    // loudnormFilter,
    optionArgs = [
      '-vcodec', 'libx264',
      '-pix_fmt', 'yuv420p',
      '-strict', '-2',
      '-y',
      '-loglevel', 'error',
      '-vf', `scale=width=${widthDivBy2}:height=${heightDivBy2}:force_original_aspect_ratio=1,pad=width=${widthDivBy2}:height=${heightDivBy2}:x=(ow-iw)/2:y=(oh-ih)/2`,
      '-vprofile', 'baseline',
      '-level', '3'
    ],
    // outPutArgs = [outputFile],
    args;

  if (hasAudio) {
    // loudnormFilter = `loudnorm=I=${LUFS}:TP=${TP}:LRA=${LRA}:measured_I=${ln.input_i}:measured_LRA=${ln.input_lra}:measured_TP=${ln.input_tp}:measured_thresh=${ln.input_thresh}:offset=${ln.target_offset}:linear=true`;
    // optionArgs.push('-acodec');
    // optionArgs.push('aac');
    // optionArgs.push('-af');
    // optionArgs.push(loudnormFilter);


    let audioOptionArgs = [
      '-acodec', 'aac',
      '-af', `loudnorm=I=${LUFS}:TP=${TP}:LRA=${LRA}:measured_I=${ln.input_i}:measured_LRA=${ln.input_lra}:measured_TP=${ln.input_tp}:measured_thresh=${ln.input_thresh}:offset=${ln.target_offset}:linear=true`
    ];
    optionArgs = [...optionArgs, ...audioOptionArgs];
  } else {
    // optionArgs.push('-an');
    optionArgs = [...optionArgs, ...['-an']];
  }

  // args = inputArgs.concat(optionArgs).concat(outPutArgs);
  args = [...inputArgs, ...optionArgs, outputFile];
  return execPromise('ffmpeg', args);
};

// exports.generateVP9Preview = function(inputFile, outputFile, width, height, hasAudio) {
// return execPromise('ffmpeg', [
//     '-i', inputFile
// ]).then(function(){
//     var widthDivBy2 = Number((width / 2).toFixed() * 2),
// heightDivBy2 = Number((height / 2).toFixed() * 2), 
//     inputArgs = ['-i', inputFile],
//         optionArgs = [
//             '-vcodec', 'libx264',
//             '-pix_fmt', 'yuv420p',
//             '-strict', '-2',
//             '-y',
//             '-loglevel', 'error',
//             '-vf', 'scale=width=' + width + ':height=' + height + ':force_original_aspect_ratio=1,pad=width=' + width + ':height=' + height + ':x=(ow-iw)/2:y=(oh-ih)/2',
//             '-vprofile', 'baseline',
//             '-level', '3'
//         ],
//         outPutArgs = [outputFile],
//         args;

//     if (hasAudio) {
//         optionArgs.push('-acodec');
//         optionArgs.push('aac');
//     } else {
//         optionArgs.push('-an');
//     }

//     args = inputArgs.concat(optionArgs).concat(outPutArgs);
//     return execPromise('ffmpeg', args);
// });
// };

exports.ffmpegExtractWav = function(inputFile, outputFile, duration, totSamples, tempo, ln) {
  var padFilter = `apad=whole_len=${totSamples}`,
    loudnormFilter = `loudnorm=I=${LUFS}:TP=${TP}:LRA=${LRA}:measured_I=${ln.input_i}:measured_LRA=${ln.input_lra}:measured_TP=${ln.input_tp}:measured_thresh=${ln.input_thresh}:offset=${ln.target_offset}:linear=true`,
    aFilterString = `${loudnormFilter}, ${padFilter}`;

  if (tempo) {
    aFilterString = `${aFilterString}, atempo=${tempo}`;
  }

  return execPromise('ffmpeg', [
    '-i', inputFile,
    '-af', aFilterString,
    '-channel_layout', '5.1',
    '-acodec', 'pcm_s24le',
    '-ab', '25000',
    '-ar', '48000',
    '-t', duration,
    outputFile
  ]);
};

exports.generateSilentWav = function(outputFile, duration) {
  // duration is in seconds
  log.debug('generateSilentWav outputFile:', outputFile);
  log.debug('duration: ', duration);
  return execPromise('ffmpeg', [
    '-f', 'lavfi',
    '-i', 'anullsrc=channel_layout=5.1',
    '-ab', '25000',
    '-ar', '48000',
    '-acodec', 'pcm_s24le',
    '-t', duration, 
    outputFile
  ]);
};

exports.concatWav = function(inputFileArray, txtFile, silenceFile, outputFile) {
  var fileString = '';

  inputFileArray.forEach(function(inputFile){
    fileString = fileString + 'file \'' + inputFile + '\'\n';
    fileString = fileString + 'file \'' + silenceFile + '\'\n';
  });

  log.debug('concat txtfile: ', txtFile);
  log.debug('silencefile: ', silenceFile);
  log.debug('concat txt: ', fileString);
  return writeFile(txtFile, fileString)
    .then(function(){
      return execPromise('ffmpeg', [
        '-f', 'concat',
        '-safe', '0',
        '-i', txtFile,
        '-c', 'copy',
        outputFile
      ]);
    });
};

exports.sequence = function(inputDirs, blackFramefile, spaceFrames, outputDir) {
  function frameFilename(frameNumber, p) {
    function digits(n) {
      var pad_char = '0',
        pad = Array(1 + p).join(pad_char);
      return (pad + n).slice(-pad.length);
    }
    return path.join('frame' + digits(frameNumber) + '.j2c');
  }

  return Promise.map(inputDirs, function(folder){
    return readdir(folder).then(function(files){
      return {
        name: folder,
        files: files
      };
    });
  })
    .then(function(folders){
      var paddingCounter = 0,
        paddingDigits = 0,
        frameCounter = 0,
        copies = [];

      folders.forEach(function(folder) {
        paddingCounter = paddingCounter + folder.files.length + spaceFrames;
      });
      paddingDigits = String(paddingCounter).length;

      folders.forEach(function(folder){
        var blackFrameCounter = 0;

        folder.files.forEach(function(file){
          if (path.extname(file) === '.j2c') {
            frameCounter = frameCounter + 1;
            copies.push(copy(path.join(folder.name, file), path.join(outputDir, frameFilename(frameCounter, paddingDigits))));
          }
        });

        while (blackFrameCounter < spaceFrames) {
          frameCounter = frameCounter + 1;
          copies.push(copy(blackFramefile, path.join(outputDir, frameFilename(frameCounter, paddingDigits))));
          blackFrameCounter = blackFrameCounter + 1;
        }
      });

      return Promise.all(copies)
        .then(() => {
          return frameCounter;
        });
    });
};

exports.entar = function(inputDir, outputFile) {
  return execPromise('tar', [
    '-cf', outputFile,
    '-C', inputDir, '.'
  ]);
};

exports.zip = function(inputDir, outputFile) {
  return execPromise('zip', [
    '-r', '-0',
    outputFile,
    '.'
  ], {
    cwd: inputDir
  });
};

exports.detar = function(inputFile, outputDir) {
  return execPromise('tar', [
    '-xf', inputFile,
    '-C', outputDir
  ]);
};
