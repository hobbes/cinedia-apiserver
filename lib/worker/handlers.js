'use strict';

var Promise = require('bluebird'),
  path = require('path'),
  os = require('os'),
  fs = require('fs-extra'),
  ftp = require('basic-ftp'),
  config = require('config'),
  log = require('../shared/logger'),
  SourceValidationErrorType = require('./error').types.SourceValidationError,
  mkdirs = Promise.promisify(fs.mkdirs),
  emptyDir = Promise.promisify(fs.emptyDir),
  remove = Promise.promisify(fs.remove),
  stat = Promise.promisify(fs.stat),
  newUniqueKey = require('../shared/storage').newUniqueKey,
  dcputil = require('./dcputil'),
  storage = require('./storage'),
  osTmpDir = config.get('tmpDir') || os.tmpdir(),
  WORK_DIR = path.join(osTmpDir, 'cinediaworker'),
  BLACK_FRAME_FILE = path.join(__dirname, '../../files', 'black.j2c');

const DIA_VALIDATE_TASKNAME = config.get('queue.jobnames.dia.validate'),
  DIA_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.dia.intermediate'),

  SPOTMUTE_VALIDATE_TASKNAME = config.get('queue.jobnames.spotmute.validate'),
  SPOTMUTE_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotmute.intermediate'),

  SPOTSOUND_VALIDATE_TASKNAME = config.get('queue.jobnames.spotsound.validate'),
  SPOTSOUND_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotsound.intermediate'),

  BLOCK_DCP_TASKNAME = config.get('queue.jobnames.block.dcp.render');

function createNewUniqueWorkFolderPath() {
  return path.join(WORK_DIR, newUniqueKey());
}

function uploadGeneratedFiles(report) {
  return function(dcpUtilResult) {
    return Promise.reduce(dcpUtilResult.files, (acc, localFile) => {
      return stat(localFile.path).then((statObj) => {
        return acc + statObj.size;
      });
    }, 0).then((totalSize) => {
      let fileCounter = [],
        index = 0;

      function countBits() {
        return fileCounter.reduce((acc, current) => {
          return acc + current;
        }, 0);
      }
      return Promise.map(dcpUtilResult.files, function(localFile){
        var remoteFilepath = newUniqueKey(path.extname(localFile.path));
        fileCounter[index] = 0;

        return storage.upload(localFile.path, remoteFilepath, null, ({progressBits}) => {
          fileCounter[index] = progressBits;
          report(((countBits() / totalSize) * 100).toFixed(3));
        })
          .then(function(remoteFile){
            delete localFile.path;
            localFile.key = remoteFilepath;
            localFile.etag = remoteFile.etag;
            index = index + 1;
            return localFile;
          });
      }, {
        concurrency: 1
      })
        .then(function(remoteFiles){
          return {
            metadata: dcpUtilResult.metadata,
            files: remoteFiles
          };
        });
    });
  };
}

function addSujetIdToResult(sujetId) {
  return function (result) {
    result.sujetId = sujetId;
    return result;
  };
}

function markResultSuccess(result) {
  result._error = false;
  return result;
}

function cleanupWorkspace(dir) {
  return function () {
    return remove(dir).catch(log.error);
  };
}

function crashOnENOMEM(error) {
  if (error.code === 'ENOMEM') {
    log.error(error);
    log.error(error.stack);
    process.exit(1);
  } else {
    throw error;
  }
}

function handleSourceValidationError(data) {
  return function(error) {
    var result = data;

    result._error = true;
    result.error = {
      message: error.message,
      type: error.type,
      data: error.data
    };
    return result;
  };
}

function handleUnknownError(data) {
  return function(error) {
    var result = data;

    log.error(error);
    result._error = true;
    result.error = {
      type: 'unknown'
    };
    return result;
  };
}

exports.cleanUpOnStartup = function () {
  return emptyDir(WORK_DIR);
};

function calcSteps(start, end, progress) {
  let range = end - start;

  return start + (range / 100 * progress);
}

exports[DIA_VALIDATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localDiaSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir).then(function() {
    reportProgress(2);
    return storage.download(data.source, localDiaSourceFile, ({progressPercent}) => {
      reportProgress(calcSteps(2, 30, progressPercent));
    });
  }).then(function() {
    reportProgress(30);
    return dcputil.validateDia(localDiaSourceFile, (progress) => {
      reportProgress(calcSteps(30, 75, progress));
    });
  })
    .tap(function () {
      reportProgress(95);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[SPOTMUTE_VALIDATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir).then(function() {
    reportProgress(2);
    return storage.download(data.source, localSourceFile, ({progressPercent}) => {
      reportProgress(calcSteps(2, 30, progressPercent));
    });
  }).then(function() {
    reportProgress(30);
    return dcputil.validateSpotMute(localSourceFile, (progress) => {
      reportProgress(calcSteps(30, 75, progress));
    });
  })
    .tap(function () {
      reportProgress(95);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[SPOTSOUND_VALIDATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir).then(function() {
    reportProgress(2);
    return storage.download(data.source, localSourceFile, ({progressPercent}) => {
      reportProgress(calcSteps(2, 30, progressPercent));
    });
  }).then(function() {
    reportProgress(30);
    return dcputil.validateSpotSound(localSourceFile, (progress) => {
      reportProgress(calcSteps(30, 75, progress));
    });
  })
    .tap(function () {
      reportProgress(95);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[DIA_INTERMEDIATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir).then(function() {
    reportProgress(2);
    return storage.download(data.source, localSourceFile, ({progressPercent}) => {
      reportProgress(calcSteps(2, 30, progressPercent));
    });
  }).then(function() {
    reportProgress(30);
    return dcputil.createDiaIntermediate(localSourceFile, data.duration, currentWorkDir, (progress) => {
      reportProgress(calcSteps(30, 75, progress));
    });
  }).tap(function () {
    reportProgress(75);
  }).then(uploadGeneratedFiles((progress) => {
    reportProgress(calcSteps(75, 95, progress));
  }))
    .tap(function () {
      reportProgress(95);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[SPOTMUTE_INTERMEDIATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localSpotMuteSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir)
    .then(function() {
      return storage.download(data.source, localSpotMuteSourceFile, ({progressPercent}) => {
        reportProgress(calcSteps(2, 10, progressPercent));
      });
    })
    .then(function() {
      return dcputil.createSpotMuteIntermediate(localSpotMuteSourceFile, currentWorkDir, (progress) => {
        reportProgress(calcSteps(10, 75, progress));
      });
    })
    .then(uploadGeneratedFiles((progress) => {
      reportProgress(calcSteps(75, 90, progress));
    }))
    .tap(function () {
      reportProgress(90);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[SPOTSOUND_INTERMEDIATE_TASKNAME] = function(data, reportProgress) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    localSpotSoundSourceFile = path.join(currentWorkDir, path.basename(data.source));

  reportProgress(0);
  return mkdirs(currentWorkDir)
    .then(function() {
      return storage.download(data.source, localSpotSoundSourceFile, ({progressPercent}) => {
        reportProgress(calcSteps(2, 10, progressPercent));
      });
    })
    .then(function() {
      return dcputil.createSpotSoundIntermediate(localSpotSoundSourceFile, currentWorkDir, (progress) => {
        reportProgress(calcSteps(10, 75, progress));
      });
    })
    .then(uploadGeneratedFiles((progress) => {
      reportProgress(calcSteps(75, 92, progress));
    }))
    .tap(function () {
      reportProgress(92);
    })
    .then(addSujetIdToResult(data.sujetId))
    .tap(function () {
      reportProgress(100);
    })
    .then(markResultSuccess)
    .catch(crashOnENOMEM)
    .catch(SourceValidationErrorType, handleSourceValidationError(data))
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};

exports[BLOCK_DCP_TASKNAME] = function(data) {
  var currentWorkDir = createNewUniqueWorkFolderPath(),
    fps = data.fps,
    kw = data.blockinfo.kw,
    uploadFTP = data.uploadFTP || false,
    muteDcpWorkdir = path.join(currentWorkDir, 'workdirmute'),
    soundDcpWorkdir = path.join(currentWorkDir, 'workdirsound'),
    archivedir = path.join(currentWorkDir, 'archive'),
    localZipFile = path.join(currentWorkDir, 'archive.zip'),
    dcpRemoteKey = newUniqueKey('.zip');

  function downloadIntermediates(intermediates, localDir) {
    return Promise.map(intermediates,
      function(intermediateRemotePath) {
        var foldername = path.basename(intermediateRemotePath, path.extname(intermediateRemotePath)),
          extractFolder = path.join(localDir, foldername),
          intermediatesLocalPath = path.join(localDir, path.basename(intermediateRemotePath));

        return storage.download(intermediateRemotePath, intermediatesLocalPath)
          .then(function() {
            return mkdirs(extractFolder);
          })
          .then(function() {
            return dcputil.detar(intermediatesLocalPath, extractFolder);
          })
          .then(function() {
            return extractFolder;
          });
      }, {
        concurrency: 1
      }
    );
  }

  function createMuteDcp() {
    var archivedirMute;

    if (!data.segments.mute) {
      return;
    }
    archivedirMute = path.join(archivedir, data.segments.mute.name);
    return Promise.all([
      mkdirs(muteDcpWorkdir),
      mkdirs(archivedirMute)
    ]).then(function() {
      return downloadIntermediates(data.segments.mute.keys, muteDcpWorkdir);
    }).then(function(localIntermediates){
      var dcpInfo = {
          issuer: data.issuer,
          annotation: data.segments.mute.name,
          title: data.segments.mute.name
        },
        spaceFrames = data.spaceFrames;

      return dcputil.createBlockMute(localIntermediates, archivedirMute, dcpInfo, BLACK_FRAME_FILE, fps, spaceFrames, muteDcpWorkdir);
    });
  }

  function createSoundDcp() {
    var archivedirSound;

    if (!data.segments.audio) {
      return;
    }
    archivedirSound = path.join(archivedir, data.segments.audio.name);
    return Promise.all([
      mkdirs(soundDcpWorkdir),
      mkdirs(archivedirSound)
    ])
      .then(function() {
        return downloadIntermediates(data.segments.audio.keys, soundDcpWorkdir);
      })
      .then(function(localIntermediates){
        var dcpInfo = {
            issuer: data.issuer,
            annotation: data.segments.audio.name,
            title: data.segments.audio.name
          },
          spaceFrames = data.spaceFrames;

        return dcputil.createBlockSound(localIntermediates, archivedirSound, dcpInfo, BLACK_FRAME_FILE, fps, spaceFrames, soundDcpWorkdir);
      });
  }

  return mkdirs(currentWorkDir)
    .then(function() {
      return mkdirs(archivedir);
    })
    .then(createMuteDcp)
    .then(createSoundDcp)
    .then(function() {
      return dcputil.zip(archivedir, localZipFile);
    })
    .then(async () => {
      async function uploadToFTPServer(companyNameShort, localFolder, remoteFolder, attempt) {
        try {
          const client = new ftp.Client();

          client.ftp.verbose = config.get('env') !== 'production';
          await client.access({
            host: config.ftp.host,
            user: config.ftp.user,
            password: config.ftp.password,
            secure: false
          });
          await client.ensureDir(companyNameShort);

          const filelist = await client.list();

          for (const fileinfo of filelist) {
            let name = fileinfo.name,
              index = name.indexOf('KW'),
              number = Number(name.substring(index + 2, index + 4));

            log.debug(name);
            log.debug(number);
            log.debug(fileinfo.type, number, kw);
            if (fileinfo.type === 2 && number && number < kw - 1) {
              await client.removeDir(name);
            }
          }

          await client.uploadFromDir(localFolder, remoteFolder);
          await client.close();
        } catch(error) {
          if (attempt < 3) {
            log.error(error);
            await uploadToFTPServer(data.blockinfo.companyNameShort, archivedir, data.ftpFolderName, attempt + 1);
          } else {
            throw error;
          }
        }
      }
      if (uploadFTP) {
        await uploadToFTPServer(data.blockinfo.companyNameShort, archivedir, data.ftpFolderName, 0);
      } 
    })
    .then(function() {
      return storage.upload(localZipFile, dcpRemoteKey, `attachment; filename="${data.name}.zip"`);
    })
    .then(function(sosUpload){
      return {
        _error: false,
        blockId: data.blockId,
        blockhash: data.blockhash,
        key: dcpRemoteKey,
        etag: sosUpload.etag,
        name: data.name,
        blockinfo: data.blockinfo,
        uploadFTP: data.uploadFTP
      };
    })
    .catch(crashOnENOMEM)
    .catch(handleUnknownError(data))
    .finally(cleanupWorkspace(currentWorkDir));
};