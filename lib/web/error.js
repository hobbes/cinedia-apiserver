'use strict';

var TypedError = require("error/typed"),
  WrappedError = require('error/wrapped');

exports.ClientError = TypedError({
  type: 'client.4xx',
  message: '{title} client error, status={statusCode}',
  title: null,
  statusCode: null
});

exports.ServerError = TypedError({
  type: 'server.5xx',
  message: '{title} server error, status={statusCode}',
  title: null,
  statusCode: null
});

exports.WrappedServerError = WrappedError({
  message: '{title} server error, status={statusCode}',
  type: 'server.5xx',
  title: null,
  statusCode: null
});