'use strict';

var config = require('config'),
  moment = require('moment'),
  db = require('../db'),
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  util = require('./util/util'),
  globalLimit = config.get('model.limit'),
  dateslotPropMap;

dateslotPropMap = {
  'dateslot_id': 'dateslotId'
};

function buildDateslotModel(row) {
  util.encodeRowIdProperties(row, ['dateslot_id']);
  util.renameRowProperties(row, dateslotPropMap);

  return row;
}
        
exports.now = function () {
  return db.one(sql.dateslotsLoadNow)
    .then(buildDateslotModel);
};

exports.get = function (dateslotIdEnc) {
  var dateslotId = hashids.decode(dateslotIdEnc);

  return db.one(sql.dateslotsGet, {
    dateslotId: dateslotId
  })
    .then(buildDateslotModel)
    .catch(QueryResultError, function(error) {
      if (error.code === qrec.noData) {
        throw ModelNotFoundError({
          model: 'dateslot',
          prop: 'id',
          id: dateslotId
        });
      }
    });
};

exports.find = function (startdate, enddate) {
  return db.manyOrNone(sql.dateslotsManyByStartEndDate, {
    startdate: moment(startdate, 'YYYYMMDD').toDate(),
    enddate: moment(enddate, 'YYYYMMDD').toDate(),
    limit: globalLimit
  })
    .map(buildDateslotModel);
};

exports.findMultiple = function (dateslotIdsEnc) {
  var dateslotIds = dateslotIdsEnc.map(function (idEnc) {
    return hashids.decode(idEnc);
  });

  return db.manyOrNone(sql.dateslotsMany, {
    dateslotIds: dateslotIds,
    limit: globalLimit
  })
    .map(buildDateslotModel);
};