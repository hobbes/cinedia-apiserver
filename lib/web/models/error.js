'use strict';

var TypedError = require('error/typed'),
  WrappedError = require('error/wrapped');

module.exports.ModelError = WrappedError({
  type: 'model.error',
  message: 'Model {model} error',
  model: null
});

module.exports.UnkownModelError = WrappedError({
  type: 'unkown.model.error',
  message: 'Unknown {model}-model error',
  model: null
});

module.exports.ModelNotFoundError = TypedError({
  type: 'model.notFound',
  message: 'No {model} item with {prop}: {id} found.',
  model: null,
  prop: null,
  id: null
});

module.exports.ModelUpdateError = TypedError({
  type: 'model.update',
  message: 'No {model} item with {prop}: {id} found.',
  model: null,
  prop: null,
  id: null
});

module.exports.ModelCreateError = TypedError({
  type: 'model.insert',
  message: 'No {model} created.',
  model: null
});

module.exports.DateSlotBookError = TypedError({
  type: 'dateslot.book.impossible',
  message: 'Booking of sujet: {sujetId} in stage: {stageId} at date: {dateslotId} not possible',
  sujetId: null,
  stageId: null,
  dateslotId: null
});

module.exports.BlockSujetMoveError = TypedError({
  type: 'blocksujet.move',
  message: 'Error reordering block: {blockId}',
  blockId: null
});