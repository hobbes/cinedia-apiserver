'use strict';

var db = require('../db'),
  sql = require('./sql');

exports.getAllStorageKeys = function () {
  return db.manyOrNone(sql.storageKeyAll)
    .map(function(row){
      return row.storagekey;
    });
};