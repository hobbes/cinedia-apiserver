'use strict';

var config = require('config'),
  loudnorm = config.get('loudnorm'),
  jobqueue = require('../queueJob'),
  db = require('../db'),
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  ModelNotFoundError = require('./error').ModelNotFoundError,
  ModelCreateError = require('./error').ModelNotFoundError,
  sql = require('./sql'),
  hashids = require('./util/hashids');

const DIA_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.dia.intermediate'),
  SPOTMUTE_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotmute.intermediate'),
  SPOTSOUND_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotsound.intermediate');

exports.addPreview = function(sujetIdEnc, preview) {
  return db.result(sql.previewCreate, {
    sujetId: hashids.decode(sujetIdEnc),
    previewType: preview.previewType,
    previewFormat: preview.previewFormat,
    size: preview.size,
    storagekey: preview.key,
    etag: preview.etag
  }).then(function(result) {
    if (result.rowCount !== 1) {
      throw ModelCreateError({
        model: 'sujetPreview'
      });
    }
    return;
  });
};

exports.addValidationSuccess = function(sujetIdEnc) {
  return db.tx((t) => {
    return t.one(sql.sujet.updateValidationResultSuccess, {
      sujetId: hashids.decode(sujetIdEnc),
    }).then(function(result) {
      var sujetId = result.sujet_id,
        taskData = {
          sujetId: sujetIdEnc,
          source: result.storagekey_sourcefile
        };

      if (result.type === 'dia') {
        taskData.duration = result.duration;
        return jobqueue.newJob(DIA_INTERMEDIATE_TASKNAME, taskData)
          .then(function(jobId){
            return {
              jobId,
              sujetId
            };
          });
      }
      if (result.type === 'spot_mute') {
        return jobqueue.newJob(SPOTMUTE_INTERMEDIATE_TASKNAME, taskData)
          .then(function(jobId){
            return {
              jobId,
              sujetId
            };
          });
      }
      if (result.type === 'spot_sound') {
        taskData.loudnorm = loudnorm;
        return jobqueue.newJob(SPOTSOUND_INTERMEDIATE_TASKNAME, taskData)
          .then(function(jobId){
            return {
              jobId,
              sujetId
            };
          });
      }
    }).then(function({jobId, sujetId}) {
      return t.one(sql.sujet.addIntermediateJobId, {
        sujetId,
        jobId
      });
    }).catch(QueryResultError, function(error) {
      if (error.code === qrec.noData) {
        throw ModelNotFoundError({
          model: 'sujet',
          prop: 'id',
          id: sujetIdEnc
        });
      }
      throw error;
    }); 
  });
};

exports.addValidationError = function(sujetIdEnc, error) {
  return db.one(sql.sujet.updateValidationResultError, {
    sujetId: hashids.decode(sujetIdEnc),
    validationError: error
  }).catch(QueryResultError, function(error) {
    if (error.code === qrec.noData) {
      throw ModelNotFoundError({
        model: 'sujet',
        prop: 'id',
        id: sujetIdEnc
      });
    }
    throw error;
  }); 
};

exports.addIntermediate = function(sujetIdEnc, intermediate) {
  return db.result(sql.sujet.updateIntermediateResultSuccess, {
    sujetId: hashids.decode(sujetIdEnc),
    keyIntermediate: intermediate.key,
    duration: intermediate.duration,
    error: null
  }).then(function(result) {
    if (result.rowCount !== 1) {
      throw ModelNotFoundError({
        model: 'sujet',
        prop: 'id',
        id: sujetIdEnc
      });
    }
    return;
  });
};

exports.addIntermediateError = function(sujetIdEnc, error) {
  return db.result(sql.sujet.updateIntermediateResultError, {
    sujetId: hashids.decode(sujetIdEnc),
    error: error
  }).then(function(result) {
    if (result.rowCount !== 1) {
      throw ModelNotFoundError({
        model: 'sujet',
        prop: 'id',
        id: sujetIdEnc
      });
    }
    return;
  });
};

exports.completeDcp = function (blockIdEnc, result) {
  var ids = hashids.raw.decode(blockIdEnc),
    stageId = ids[0],
    dateslotId = ids[1],
    data = {
      stageId: stageId,
      dateslotId: dateslotId,
      etag: null,
      storagekey: null,
      blockhash: null,
      status: -1
    };

  if (result) {
    data.etag = result.etag;
    data.storagekey = result.key;
    data.blockhash = result.hash;
    data.status = 1;
  }

  return db.result(sql.blockUpdateDcp, data)
    .then(function(result) {
      if (result.rowCount !== 1) {
        throw ModelNotFoundError({
          model: 'block',
          prop: 'id',
          id: blockIdEnc
        });
      }
      return;
    });
};

exports.getNotificationEmails = function(blockIdEnc) {
  var ids = hashids.raw.decode(blockIdEnc),
    stageId = ids[0],
    dateslotId = ids[1];

  return db.manyOrNone(sql.getNotificationEmailsDcpReady, {
    stageId: stageId,
    dateslotId: dateslotId
  }).map(function (row) {
    return row.email;
  });
};