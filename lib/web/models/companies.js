'use strict';

const createModel = require('./generic'),
  propMap = {
    'company_id': 'companyId',
    'name': 'companyName',
    'nameshort': 'companyNameShort',
    'fps': 'fps',
    'lockoffsethours': 'lockoffsethours',
    'created_at': 'createdAt',
    'updated_at': 'updatedAt'
  },
  model = createModel({
    tableName: 'companies',
    propMap,
    idName: 'company_id',
    findOrderBy: ['name']
  });

function getBlockOrder(dbOrT, companyId, segmentTypeColumn) {
  return dbOrT.manyOrNone(model._sql.getCompanyBlockOrder, {
    companyId,
    segmentTypeColumn
  }).map((row) => {
    model._util.encodeRowIdProperties(row, ['customer_id']);
    model._util.renameRowProperties(row, {
      'customer_id': 'customerId'
    });
    return row;
  });
}

function getCompanySegments(dbOrT, customerId) {
  return dbOrT.manyOrNone(model._sql.getCompanySegments, {
    customerId
  }).map(model._buildModel);
}

function getReadCompanies(dbOrT, customerId) {
  return dbOrT.manyOrNone(model._sql.getReadCompanies, {
    customerId
  }).map(model._buildModel);
}

model.getBlockOrder = function(companyIdEnc, segmentType) {
  let companyId = model._hashids.decode(companyIdEnc),
    segmentTypeColumn = segmentType === 'mute' ? 'mute_position' : 'audio_position';

  return getBlockOrder(model._db, companyId, segmentTypeColumn);
};

model.setBlockOrder = function(companyIdEnc, segmentType, newCustomerOrderIdsEnc) {
  let pgp = model._db.$config.pgp,
    companyId = model._hashids.decode(companyIdEnc),
    segmentTypeColumn = segmentType === 'mute' ? 'mute_position' : 'audio_position',
    cs = new pgp.helpers.ColumnSet(['?customer_id', segmentTypeColumn], {
      table: 'access_readwrite_customer_to_companyblocksegment'
    }),
    updateQuery = newCustomerOrderIdsEnc.map((customerId, index) => {
      return {
        customer_id: model._hashids.decode(customerId),
        [segmentTypeColumn]: index
      };
    });

  return model._db.tx((t) => {
    return t.result(pgp.helpers.update(updateQuery, cs) + pgp.as.format(model._sql.setCompanyBlockOrder, {
      companyId
    })).then((result) => {
      return getBlockOrder(t, companyId, segmentTypeColumn);
    });
  });
};

model.getCompanySegments = function(customerIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc);

  return getCompanySegments(model._db, customerId);
};
model.addCompanySegments = function(customerIdEnc, companyIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc),
    companyId = model._hashids.decode(companyIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.addCompanySegments, {
      customerId,
      companyId
    }).then(() => {
      return getCompanySegments(t, customerId);
    });
  });
};
model.removeCompanySegments = function(customerIdEnc, companyIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc),
    companyId = model._hashids.decode(companyIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.removeCompanySegments, {
      customerId,
      companyId
    }).then(() => {
      return getCompanySegments(t, customerId);
    });
  });
};



model.getReadCompanies = function(customerIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc);

  return getReadCompanies(model._db, customerId);
};
model.addReadCompany = function(customerIdEnc, companyIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc),
    companyId = model._hashids.decode(companyIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.addReadCompany, {
      customerId,
      companyId
    }).then(() => {
      return getReadCompanies(t, customerId);
    });
  });
};
model.removeReadCompany = function(customerIdEnc, companyIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc),
    companyId = model._hashids.decode(companyIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.removeReadCompany, {
      customerId,
      companyId
    }).then(() => {
      return getReadCompanies(t, customerId);
    });
  });
};

module.exports = model;