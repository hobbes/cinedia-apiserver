'use strict';

var db = require('../db'),
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids');

exports.get = function(sujetIdEnc, type, mimeType, maxSize) {
  return db.one(sql.sujet.getPreview, {
    sujetId: hashids.decode(sujetIdEnc),
    type,
    mimeType,
    maxSize
  }).catch(QueryResultError, function(error) {
    if (error.code === qrec.noData) {
      throw ModelNotFoundError({
        model: 'preview',
        prop: 'type',
        id: sujetIdEnc
      });
    }
  });
};