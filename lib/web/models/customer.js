'use strict';

const createModel = require('./generic'),
  propMap = {
    'customer_id': 'customerId',
    'type': 'type',
    'name': 'name',
    'nameshort': 'nameShort',
    'created_at': 'createdAt',
    'updated_at': 'updatedAt'
  },
  model = createModel({
    tableName: 'customers',
    propMap,
    idName: 'customer_id'
  });

function findCustomersReadAccess(dbOrTx, customerId) {
  return dbOrTx.manyOrNone(model._sql.findCustomersReadAccessByCustomer, {
    customerId
  }).map(model._buildModel);
}

model.findCustomersReadAccess = function (customerIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc);

  return findCustomersReadAccess(model._db, customerId);
};

model.addCustomersReadAccess = function(customerIdEnc, customerReadIdEnc) {
  let pgp = model._db.$config.pgp,
    customerId = model._hashids.decode(customerIdEnc),
    customerReadId = model._hashids.decode(customerReadIdEnc),
    inserQuery = pgp.helpers.insert({
      customer_id: customerId,
      access_read_customer_id: customerReadId
    }, null, 'access_read_customers_customers')

  return model._db.tx((t) => {
    return t.result(inserQuery).then(() => {
      return findCustomersReadAccess(t, customerId);
    });
  });
};

model.removeCustomersReadAccess = function(customerIdEnc, customerReadIdEnc) {
  let customerId = model._hashids.decode(customerIdEnc),
    customerReadId = model._hashids.decode(customerReadIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.removeCustomersReadAccess, {
      customerId,
      customerReadId
    }).then(() => {
      return findCustomersReadAccess(t, customerId);
    });
  });
};

module.exports = model;