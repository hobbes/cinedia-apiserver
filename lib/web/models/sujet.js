'use strict';

var config = require('config'),
  log = require('../../shared/logger'),
  queue = require('../../shared/queue'),
  db = require('../db'),
  mailer = require('../mailer'),
  storage = require('../storage'),
  jobqueue = require('../queueJob'),
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  timeStampToVersion = require('./util/util').timeStampToVersion,
  encodeRowIdProperties = require('./util/util').encodeRowIdProperties,
  renameRowProperties = require('./util/util').renameRowProperties,
  globaLimit = config.get('model.limit'),
  loudnorm = config.get('loudnorm'),
  propMap,
  idsToEncode,
  sujetTypes;

const DIA_VALIDATE_TASKNAME = config.get('queue.jobnames.dia.validate'),
  SPOTMUTE_VALIDATE_TASKNAME = config.get('queue.jobnames.spotmute.validate'),
  SPOTSOUND_VALIDATE_TASKNAME = config.get('queue.jobnames.spotsound.validate');

propMap = {
  'sujet_id': 'sujetId',
  'customer_id': 'customerId',
  'sourcefile_validation_job_id': 'sourcefileValidationJobId',
  'sourcefile_validated': 'sourcefileValidated',
  'sourcefile_isvalid': 'sourcefileIsvalid',
  'sourcefile_validation_error': 'sourcefileValidationError',
  'sourcefile_validation_error_reported': 'sourcefileValidationErrorReported',
  'storagekey_intermediatefile': 'storageKeyIntermediatefile',
  'storagekey_sourcefile': 'storageKeySourceFile',
  'booked_past': 'bookedPast',
  'booked_current': 'bookedCurrent',
  'booked_future': 'bookedFuture',
  'intermediate_job_id': 'intermediateJobId',
  'intermediate_error': 'intermediateError',
  'created_at': 'createdAt',
  'updated_at': 'updatedAt'
};

idsToEncode = [
  'sujet_id',
  'customer_id'
];

sujetTypes = {
  'dia': 'dia',
  'spotmute': 'spot_mute',
  'spotsound': 'spot_sound'
};

function createSujetItemModel(row) {
  encodeRowIdProperties(row, idsToEncode);
  renameRowProperties(row, propMap);
  row.bookedPast = row.bookedPast ? true : false;
  row.bookedCurrent = row.bookedCurrent ? true : false;
  row.bookedFuture = row.bookedFuture ? true : false;
  row.booked = row.bookedPast || row.bookedCurrent || row.bookedFuture;
  row.version = timeStampToVersion(row.updatedAt);
  row.intermediateReady = (row.storageKeyIntermediatefile !== null && row.intermediateError === null);
  log.debug('sujetmodel:', row);
  return row;
}

function getSujet(t, sujetIdEnc) {
  var query = {
    sujetId: hashids.decode(sujetIdEnc)
  };

  return t.one(sql.sujet.get, query)
    .then((row) => {
      if (row.intermediate_job_id) {
        return queue.jobs.getJob(row.intermediate_job_id)
          .then((job) => {
            if (job) {
              row.intermediateJobProgress = job._progress;
            } else {
              row.intermediateJobProgress = 0;
            }
            return row;
          });
      } else {
        row.intermediateJobProgress = 0;
        return row;
      }
    })
    .then((row) => {
      if (row.sourcefile_validation_job_id) {
        return queue.jobs.getJob(row.sourcefile_validation_job_id)
          .then((job) => {
            if (job) {
              row.sourcefileValidationJobProgress = job._progress;
            } else {
              row.sourcefileValidationJobProgress = 0;
            }
            return row;
          });
      } else {
        row.sourcefileValidationJobProgress = 0;
        return row;
      }
    })
    .then(createSujetItemModel)
    .catch(QueryResultError, function(error) {
      if (error.code === qrec.noData) {
        throw ModelNotFoundError({
          model: 'sujet',
          prop: 'id',
          id: sujetIdEnc
        });
      }
      throw error;
    }); 
}

exports.create = function(newSujet, customerIdEnc) {
  var insertData = {
    customer_id: hashids.decode(customerIdEnc),
    name: newSujet.name,
    type: sujetTypes[newSujet.sujettype],
    storagekey_sourcefile: newSujet.filekey,
    duration: newSujet.duration,
    updated_at: null,
    created_at: null
  };

  return db.tx(function(t) {
    return t.one(sql.sujet.create, insertData)
      .then(function(data){
        var sujetId = data.sujet_id,
          sujetIdEnc = hashids.encode(sujetId),
          taskData = {
            sujetId: sujetIdEnc,
            source: newSujet.filekey
          };

        if (insertData.type === 'dia') {
          return jobqueue.newJob(DIA_VALIDATE_TASKNAME, taskData, {
            lifo: true
          })
            .then(function(jobId){
              return {
                jobId,
                sujetId
              };
            });
        }
        if (insertData.type === 'spot_mute') {
          return jobqueue.newJob(SPOTMUTE_VALIDATE_TASKNAME, taskData, {
            lifo: true
          })
            .then(function(jobId){
              return {
                jobId,
                sujetId
              };
            });
        }
        if (insertData.type === 'spot_sound') {
          return jobqueue.newJob(SPOTSOUND_VALIDATE_TASKNAME, taskData, {
            lifo: true
          })
            .then(function(jobId){
              return {
                jobId,
                sujetId
              };
            });
        }
      })
      .then(function({jobId, sujetId}) {
        return t.one(sql.sujet.addValidationJobId, {
          sujetId,
          jobId
        });
      })
      .then(function(row) {
        let sujetIdEnc = hashids.encode(row.sujet_id);

        return getSujet(t, sujetIdEnc);
      });
  });
};

exports.findByCustomer = function(query, customerIdEnc) {
  var dbQuery,
    sqlFile = 'findSortById',
    pointer,
    direction = query.direction,
    limit = query.limit,
    types = query.types,
    status = query.status,
    pointerDirection = '<=',
    sortDirection = 'DESC',
    sujetTypeFilter = ['dia', 'spot_mute', 'spot_sound'],
    bookedPastFilter = [false, true],
    bookedCurrentFilter  = [false, true],
    bookedFutureFilter = [false, true];

  if (types && !Array.isArray(types)) {
    types = [types];
  }

  if (types) {
    sujetTypeFilter = types.map(function(item) {
      if (item === 'spotmute') {
        return 'spot_mute';
      }
      if (item === 'spotsound') {
        return 'spot_sound';
      }
      return item;
    });
  }

  if (status === 'past') {
    bookedPastFilter = [true];
    bookedCurrentFilter  = [false];
    bookedFutureFilter = [false];
  }
  if (status === 'all') {
    bookedPastFilter = [false, true];
    bookedCurrentFilter  = [false, true];
    bookedFutureFilter = [false, true];
  }
  if (status === 'new') {
    bookedPastFilter = [false];
    bookedCurrentFilter  = [false];
    bookedFutureFilter = [false];
  }
  if (status === 'current') {
    bookedPastFilter = [false, true];
    bookedCurrentFilter  = [true];
    bookedFutureFilter = [false, true];
  }

  if (direction === 'next') {
    pointer = hashids.decode(query.pointer) || 9223372036854775807;
  }
  if (direction === 'prev') {
    pointer = hashids.decode(query.pointer) || 0;
    pointerDirection = '>=';
    sortDirection = 'ASC';
  }

  dbQuery = {
    customerId: hashids.decode(customerIdEnc),
    pointer,
    pointerDirection,
    sortDirection,
    type: sujetTypeFilter,
    bookedPast: bookedPastFilter,
    bookedCurrent: bookedCurrentFilter,
    bookedFuture: bookedFutureFilter,
    limit: limit || globaLimit
  };

  if (query.search) {
    sqlFile = 'findSearch';
    dbQuery.search = query.search.trim();
  }

  return db.manyOrNone(sql.sujet[sqlFile], dbQuery)
    .map(createSujetItemModel)
    .then(function(result){
      if (direction === 'prev' && !query.search) {
        return result.reverse();
      }
      return result;
    });
};

exports.get = function (sujetIdEnc) {
  return db.tx(function(t) {
    return getSujet(t, sujetIdEnc);
  });
};

exports.updateName = function(sujetIdEnc, name, customerIdEnc) {
  var sujetId = hashids.decode(sujetIdEnc);

  //TODO include version/updatedAt timestamp in query
  return db.tx(function(t) {
    return t.one(sql.sujet.updateName, {
      sujetId: sujetId,
      customerId: hashids.decode(customerIdEnc),
      name: name
    }).then(function(){
      return getSujet(t, sujetIdEnc);
    }).then(createSujetItemModel)
      .catch(QueryResultError, function(error) {
        if (error.code === qrec.noData) {
          throw ModelNotFoundError({
            model: 'sujet',
            prop: 'id',
            id: sujetIdEnc
          });
        }
      });
  });
};

exports.reportValidationError = (sujetIdEnc, data) => {
  var sujetId = hashids.decode(sujetIdEnc);

  return db.tx((t) => {
    return t.one(sql.sujet.reportValidationError, {
      sujetId
    }).then(() => {
      return mailer.reportValidationError(data);
    }).then(() => {
      return getSujet(t, sujetIdEnc);
    });
  });
};

exports.replaceSourceFile = (sujetIdEnc, data) => {
  var sujetId = hashids.decode(sujetIdEnc);

  return db.tx((t)  => {
    return t.one(sql.sujet.getOldSujetFiles, {
      sujetId
    }).then((row) => {
      let keysToDelete = [];

      if (row.storagekey_sourcefile) {
        keysToDelete.push(row.storagekey_sourcefile);
      }
      if (row.storagekey_intermediatefile) {
        keysToDelete.push(row.storagekey_intermediatefile);
      }
      return storage.deleteFiles(keysToDelete);
    }).then(() => {
      return t.one(sql.sujet.replaceSourceFile, {
        sujetId,
        sourcefilekey: data.filekey
      });
    }).then(() => {
      return t.manyOrNone(sql.sujet.removePreviews, {
        sujetId
      }).then((rows) => {
        let previewStorageKeys = rows.map((row) => {
          return row.storagekey;
        });

        return storage.deleteFiles(previewStorageKeys);
      });
    }).then(() => {
      var taskData = {
        sujetId: sujetIdEnc,
        source: data.filekey
      };

      if (data.type === 'dia') {
        taskData.duration = data.duration;
        return jobqueue.newJob(DIA_VALIDATE_TASKNAME, taskData, {
          lifo: true
        })
          .then(function(){
            return data;
          });
      }

      if (data.type === 'spot_mute') {
        return jobqueue.newJob(SPOTMUTE_VALIDATE_TASKNAME, taskData, {
          lifo: true
        })
          .then(() => {
            return data;
          });
      }
      if (data.type === 'spot_sound') {
        taskData.loudnorm = loudnorm;
        return jobqueue.newJob(SPOTSOUND_VALIDATE_TASKNAME, taskData, {
          lifo: true
        })
          .then(() => {
            return data;
          });
      }
    }).then(() => {
      return getSujet(t, sujetIdEnc);
    });
  });
};

exports.del = function(sujetIdEnc, customerIdEnc) {
  var sujetId = hashids.decode(sujetIdEnc);

  return db.tx((t) => {
    return t.manyOrNone(sql.sujet.getAllStoragekeys, {
      sujetId
    }).map((row) => {
      return row.storagekey;
    }).then((storagekeys) => {
      return t.one(sql.sujet.delete, {
        sujetId: sujetId,
        customerId: hashids.decode(customerIdEnc)
      }).catch(QueryResultError, function(error) {
        if (error.code === qrec.noData) {
          throw ModelNotFoundError({
            model: 'sujet',
            prop: 'id',
            id: sujetIdEnc
          });
        }
        throw error;
      }).then(() => {
        log.info(`deleting storagefiles: ${storagekeys}`);
        return storage.deleteFiles(storagekeys);
      });
    });
  });
};

exports.getSignedGETUrl = function(filekey, time) {
  return storage.getSignedGETUrl(filekey, time);
};