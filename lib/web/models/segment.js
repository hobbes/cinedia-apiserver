'use strict';

var db = require('../db'),
  pgp = db.$config.pgp,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  BlockSujetMoveError = require('./error').BlockSujetMoveError,
  encodeRowIdProperties = require('./util/util').encodeRowIdProperties,
  renameRowProperties = require('./util/util').renameRowProperties,
  propMap,
  idsToEncode;

propMap = {
  'stage_id': 'stageId',
  'customer_id': 'customerId',
  'sujet_id': 'sujetId',
  'dateslot_id': 'dateslotId',
  'sujet_name': 'sujetName',
  'sujet_duration': 'sujetDuration',
  'sujet_type': 'sujetType',
  'segment_position': 'segmentPosition',
  'segment_has_audio': 'segmentHasAudio',
  'sujet_storagekey_intermediatefile': 'sujetIntermediatefileKey',
  'sujet_errormessage': 'sujetErrormessage',
  'sujet_error_reported': 'sujetErrorReported'
};

idsToEncode = [
  'stage_id',
  'customer_id',
  'sujet_id',
  'dateslot_id'
];

function getAllCustomersOfBlock(dbOrT, customerIdsReadAccess) {
  return dbOrT.manyOrNone(sql.block.getAllCustomers, {
    customerIds: customerIdsReadAccess
  }).map(function (row) {
    encodeRowIdProperties(row, ['customer_id']);
    renameRowProperties(row, {
      'customer_id': 'customerId'
    });
    return row;
  }).then(function (customers) {
    var customersMap = {};
    customers.forEach(function (customer) {
      customersMap[customer.customerId] = customer;
    });
    return customersMap;
  });
}

function buildBlockSegmentRow(row) {
  row.id = hashids.raw.encode([row.customer_id, row.segment_has_audio, row.segment_position]);
  encodeRowIdProperties(row, idsToEncode);
  renameRowProperties(row, propMap);
  row.bookingGuaranteed = (row.sujetIntermediatefileKey && !row.sujetErrormessage);
  return row;
}

function getAllSegmentsOfBlock(dbOrT, stageId, dateslotId, customerIdsReadAccess) {
  return dbOrT.manyOrNone(sql.block.getAllSegments, {
    stageId: stageId,
    dateslotId: dateslotId,
    customerIds: customerIdsReadAccess
  }).map(buildBlockSegmentRow);
}

function getOneSegmentOfBlock(dbOrT, stageId, dateslotId, customerId, segmentHasAudio, segmentPosition) {
  return dbOrT.manyOrNone(sql.block.getOneSegment, {
    stageId,
    dateslotId,
    customerId,
    segmentHasAudio,
    segmentPosition
  }).map(buildBlockSegmentRow);
}

function getAllBlockSegmentsWithCustomers(dbOrT, dateslotId, stageId, customerIdsReadAccess) {
  return dbOrT.tx(function(t) {
    var allCustomersOfBlock = getAllCustomersOfBlock(dbOrT, customerIdsReadAccess),
      allSegmentsOfBlock = getAllSegmentsOfBlock(dbOrT, stageId, dateslotId, customerIdsReadAccess);

    return t.batch([allSegmentsOfBlock, allCustomersOfBlock])
      .spread(function(segmentRows, customers) {
        var result = [],
          currentSegm,
          currentSegmPos,
          curremtSegmHasAudio,
          currentSegmCustomerId;

        if (segmentRows.length > 0) {
          currentSegmPos = segmentRows[0].segmentPosition;
          curremtSegmHasAudio = segmentRows[0].segmentHasAudio;
          currentSegmCustomerId = segmentRows[0].customerId;
          currentSegm = {
            id: segmentRows[0].id,
            blockPosition: segmentRows[0].segmentPosition,
            duration: 0,
            customer: {
              id: currentSegmCustomerId,
              name: customers[currentSegmCustomerId].name,
              nameshort: customers[currentSegmCustomerId].nameshort,
              type: customers[currentSegmCustomerId].type
            },
            audio: false,
            sujets: []
          };
          if (segmentRows[0].segmentHasAudio === 1) {
            currentSegm.audio = true;
          }
          segmentRows.forEach(function (row) {
            if (row.segmentPosition !== currentSegmPos || row.segmentHasAudio !== curremtSegmHasAudio) {
              result.push(currentSegm);
              currentSegmPos = row.segmentPosition;
              curremtSegmHasAudio = row.segmentHasAudio;
              currentSegmCustomerId = row.customerId;
              currentSegm = {
                id: row.id,
                blockPosition: row.segmentPosition,
                duration: 0,
                customer: {
                  id: row.customerId,
                  name: customers[currentSegmCustomerId].name,
                  nameshort: customers[currentSegmCustomerId].nameshort,
                  type: customers[currentSegmCustomerId].type
                },
                audio: false,
                sujets: []
              };
              if (row.segmentHasAudio === 1) {
                currentSegm.audio = true;
              }
            }
            currentSegm.duration = currentSegm.duration + row.sujetDuration;
            currentSegm.sujets.push(row);
          });
          result.push(currentSegm);
        }
        return result;
      });
  });
}

exports.getAllBlockSegments = function (blockIdsEnc, customerIdsReadAccessEnc) {
  var ids = hashids.raw.decode(blockIdsEnc),
    stageId = ids[0],
    dateslotId = ids[1],
    customerIdsReadAccess = customerIdsReadAccessEnc.map(function (customerId) {
      return hashids.decode(customerId);
    });

  return getAllBlockSegmentsWithCustomers(db, dateslotId, stageId, customerIdsReadAccess);
};

exports.getOneBlockSegment = function (blockIdsEnc, segmentIdEnc) {
  var ids = hashids.raw.decode(blockIdsEnc),
    stageId = ids[0],
    dateslotId = ids[1],
    segmentIds = hashids.raw.decode(segmentIdEnc),
    customerId = segmentIds[0],
    segmentHasAudio = segmentIds[1],
    segmentPosition = segmentIds[2];

  return db.tx(function(t) {
    var allCustomersOfBlock = getAllCustomersOfBlock(t, [customerId]),
      getSegment = getOneSegmentOfBlock(t, stageId, dateslotId, customerId, segmentHasAudio, segmentPosition);

    return t.batch([getSegment, allCustomersOfBlock])
      .spread(function(segmentRows, customers) {
        var result = {},
          currentSegm,
          currentSegmCustomerId;

        if (segmentRows.length > 0) {
          currentSegmCustomerId = segmentRows[0].customerId;
          currentSegm = {
            id: segmentRows[0].id,
            blockPosition: segmentRows[0].segmentPosition,
            customer: {
              id: currentSegmCustomerId,
              name: customers[currentSegmCustomerId].name,
              nameshort: customers[currentSegmCustomerId].nameshort,
              type: customers[currentSegmCustomerId].type
            },
            audio: false,
            sujets: []
          };
          if (segmentRows[0].segmentHasAudio === 1) {
            currentSegm.audio = true;
          }
          segmentRows.forEach(function (row) {
            currentSegm.sujets.push(row);
          });
          result = currentSegm;
        }
        return result;
      });
  });
};

exports.updateSegmentOrder = function (blockIdsEnc, segmentIdEnc, list, customerIdsReadAccessEnc) {
  var blockIds = hashids.raw.decode(blockIdsEnc),
    stageId = blockIds[0],
    dateslotId = blockIds[1],
    segmentIds = hashids.raw.decode(segmentIdEnc),
    customerId = segmentIds[0],
    segmentHasAudio = segmentIds[1],
    segmentPosition = segmentIds[2],
    reelSujetsIds = list.map(function (sujetId) {
      return hashids.decode(sujetId);
    }),
    customerIdsReadAccess = customerIdsReadAccessEnc.map(function (customerId) {
      return hashids.decode(customerId);
    });

  return db.tx('updateSegmentOrder', function (t) {
    return t.manyOrNone(sql.block.segment.getSujetOrder, {
      stageId,
      dateslotId,
      segmentHasAudio,
      segmentPosition,
      customerId,
      reelSujetsIds
    }).then(function (rows) {
      if (rows.length !== list.length) {
        throw BlockSujetMoveError({
          model: 'segment'
        });
      }

      return rows.map(function (row) {
        return row.position;
      }).sort(function(a, b) {
        return a - b;
      });
    }).then(function (orderedPositions) {
      return reelSujetsIds.map(function (sujetId, index) {
        return {
          sujet_id: sujetId,
          position: orderedPositions[index]
        };
      });
    }).then(function (map) {
      var cs = new pgp.helpers.ColumnSet(['?sujet_id', 'position'], {
        table: 'sujet_stage_dateslot'
      });

      return t.result(pgp.helpers.update(map, cs) + pgp.as.format(sql.block.segment.updateSujetOrder, {
        stageId: stageId,
        dateslotId: dateslotId,
        customerId: customerId
      })).then(function () {
        return getAllBlockSegmentsWithCustomers(t, dateslotId, stageId, customerIdsReadAccess);
      });
    });
  });
};