DELETE FROM
  access_read_customer_to_company
WHERE
  customer_id = ${customerId}
AND
  company_id = ${companyId}
RETURNING
  company_id;