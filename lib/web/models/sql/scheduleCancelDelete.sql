DELETE FROM
  sujet_stage_dateslot
WHERE
  sujet_id = ${sujetId}
AND
  customer_id = ${customerId}
AND
  stage_id = ${stageId}
AND
  dateslot_id = ${dateslotId}
RETURNING
  position;