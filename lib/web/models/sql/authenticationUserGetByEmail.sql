SELECT
  user_id,
  user_firstname,
  user_lastname,
  customer_name,
  customer_nameshort,
  customer_type,
  customer_id,
  email
FROM
  view_users
WHERE
  email = ${email};