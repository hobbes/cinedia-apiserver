UPDATE
  dcps
SET
  storagekey = ${storagekey},
  blockhash = ${blockhash},
  status = ${status},
  etag = ${etag}
WHERE
  stage_id = ${stageId}
AND
  dateslot_id = ${dateslotId};