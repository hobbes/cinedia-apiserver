SELECT
  *,
  CURRENT_TIMESTAMP as now
FROM view_render_blocks
WHERE startdate <= ${date}
AND enddate >= ${date};