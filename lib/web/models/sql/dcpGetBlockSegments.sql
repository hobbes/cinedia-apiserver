SELECT
  segment_has_audio,
  sujet_storagekey_intermediatefile
FROM
  view_sujet_stage_dateslot
WHERE
  dateslot_id = ${dateslotId}
AND
  stage_id = ${stageId}
AND
  sujet_storagekey_intermediatefile IS NOT NULL
ORDER BY
  segment_has_audio ASC,
  segment_position ASC,
  position ASC;