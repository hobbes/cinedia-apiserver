SELECT
  dateslot_id,
  startdate,
  enddate,
  kw,

  stage_id,
  stage_name,
  stage_name_short,
  stage_lockoffsethours,
  stage_fps,

  location_id,
  location_name,
  
  company_id,
  company_name,
  company_name_short,

  dcps.status AS dcp_status,
  dcps.storagekey AS dcp_storagekey,
  dcps.blockhash AS dcp_blockhash,
  dcps.version AS dcp_version,

  CURRENT_TIMESTAMP as now,
  
  md5 (string_agg(rowstring, '' ORDER BY rowstring ASC) ) as blockhash
FROM
  view_sujet_stage_dateslot
LEFT JOIN dcps using (stage_id, dateslot_id)
WHERE 
  startdate <= ${date}
AND
  enddate >= ${date}
AND
  view_sujet_stage_dateslot.customer_id IN (${customerIds:csv})
AND
  view_sujet_stage_dateslot.stage_id IN (${stageIds:csv})
GROUP BY
  dateslot_id,
  kw,
  startdate,
  enddate,

  company_id,
  company_name,
  company_name_short,

  location_id,
  location_name,

  stage_id,
  stage_name,
  stage_name_short,
  stage_lockoffsethours,
  stage_fps,
  
  dcp_status,
  dcp_storagekey,
  dcp_blockhash,
  dcp_version
ORDER BY
  dateslot_id ASC,
  company_name ASC,
  location_name ASC,
  stage_name ASC;



