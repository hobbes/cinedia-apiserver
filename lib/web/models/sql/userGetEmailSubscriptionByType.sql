SELECT
  email
FROM
  users
WHERE
  customer_id = ${customerId}
AND
  notification_dcpready = TRUE;