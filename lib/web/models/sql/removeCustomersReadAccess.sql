DELETE FROM
  access_read_customers_customers
WHERE
  customer_id = ${customerId}
AND
  access_read_customer_id = ${customerReadId};