INSERT INTO
  dcps (stage_id, dateslot_id, status, version)
VALUES
  (${stageId}, ${dateslotId}, 0, 0)
ON CONFLICT (stage_id, dateslot_id) DO UPDATE
SET
  status = 0,
  storagekey = NULL,
  etag = NULL,
  blockhash = NULL,
  version = dcps.version + 1
WHERE
  dcps.dateslot_id = ${dateslotId}
AND
  dcps.stage_id = ${stageId}
RETURNING
  stage_id,
  dateslot_id,
  status,
  version;