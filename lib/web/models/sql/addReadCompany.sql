INSERT INTO
  access_read_customer_to_company (customer_id, company_id)
VALUES
  (${customerId}, ${companyId})
RETURNING
  company_id;