SELECT
  customer_id,
  name,
  nameshort,
  type
FROM
  customers
WHERE customer_id IN (${customerIds:csv})
