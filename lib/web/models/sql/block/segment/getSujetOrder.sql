SELECT
  sujet_id,
  position
FROM
  view_sujet_stage_dateslot
WHERE
  sujet_id IN (${reelSujetsIds:csv})
AND
  customer_id = ${customerId}
AND
  segment_has_audio = ${segmentHasAudio}
AND
  segment_position = ${segmentPosition}
AND
  stage_id = ${stageId}
AND
  dateslot_id = ${dateslotId}
ORDER BY
  position ASC;