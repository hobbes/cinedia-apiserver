SELECT
  stage_id,
  customer_id,
  sujet_id,
  sujet_name,
  dateslot_id,
  segment_has_audio,
  segment_position,
  position,
  sujet_duration,
  sujet_type
FROM
  view_sujet_stage_dateslot
WHERE
  dateslot_id = ${dateslotId}
AND
  stage_id = ${stageId}
AND
  customer_id = ${customerId}
AND
  segment_has_audio = ${segmentHasAudio}
AND
  segment_position = ${segmentPosition}
ORDER BY
  segment_has_audio ASC,
  segment_position ASC,
  position ASC;