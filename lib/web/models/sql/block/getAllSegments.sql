SELECT
  stage_id,
  customer_id,
  sujet_id,
  sujet_name,
  dateslot_id,
  segment_has_audio,
  segment_position,
  position,
  sujet_duration,
  sujet_type,
  sujet_storagekey_intermediatefile,
  sujet_errormessage,
  sujet_error_reported
FROM
  view_sujet_stage_dateslot
WHERE
  dateslot_id = ${dateslotId}
AND
  stage_id = ${stageId}
AND
  customer_id IN (${customerIds:csv})
ORDER BY
  segment_has_audio ASC,
  segment_position ASC,
  position ASC;