UPDATE
  sujets
SET
  storagekey_intermediatefile = ${keyIntermediate},
  duration = ${duration},
  errormessage = ${error},
  error_reported = FALSE
WHERE
  sujet_id = ${sujetId};