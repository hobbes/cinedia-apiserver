DELETE FROM
  dcps
WHERE dateslot_id < (
  SELECT dateslot_id
  FROM dateslots
  WHERE enddate < (current_date - INTERVAL '4 week')
  ORDER BY enddate DESC
  limit 1
)
AND
  storagekey IS NOT NULL
RETURNING
  storagekey;