SELECT
  dateslot_id,
  startdate,
  enddate,
  kw
FROM
  dateslots
WHERE
  startdate >= ${startdate}
AND
  enddate <= ${enddate}
ORDER BY
  startdate ASC
LIMIT
  ${limit};