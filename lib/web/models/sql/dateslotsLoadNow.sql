SELECT
  dateslot_id,
  startdate,
  enddate,
  kw,
  CURRENT_TIMESTAMP as now
FROM
  dateslots
WHERE
  startdate <= CURRENT_DATE
AND
  enddate >= CURRENT_DATE;