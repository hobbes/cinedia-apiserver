SELECT DISTINCT
  location_id,
  location_name,
  company_id,
  company_name,
  company_nameshort,
  company_lockoffsethours
FROM
  view_access_readwrite_customer_to_stageblocksegment
WHERE
  customer_id = ${customerId}
ORDER BY
  company_id,
  location_id;