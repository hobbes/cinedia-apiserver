SELECT
  access_read_customer_id as customer_id,
  type,
  name,
  nameshort,
  updated_at,
  created_at
FROM
  view_access_customers_customers
WHERE customer_id = ${customerId};