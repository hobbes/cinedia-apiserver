INSERT INTO 
  access_readwrite_customer_to_companyblocksegment (customer_id, company_id, mute_position, audio_position)
VALUES
  (
    ${customerId},
    ${companyId},
    (
      SELECT
        coalesce(max(mute_position), 0) + 1
      FROM
        access_readwrite_customer_to_companyblocksegment
      WHERE
        company_id = ${companyId}
    ),
    (
      SELECT
        coalesce(max(audio_position), 0) + 1
      FROM
        access_readwrite_customer_to_companyblocksegment
      WHERE
        company_id = ${companyId}
    )
  );