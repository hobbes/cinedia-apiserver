UPDATE
  sujets
SET
  storagekey_intermediatefile = NULL,
  errormessage = ${error},
  error_reported = FALSE
WHERE
  sujet_id = ${sujetId};