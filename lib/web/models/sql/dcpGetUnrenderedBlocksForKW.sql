SELECT
  stage_id,
  dateslot_id
FROM
  view_render_blocks
WHERE
  ((startdate - stage_lockoffsethours) <= CURRENT_TIMESTAMP)
AND
  enddate >= CURRENT_DATE
AND
  dcp_blockhash IS NULL
AND
  dcp_storagekey IS NULL
AND
  dcp_status IS NULL
ORDER BY
  dateslot_id;