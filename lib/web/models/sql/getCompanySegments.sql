SELECT
  company_id,
  name,
  nameshort,
  fps,
  lockoffsethours,
  updated_at,
  created_at
FROM
  view_access_readwrite_customer_to_companyblocksegment
WHERE customer_id = ${customerId};