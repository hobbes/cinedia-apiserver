SELECT 
  email
FROM
  view_render_blocks
INNER JOIN
  access_readwrite_customer_to_companyblocksegment using (company_id)
INNER JOIN
  view_users_activities using (customer_id)
WHERE
  dateslot_id = ${dateslotId}
AND
  stage_id = ${stageId}
AND
  view_users_activities.name = 'blocks.dcps.read'
AND
  flag_notification_dcpready = TRUE;