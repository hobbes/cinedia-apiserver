SELECT
  customer_id,
  type,
  name,
  nameshort,
  ${segmentTypeColumn:name} as position
FROM access_readwrite_customer_to_companyblocksegment
INNER JOIN customers USING (customer_id)
WHERE company_id = ${companyId}
ORDER BY position ASC;