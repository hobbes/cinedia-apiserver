SELECT
  dateslot_id,
  startdate,
  enddate,
  kw
FROM
  dateslots
WHERE
  dateslot_id IN (${dateslotIds:csv})
ORDER BY
  startdate ASC
LIMIT
  ${limit};