SELECT
  access_read_customer_id
FROM
  view_access_read_customers_customers
WHERE
  customer_id = ${customerId};