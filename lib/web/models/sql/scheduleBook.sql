INSERT INTO
  sujet_stage_dateslot (sujet_id, stage_id, dateslot_id, customer_id, position)
VALUES
  (${sujetId}, ${stageId}, ${dateslotId}, ${customerId}, (
    SELECT
      coalesce(max(position), 0) + 1
    FROM
      sujet_stage_dateslot
    WHERE
      customer_id = ${customerId}
    AND
      stage_id = ${stageId}
    AND
      dateslot_id = ${dateslotId})
  )
RETURNING position;