INSERT INTO 
  sujets (customer_id, name, type, storagekey_sourcefile, duration)
VALUES
  (${customer_id}, ${name}, ${type}, ${storagekey_sourcefile}, ${duration})
RETURNING
  sujet_id;