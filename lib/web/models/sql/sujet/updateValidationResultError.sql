UPDATE
  sujets
SET
  sourcefile_validated = TRUE,
  sourcefile_isvalid = FALSE,
  sourcefile_validation_error = ${validationError},
  sourcefile_validation_job_id = NULL
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id;