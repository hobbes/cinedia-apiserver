SELECT
  type,
  size,
  storagekey,
  etag
FROM
  view_customer_sujet_previews
WHERE
  sujet_id = ${sujetId}
AND
  type = ${type}
AND
  mime_type = ${mimeType}
AND
  size <= ${maxSize}
ORDER BY
  size DESC
LIMIT 1;