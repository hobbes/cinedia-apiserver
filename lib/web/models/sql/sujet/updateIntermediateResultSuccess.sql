UPDATE
  sujets
SET
  storagekey_intermediatefile = ${keyIntermediate},
  duration = ${duration},
  sourcefile_validation_error_reported = FALSE,
  sourcefile_validation_job_id = NULL,
  sourcefile_validation_error = NULL,
  intermediate_error = NULL,
  intermediate_job_id = NULL
WHERE
  sujet_id = ${sujetId};