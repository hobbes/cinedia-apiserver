DELETE FROM
  sujets
WHERE
  sujet_id = ${sujetId}
AND
  customer_id = ${customerId}
RETURNING
  sujet_id;