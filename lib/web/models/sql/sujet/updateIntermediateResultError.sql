UPDATE
  sujets
SET
  storagekey_intermediatefile = NULL,
  intermediate_error = ${error},
  intermediate_job_id = NULL
WHERE
  sujet_id = ${sujetId};