SELECT 
  sujets.sujet_id,
  sujets.customer_id,
  sujets.name,
  sujets.type,
  sujets.duration,
  sujets.storagekey_intermediatefile,
  sujets.storagekey_sourcefile,
  sujets.sourcefile_validation_job_id,
  sujets.sourcefile_validated,
  sujets.sourcefile_isvalid,
  sujets.sourcefile_validation_error,
  sujets.sourcefile_validation_error_reported,
  sujets.intermediate_job_id,
  sujets.intermediate_error,
  sujets.created_at,
  sujets.updated_at,
  join_view_sujet_stage_dateslot_past.booked_past,
  join_view_sujet_stage_dateslot_current.booked_current,
  join_view_sujet_stage_dateslot_future.booked_future
FROM
  sujets

LEFT JOIN LATERAL (
  SELECT
    view_sujet_stage_dateslot.dateslot_id  AS booked_past
  FROM
    view_sujet_stage_dateslot
  WHERE
    sujets.sujet_id = view_sujet_stage_dateslot.sujet_id
  AND
    enddate < CURRENT_TIMESTAMP
  LIMIT 1
) AS join_view_sujet_stage_dateslot_past on true

LEFT JOIN LATERAL (
  SELECT
    view_sujet_stage_dateslot.dateslot_id  AS booked_current
  FROM
    view_sujet_stage_dateslot
  WHERE
    sujets.sujet_id = view_sujet_stage_dateslot.sujet_id
  AND
    startdate < CURRENT_TIMESTAMP
  AND
    enddate > CURRENT_TIMESTAMP
  LIMIT 1
) AS join_view_sujet_stage_dateslot_current on true

LEFT JOIN LATERAL (
  SELECT
    view_sujet_stage_dateslot.dateslot_id  AS booked_future
  FROM
    view_sujet_stage_dateslot
  WHERE
    sujets.sujet_id = view_sujet_stage_dateslot.sujet_id
  AND
    startdate > CURRENT_TIMESTAMP
  LIMIT 1
) AS join_view_sujet_stage_dateslot_future on true

WHERE
  customer_id = ${customerId}
AND
  type IN (${type:csv})
AND
  name ILIKE '%${search:value}%'
AND
  booked_past is not null IN (${bookedPast:csv})
AND
  booked_current is not null IN (${bookedCurrent:csv})
ORDER BY
  name <-> '%${search:value}%' ASC
LIMIT
  ${limit};