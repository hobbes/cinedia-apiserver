SELECT sujets.storagekey_intermediatefile AS storagekey
FROM sujets
WHERE sujet_id = ${sujetId}
AND sujets.storagekey_intermediatefile IS NOT NULL

UNION

SELECT sujets.storagekey_sourcefile AS storagekey
FROM sujets
WHERE sujet_id = ${sujetId}
AND sujets.storagekey_sourcefile IS NOT NULL

UNION

SELECT sujet_previews.storagekey AS storagekey
FROM sujet_previews
WHERE sujet_id = ${sujetId}
AND sujet_previews.storagekey IS NOT NULL;