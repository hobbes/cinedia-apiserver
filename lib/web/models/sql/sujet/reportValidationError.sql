UPDATE
  sujets
SET
  sourcefile_validation_error_reported = true
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id, sourcefile_validation_error_reported;