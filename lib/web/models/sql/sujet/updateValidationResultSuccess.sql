UPDATE
  sujets
SET
  sourcefile_validated = TRUE,
  sourcefile_isvalid = TRUE,
  sourcefile_validation_error = NULL,
  sourcefile_validation_job_id = NULL
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id,
  type,
  duration,
  storagekey_sourcefile;