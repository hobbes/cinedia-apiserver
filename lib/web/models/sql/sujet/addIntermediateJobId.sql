UPDATE
  sujets
SET
  intermediate_job_id = ${jobId}
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id;