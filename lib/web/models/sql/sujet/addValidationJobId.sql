UPDATE
  sujets
SET
  sourcefile_validation_job_id = ${jobId}
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id;