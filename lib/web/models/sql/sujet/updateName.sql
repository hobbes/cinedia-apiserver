UPDATE
  sujets
SET
  name = ${name},
  updated_at = CURRENT_TIMESTAMP
WHERE
  sujet_id = ${sujetId}
AND
  customer_id = ${customerId}
RETURNING
  sujet_id;