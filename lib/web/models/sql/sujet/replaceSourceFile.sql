UPDATE
  sujets
SET
  storagekey_sourcefile = ${sourcefilekey},
  storagekey_intermediatefile = NULL,
  errormessage = NULL,
  error_reported = false
WHERE
  sujet_id = ${sujetId}
RETURNING
  sujet_id;