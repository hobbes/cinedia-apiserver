SELECT
  stage_id,
  stage_fps,
  dateslot_id,
  kw,
  startdate,
  enddate,
  company_name,
  company_name_short,
  stage_name,
  stage_name_short,
  blockhash
FROM
  view_render_blocks
WHERE
  dateslot_id = ${dateslotId}
AND
  stage_id = ${stageId};