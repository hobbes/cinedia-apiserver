SELECT 
  stage_name,
  location_name,
  company_name,
  sujet_id,
  location_id,
  stage_id,
  dateslot_id,
  startdate,
  enddate
FROM
  view_sujet_stage_dateslot
WHERE 
  sujet_id = ${sujetId}
AND
  customer_id = ${customerId}
AND
  startdate >= ${startdate}
AND
  enddate <= ${enddate}
ORDER BY 
  location_id ASC,
  stage_id ASC,
  startdate ASC
LIMIT
  ${limit};