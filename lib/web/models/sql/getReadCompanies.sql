SELECT
  company_id,
  name,
  nameshort,
  fps,
  lockoffsethours,
  updated_at,
  created_at
FROM
  view_access_read_customer_to_company
WHERE customer_id = ${customerId};