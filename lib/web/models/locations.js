'use strict';

const createModel = require('./generic'),
  propMap = {
    'location_id': 'locationId',
    'name': 'locationName',
    'company_id': 'companyId',
    'created_at': 'createdAt',
    'updated_at': 'updatedAt'
  },
  model = createModel({
    tableName: 'locations',
    propMap,
    idName: 'location_id',
    findOrderBy: ['company_id', 'location_id', 'name']
  });

module.exports = model;