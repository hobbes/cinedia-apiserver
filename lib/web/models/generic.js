'use strict';

const db = require('../db'),
  pgp = db.$config.pgp,
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = pgp.errors.QueryResultError,
  qrec = pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  util = require('./util/util');

function buildModel(idsToEncodeDecode, propMap) {
  return function(row) {
    util.encodeRowIdProperties(row, idsToEncodeDecode);
    util.renameRowProperties(row, propMap);
    row.version = util.timeStampToVersion(row.updatedAt);
    return row;
  };
}

module.exports = function ({tableName, modifyTableName = tableName, propMap, propMapModify = propMap, idName, findOrderBy = [idName]}) {
  const propMapRead = propMap,
    propMapModifyWrite = util.swap(propMapModify),
    columNames = util.mapKeysToArray(propMap),
    columNamesModify = util.mapKeysToArray(propMapModify),
    idsToEncodeDecode = util.extractIdsToEncodeDecodeFromMap(propMap);

  return {
    _db: db,
    _sql: sql,
    _hashids: hashids,
    _buildModel: buildModel(idsToEncodeDecode, propMapRead),
    _util: util,
    create(data) {
      let row = util.renameRowProperties(data, propMapModifyWrite),
        cs = new pgp.helpers.ColumnSet(columNamesModify),
        query;

      util.decodeRowIdProperties(row, idsToEncodeDecode);
      query = `${pgp.helpers.insert(row, null, modifyTableName)} RETURNING ${cs.names}`;

      return db.one(query)
        .then(buildModel(idsToEncodeDecode, propMapModify));
    },
    find() {
      return db.manyOrNone(sql.generic.find, {
        table: tableName,
        columns: columNames,
        findOrderBy
      }).map(buildModel(idsToEncodeDecode, propMapRead));
    },
    get(itemIdEnc) {
      let itemId = hashids.decode(itemIdEnc);

      return db.one(sql.generic.get, {
        table: tableName,
        columns: columNames,
        idName,
        itemId
      }).then(buildModel(idsToEncodeDecode, propMapRead))
        .catch(QueryResultError, function(error) {
          if (error.code === qrec.noData) {
            throw ModelNotFoundError({
              model: tableName,
              prop: 'id',
              id: itemId
            });
          }
        });
    },
    update(itemIdEnc, data) {
      let itemId = hashids.decode(itemIdEnc),
        row = util.renameRowProperties(data, propMapModifyWrite);

      util.decodeRowIdProperties(row, idsToEncodeDecode);
      return db.one(sql.generic.update, {
        updateQuery: pgp.helpers.update(row, null, modifyTableName),
        idName,
        itemId,
        columns: columNamesModify
      }).then(buildModel(idsToEncodeDecode, propMapModify))
        .catch(QueryResultError, function(error) {
          if (error.code === qrec.noData) {
            throw ModelNotFoundError({
              model: modifyTableName,
              prop: 'id',
              id: itemId
            });
          }
        })
        .catch(function(error) {
          if (error.code === '23505') {
            console.log(error.detail);
            throw error;
          }
          throw error;
        });
    },
    delete(itemIdEnc) {
      let itemId = hashids.decode(itemIdEnc);

      return db.one(sql.generic.delete, {
        table: modifyTableName,
        idName,
        itemId
      });
    }
  };
};