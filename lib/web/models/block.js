'use strict';

var db = require('../db'),
  config = require('config'),
  apphost = config.get('apphost'),
  apihost = config.get('host'),
  issuer = config.get('dcp.issuer'),
  spaceFrames = config.get('dcp.spaceFrames'),
  jwt = require('../jwt'),
  storage = require('../storage'),
  moment = require('moment'),
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  jobqueue = require('../queueJob'),
  encodeRowIdProperties = require('./util/util').encodeRowIdProperties,
  renameRowProperties = require('./util/util').renameRowProperties,
  propMap,
  idsToEncode;

const BLOCK_DCP_TASKNAME = config.get('queue.jobnames.block.dcp.render');

propMap = {
  'dateslot_id': 'dateslotId',
  'dcp_status': 'dcpStatus',
  'dcp_blockhash': 'dcpBlockhash',
  'dcp_storagekey': 'dcpStoragekey',
  'dcp_version': 'dcpVersion',
  'stage_id': 'stageId',
  'stage_name': 'stageName',
  'stage_name_short': 'stageNameShort',
  'location_name': 'locationName',
  'location_id': 'locationId',
  'company_name': 'companyName',
  'company_name_short': 'companyNameShort',
  'company_id': 'companyId',
  'customer_id': 'customerId',
  'sujet_id': 'sujetId',
  'sujet_name': 'sujetName',
  'sujet_duration': 'sujetDuration',
  'sujet_type': 'sujetType',
  'kw': 'kw'
};

idsToEncode = [
  'dateslot_id',
  'sujet_id',
  'stage_id',
  'location_id',
  'company_id',
  'customer_id'
];

function dcpBaseName(kw, stageNameShort, versionNumber) {
  const paddedKw = String(kw).padStart(2, '0'),
    truncatedKW = paddedKw.substring(0, 2),
    truncatedStageNameShort = stageNameShort.substring(0, 4);

  let versionSuffix = '';

  if (versionNumber !== null) {
    versionSuffix = `v${Number(versionNumber + 1).toFixed()}`;
  }

  // OmetKW06KSM1
  // XXXX
  //     KWXX
  //         XXXX
  return `OmetKW${truncatedKW}${truncatedStageNameShort}${versionSuffix}`;
}

function fullCPL(name, hasSound, version) {
  let hasSoundName = hasSound ? 's' : 'm',
    audioType = hasSound ? '51' : 'MOS',
    title = `${name}${hasSoundName}`,
    versionNumber = Number(version) + 1,
    language = 'NULL',
    territoryAndRating = 'NULL',
    studio = 'NULL',
    facility = 'NUL',
    date = moment().format('YYYYMMDD');

  // MovieTitle_TLR-1-Temp-RedBand-Chain-3D-4Ol-48-DVis_F-133_EN-EN-OCAP_US-GB_51-HI-VI-IAB-Dbox_2K_ST_20190103_Facility_IOP-3D_OV
  // OmetKW06KSM1m_ADV-1_F_51_2K_20200205_SMPTE_OV
  return `${title}_ADV-${versionNumber}_F_${language}_${territoryAndRating}_${audioType}_2K_${studio}_${date}_${facility}_SMPTE_OV`;
}


function buildBlockModel(row) {
  var now = moment(row.now),
    startdate = moment(row.startdate),
    enddate = moment(row.enddate),
    lockdate = startdate.subtract(row.stage_lockoffsethours.hours, 'hours'),
    dcpFolderName = dcpBaseName(row.kw, row.stage_name_short, row.dcp_version);

  if (row.dcp_version !== null) {
    row.dcp_version = row.dcp_version + 1;
  }

  row.name = dcpFolderName;
  row.stage_lockoffsethours = row.stage_lockoffsethours.hours;
  row.blockId = hashids.raw.encode(row.stage_id, row.dateslot_id);
  row.locked = now.isSameOrAfter(lockdate);
  row.lockDate = lockdate.toISOString();
  row.current = now.isAfter(lockdate) && now.isBefore(enddate);
  row.outdated = now.isAfter(enddate);
  encodeRowIdProperties(row, idsToEncode);
  renameRowProperties(row, propMap);
  return row;
}

exports.find = function (dateString, customerIdsReadAccessEnc, stageIdsReadAccessEnc) {
  var date = moment(dateString, 'YYYYMMDD').toDate(),
    customerIdsReadAccess = customerIdsReadAccessEnc.map(function (id) {
      return hashids.decode(id);
    }),
    stageIdsReadAccess = stageIdsReadAccessEnc.map(function (id) {
      return hashids.decode(id);
    });

  return db.manyOrNone(sql.blockFind, {
    date: date,
    customerIds: customerIdsReadAccess,
    stageIds: stageIdsReadAccess
  }).map(buildBlockModel);
};

exports.findAll = function (dateString) {
  var date = moment(dateString, 'YYYYMMDD').toDate();

  return db.manyOrNone(sql.dcpsFind, {
    date: date
  }).map(buildBlockModel);
};

exports.get = function (blockIdEnc) {
  var ids = hashids.raw.decode(blockIdEnc),
    stageId = ids[0],
    dateslotId = ids[1];

  return db.one(sql.blockGet, {
    stageId: stageId,
    dateslotId: dateslotId
  }).then(buildBlockModel).catch(QueryResultError, function(error) {
    if (error.code === qrec.noData) {
      throw ModelNotFoundError({
        model: 'block',
        prop: 'id',
        id: blockIdEnc
      });
    }
    throw error;
  });
};

exports.getCurrentRenderBlocks = function () {
  return db.manyOrNone(sql.dcpGetUnrenderedBlocksForKW).map(function (row) {
    return hashids.raw.encode(row.stage_id, row.dateslot_id);
  });
};

exports.removeOldDcps = function () {
  return db.tx((t) => {
    return t.manyOrNone(sql.dcpRemoveOld)
      .map((row) => {
        return row.storagekey;
      })
      .then(function(storageKeys) {
        return storage.deleteFiles(storageKeys)
          .then(() => {
            return storageKeys.length;
          });
      });
  });
};

exports.renderBlock = function (blockIdEnc) {
  var ids = hashids.raw.decode(blockIdEnc),
    stageId = ids[0],
    dateslotId = ids[1],
    stageDateQuery = {
      stageId: stageId,
      dateslotId: dateslotId
    };

  return db.tx(function(t) {
    var getBlock,
      getSegments,
      addDcp;

    getBlock = t.one(sql.dcpGetBlock, stageDateQuery)
      .then((block) => {
        return jwt.sign({
          blockId: blockIdEnc
        }).then((token) => {
          block.token = token;
          return block;
        });
      })
      .catch(QueryResultError, function(error) {
        if (error.code === qrec.noData) {
          throw ModelNotFoundError({
            model: 'dcp',
            prop: 'id',
            id: blockIdEnc
          });
        }
        throw error;
      });

    getSegments = t.manyOrNone(sql.dcpGetBlockSegments, stageDateQuery)
      .then(function (rows) {
        var muteSegment = [],
          audioSegment = [];

        rows.forEach(function(row) {
          if (row.sujet_storagekey_intermediatefile) {
            if (row.segment_has_audio === 0) {
              muteSegment.push(row.sujet_storagekey_intermediatefile);
            } else {
              audioSegment.push(row.sujet_storagekey_intermediatefile);
            }
          }
        });
        return {
          mute: muteSegment,
          audio: audioSegment
        };
      });

    addDcp = t.one(sql.dcpUpsertOne, stageDateQuery);

    return t.batch([getBlock, getSegments, addDcp])
      .spread(function(block, segments, dcp) {
        var cplPrefix = dcpBaseName(block.kw, block.stage_name_short, dcp.version),
          taskData = {
            blockId: blockIdEnc,
            blockhash: block.blockhash,
            issuer: issuer,
            spaceFrames: spaceFrames,
            blockinfo: {
              enddate: block.enddate,
              blockUrl: `${apphost}/blocks/${blockIdEnc}`,
              downLoadUrl: `${apihost}/blocks/${blockIdEnc}/dcp?token=${block.token}`,
              kw: block.kw,
              stagename: block.stage_name,
              stageNameShort: block.stage_name_short,
              companyName: block.company_name,
              companyNameShort: block.company_name_short
            },
            fps: block.stage_fps,
            uploadFTP: String(config.get('ftp.uploadActive')).split('.').indexOf(block.company_name_short) !== -1,
            name: cplPrefix,
            ftpFolderName: dcpBaseName(block.kw, block.stage_name_short, dcp.version),
            segments: {}
          };

        if (segments.mute.length > 0) {
          taskData.segments.mute = {
            name: fullCPL(cplPrefix, false, dcp.version),
            keys: segments.mute
          };
        }
        if (segments.audio.length > 0) {
          taskData.segments.audio = {
            name: fullCPL(cplPrefix, true, dcp.version),
            keys: segments.audio
          };
        }
        return jobqueue.newJob(BLOCK_DCP_TASKNAME, taskData);
      });
  }).then(function() {
    return db.one(sql.blockGet, stageDateQuery)   
      .then(buildBlockModel);
  });
};

exports.getUrl = function (blockItem, time) {
  return storage.getSignedGETUrl(blockItem.dcpStoragekey, time);
};