'use strict';

var salt = require('config').get('hashidsalt'),
  alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ',
  Hashids = require('hashids'),
  hashids = new Hashids(salt, 10, alphabet);

module.exports = {
  raw: hashids,
  encode: function (id) {
    return hashids.encode(id);
  },
  decode: function(hashid) {
    var id = hashids.decode(hashid);

    if (id.length === null) {
      return null;
    }
    return id[0];
  }
};
