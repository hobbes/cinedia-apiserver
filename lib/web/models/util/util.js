'use strict';

var hashids = require('./hashids'),
  moment = require('moment');

function mysqlToUnixTimestamp(mysqlTimestamp) {
  var time = moment(mysqlTimestamp);

  return Number(time.format('X'));
}

function unixToMysqlTimestamp(unixTimestamp) {
  var time = moment(unixTimestamp, 'X');

  return time.format('YYYY-MM-DD HH:mm:ss');
}

exports.encodeRowIdProperties = function(row, idProps) {
  idProps.forEach(function(idProp) {
    if (row[idProp]) {
      row[idProp] = hashids.encode(row[idProp]);
    }
  });
  return row;
};
exports.decodeRowIdProperties = function(row, idProps) {
  idProps.forEach(function(idProp) {
    if (row[idProp]) {
      row[idProp] = hashids.decode(row[idProp]);
    }
  });
  return row;
};

exports.renameRowProperties = function (row, map) {
  var rowPropNames = Object.keys(map);

  rowPropNames.forEach(function (propName) {
    var value = row[propName];

    if (value !== undefined) {
      delete row[propName];
      row[map[propName]] = value;
    }
  });
  return row;
};

exports.timeStampToVersion = function (mysqlTimestamp) {
  return hashids.encode(mysqlToUnixTimestamp(mysqlTimestamp));
};

exports.versionToTimestamp = function (version) {
  return unixToMysqlTimestamp(hashids.decode(version));
};

exports.swap = function(input){
  let swapped = {};

  for(let key in input){
    swapped[input[key]] = key;
  }

  return swapped;
};

exports.mapKeysToArray = function(input){
  let arr = [];

  for(let key in input){
    arr.push(key);
  }

  return arr;
};

exports.extractIdsToEncodeDecodeFromMap = function (propMap) {
  let arr = [];

  for(let key in propMap){
    if (key.substr(-3) === '_id') {
      arr.push(key);
    }
  }
      
  return arr;
};