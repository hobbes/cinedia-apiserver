'use strict';

const createModel = require('./generic'),
  propMap = {
    'activity_id': 'activityId',
    'name': 'name'
  },
  model = createModel({
    tableName: 'activities',
    propMap,
    idName: 'activity_id'
  });

function findUserActivities(dbOrT, userIdEnc) {
  let userId = model._hashids.decode(userIdEnc);

  return dbOrT.manyOrNone(model._sql.findUserActivities, {
    userId
  }).map(model._buildModel);
}

model.findUserActivities = function(userIdEnc) {
  return findUserActivities(model._db, userIdEnc);
};

model.addUserActivity = function(userIdEnc, activityIdEnc) {
  let pgp = model._db.$config.pgp,
    userId = model._hashids.decode(userIdEnc),
    activityId = model._hashids.decode(activityIdEnc),
    query = pgp.helpers.insert({
      user_id: userId,
      activity_id: activityId
    }, ['user_id', 'activity_id'], 'users_activities');

  return model._db.tx((t) => {
    return t.result(query).then(() => {
      return findUserActivities(t, userIdEnc);
    });
  });
};

model.deleteUserActivity = function(userIdEnc, activityIdEnc) {
  let userId = model._hashids.decode(userIdEnc),
    activityId = model._hashids.decode(activityIdEnc);

  return model._db.tx((t) => {
    return t.result(model._sql.user.removeActivity, {
      userId,
      activityId
    }).then(() => {
      return findUserActivities(t, userIdEnc);
    });
  });
};

module.exports = model;