'use strict';

const createModel = require('./generic'),
  propMap = {
    'user_id': 'userId',
    'customer_id': 'customerId',
    'email': 'email',
    'firstname': 'firstname',
    'lastname': 'lastname',
    'flag_notification_dcpready': 'getEmails',
    'created_at': 'createdAt',
    'updated_at': 'updatedAt'
  };

module.exports = createModel({
  tableName: 'users',
  propMap,
  idName: 'user_id'
});