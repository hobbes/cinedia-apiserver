'use strict';

var _ = require('lodash'),
  db = require('../db'),
  ModelNotFoundError = require('./error').ModelNotFoundError,
  QueryResultError = db.$config.pgp.errors.QueryResultError,
  qrec = db.$config.pgp.errors.queryResultErrorCode,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  encodeRowIdProperties = require('./util/util').encodeRowIdProperties,
  renameRowProperties = require('./util/util').renameRowProperties,
  idsToEncode,
  userPropMap;

userPropMap = {
  'user_id': 'userId',
  'customer_id': 'customerId'
};

idsToEncode = [
  'user_id',
  'customer_id',
  'stage_id'
];

module.exports = {
  findUserByEmail: function(email) {
    return db.tx('loadAuthuser', function(t) {
      return t.one(sql.authenticationUserGetByEmail, {
        email: email
      })
        .then(function(row){
          encodeRowIdProperties(row, idsToEncode);
          renameRowProperties(row, userPropMap);
          return row;
        })
        .then(function(user) {
          var customerId = null,
            getActivities,
            getStagesWriteAccess,
            getStagesReadAccess,
            getCustomers;

          if (user.customerId) {
            customerId = hashids.decode(user.customerId);
          }

          getActivities = t.manyOrNone(sql.findUserActivities, {
            userId: hashids.decode(user.userId)
          }).map(function (row) {
            return row.name;
          });
                
          getStagesWriteAccess = t.manyOrNone(sql.stageWriteAccessLoadIdsByUserAccess, {
            customerId: customerId
          }).map(function(row){
            return hashids.encode(row.stage_id);
          });

          getStagesReadAccess = t.manyOrNone(sql.stageReadAccessLoadIdsByUserAccess, {
            customerId: customerId
          }).map(function(row){
            return hashids.encode(row.stage_id);
          });

          getCustomers = t.manyOrNone(sql.customerLoadIdsByUserAccess, {
            customerId: customerId
          }).map(function(row){
            return hashids.encode(row.access_read_customer_id);
          });

          return t.batch([getActivities, getStagesWriteAccess, getStagesReadAccess,getCustomers])
            .spread(function(activities, stagesWrite, stagesRead, customers) {
              return {
                id: user.userId,
                email: user.email,
                firstname: user.user_firstname,
                lastname: user.user_lastname,
                customer: {
                  id: user.customerId,
                  name: user.customer_name,
                  nameshort: user.customer_nameshort,
                  type: user.customer_type
                },
                activities: activities,
                access: {
                  stages: {
                    read: _.union(stagesRead, stagesWrite),
                    write: stagesWrite
                  },
                  customers: {
                    read: customers
                  }
                }
              };
            });
        })
        .catch(QueryResultError, function(error) {
          if (error.code === qrec.noData) {
            throw ModelNotFoundError({
              model: 'user',
              prop: 'email',
              id: email
            });
          } else {
            throw error;
          }
        });
    });
  },
  getPwdHash: function(email) {
    return db.one(sql.authenticationLoadPwdHash, {
      email: email
    })
      .then(function(row){
        return row.pwhash;
      })
      .catch(QueryResultError, function(error) {
        if (error.code === qrec.noData) {
          throw ModelNotFoundError({
            model: 'user',
            prop: 'email',
            id: email
          });
        } else {
          throw error;
        }
      });
  },
  setPwd: function(email, pwhash) {
    return db.result(sql.authenticationSetPwdHash, {
      pwhash: pwhash,
      email: email
    })
      .then(function(result) {
        if (result.rowCount !== 1) {
          throw ModelNotFoundError({
            model: 'user',
            prop: 'email',
            id: email
          });
        } else {
          return null;
        }
      });
  }
};