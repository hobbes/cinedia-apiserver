'use strict';

var moment = require('moment'),
  db = require('../db'),
  pgPromisNoDataErrorCode = db.$config.pgp.errors.queryResultErrorCode.noData,
  sql = require('./sql'),
  hashids = require('./util/hashids'),
  config = require('config'),
  globaLimit = config.get('model.limit'),
  DateSlotBookError = require('./error').DateSlotBookError,
  encodeRowIdProperties = require('./util/util').encodeRowIdProperties,
  renameRowProperties = require('./util/util').renameRowProperties,
  idsToEncode,
  propMap;

propMap = {
  'stage_name': 'stageName',
  'location_name': 'locationName',
  'company_name': 'companyName',
  'sujet_id': 'sujetId',
  'location_id': 'locationId',
  'stage_id': 'stageId',
  'dateslot_id': 'dateslotId'
};

idsToEncode = [
  'sujet_id',
  'customer_id',
  'location_id',
  'stage_id',
  'dateslot_id'
];

function buildScheduleModel(row){
  encodeRowIdProperties(row, idsToEncode);
  renameRowProperties(row, propMap);
  return row;
}

function loadSchedule(dbOrTr, query) {
  return dbOrTr.manyOrNone(sql.loadSchedule, {
    sujetId: query.sujetId,
    customerId: query.customerId,
    startdate: moment(query.startdate, 'YYYYMMDD').toDate(),
    enddate: moment(query.enddate, 'YYYYMMDD').toDate(),
    limit: query.limit
  })
    .map(buildScheduleModel);
}

function decodeDateslotIdsEncByStageEnc(stageIds, dateslotIds, dateslotIdsEncByStageEnc) {
  Object.keys(dateslotIdsEncByStageEnc).forEach((stageIdEnc) => {
    let stageId = hashids.decode(stageIdEnc);

    stageIds.push(stageId);
    dateslotIds[stageId] = dateslotIdsEncByStageEnc[stageIdEnc].map((dateslotIdEnc) => {
      return hashids.decode(dateslotIdEnc);
    });
  });
}

exports.get = function(sujetIdEnc, customerIdEnc, startdate, enddate) {
  var sujetId = hashids.decode(sujetIdEnc);

  return loadSchedule(db, {
    sujetId: sujetId,
    customerId: hashids.decode(customerIdEnc),
    startdate: startdate,
    enddate: enddate,
    limit: 500
  });
};

exports.bookDateslot = function(sujetIdEnc, dateslotIdsEncByStageEnc, customerIdEnc, startdate, enddate) {
  var customerId = hashids.decode(customerIdEnc),
    sujetId = hashids.decode(sujetIdEnc),
    stageIds = [],
    dateslotIds = {};

  decodeDateslotIdsEncByStageEnc(stageIds, dateslotIds, dateslotIdsEncByStageEnc);

  return db.tx('bookDateslot', function(t) {
    return t.batch(stageIds.map(function(stageId) {
      return t.batch(dateslotIds[stageId].map(function(dateslotId) {
        return t.one(sql.scheduleBook, {
          sujetId: sujetId,
          stageId: stageId,
          dateslotId: dateslotId,
          customerId: customerId
        });
      }));
    }))
      .catch(function(error){
        if (error.first.code === '23505' &&
                error.first.constraint === 'unique_sujet_per_stagedate') {
          throw DateSlotBookError({
            sujetId: sujetIdEnc,
            dateslotIds: dateslotIdsEncByStageEnc
          });
        } else {
          throw error;
        }
      })
      .then(function() {
        return loadSchedule(t, {
          sujetId: sujetId,
          customerId: customerId,
          startdate: startdate,
          enddate: enddate,
          limit: 500
        });
      });
  });
};
exports.cancelDateslot = function(sujetIdEnc, dateslotIdsEncByStageEnc, customerIdEnc, startdate, enddate) {
  var customerId = hashids.decode(customerIdEnc),
    sujetId = hashids.decode(sujetIdEnc),
    stageIds = [],
    dateslotIds = {};

  decodeDateslotIdsEncByStageEnc(stageIds, dateslotIds, dateslotIdsEncByStageEnc);

  return db.tx('cancelDateslot', function(t) {
    return t.batch(stageIds.map(function(stageId) {
      return t.batch(dateslotIds[stageId].map(function(dateslotId){
        return t.one(sql.scheduleCancelDelete, {
          sujetId: sujetId,
          stageId: stageId,
          dateslotId: dateslotId,
          customerId: customerId
        })
          .then(function(result){
            return t.manyOrNone(sql.scheduleCancelAdjustPosition, {
              position: result.position,
              stageId: stageId,
              dateslotId: dateslotId,
              customerId: customerId
            });
          });
      }));
    }))
      .catch((error) => {
        // console.log('pgPromisNoDataErrorCode', pgPromisNoDataErrorCode)
        // console.log('error----------------------', error.first.code)
        // console.log(error.first)
        if (error.first.code === pgPromisNoDataErrorCode) {
          throw DateSlotBookError({
            sujetId: sujetIdEnc,
            dateslotIds: dateslotIdsEncByStageEnc
          });
        } else {
          throw error;
        }
      })
      .then(function() {
        return loadSchedule(t, {
          sujetId: sujetId,
          customerId: customerId,
          startdate: startdate,
          enddate: enddate,
          limit: 500
        });
      });
  });
};