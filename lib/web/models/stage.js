'use strict';

const createModel = require('./generic'),
  propMap = {
    'stage_id': 'stageId',
    'stage_name': 'stageName',
    'stage_nameshort': 'stageNameShort',
    'location_id': 'locationId',
    'stage_created_at': 'createdAt',
    'stage_updated_at': 'updatedAt'
  },
  customerStagesPropMap = Object.assign({}, propMap),
  propMapModify = {
    'stage_id': 'stageId',
    'name': 'stageName',
    'nameshort': 'stageNameShort',
    'location_id': 'locationId',
    'created_at': 'createdAt',
    'updated_at': 'updatedAt'
  },
  model = createModel({
    tableName: 'view_stages',
    modifyTableName: 'stages',
    propMap,
    propMapModify,
    idName: 'stage_id',
    findOrderBy: ['location_id', 'stage_id', 'stage_name']
  });

customerStagesPropMap.company_id = 'companyId';
customerStagesPropMap.location_name = 'locationName';
customerStagesPropMap.company_name = 'companyName';
customerStagesPropMap.company_nameshort = 'companyNameShort';
customerStagesPropMap.company_lockoffsethours = 'lockoffsethours';

model.findByCostomerId = function(customerIdEnc) {
  return model._db.manyOrNone(model._sql.stagesGetByCustomerId, {
    customerId: model._hashids.decode(customerIdEnc)
  }).map((row) => {
    model._util.encodeRowIdProperties(row, ['stage_id', 'location_id', 'company_id']);
    model._util.renameRowProperties(row, customerStagesPropMap);
    row.version = model._util.timeStampToVersion(row.updatedAt);
    return row;
  });
};

model.getMultiple = function (stageIdsEnc, customerIdEnc) {
  var stageIds = stageIdsEnc.map((stageIdEnc) => {
    return model._hashids.decode(stageIdEnc);
  });

  return model._db.manyOrNone(model._sql.stagesGetMultiple, {
    customerId: model._hashids.decode(customerIdEnc),
    stageIds
  }).map(model._buildModel);
};

module.exports = model;