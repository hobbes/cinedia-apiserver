'use strict';

var Promise = require('bluebird'),
  jwt = require('jsonwebtoken'),
  config = require('config'),
  jwtsecret = config.get('jwtsecret');

exports.verify = function(token) {
  return new Promise(function(resolve, reject) {
    jwt.verify(token, jwtsecret, function(err, decoded) {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
};

exports.sign = function(data, options) {
  return new Promise(function(resolve, reject) {
    jwt.sign(data, jwtsecret, options, function(err, token) {
      if (err) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
};