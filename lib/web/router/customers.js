'use strict';

const createRouter = require('./generic'), 
  middleware = require('../middleware/customer'),
  router = createRouter({
    name: 'customer',
    middleware,
    authorization: {
      create: 'admin',
      find: 'admin',
      get(req, res, next) {
        if (middleware._userHasActivity(res, 'admin')) {
          return next();
        }
        if (middleware._getRequestUser(res).customer.id === req.params[middleware._paramIdName]) {
          return next();
        }
        return middleware._sendAuthError(next);
      },
      update: 'admin',
      delete: 'admin'
    }
  });

router.get(`/:${router._paramIdName}/access/read/customers`,
  router._can.hasActivity('admin'),
  middleware.getCustomersReadAccess,
  router._send('customersReadAccess', 'collection'));

router.post(`/:${router._paramIdName}/access/read/customers/:customerreadid`,
  router._can.hasActivity('admin'),
  middleware.addCustomersReadAccess,
  router._send('customersReadAccess', 'collection'));

router.delete(`/:${router._paramIdName}/access/read/customers/:customerreadid`,
  router._can.hasActivity('admin'),
  middleware.removeCustomersReadAccess,
  router._send('customersReadAccess', 'collection'));


router.get(`/:${router._paramIdName}/companies/write`,
  router._can.hasActivity('admin'),
  middleware.getCompanySegments,
  router._send('companySegments', 'collection'));

router.post(`/:${router._paramIdName}/companies/write/:companyid`,
  router._can.hasActivity('admin'),
  middleware.addCompanySegments,
  router._send('companySegments', 'collection'));

router.delete(`/:${router._paramIdName}/companies/write/:companyid`,
  router._can.hasActivity('admin'),
  middleware.removeCompanySegments,
  router._send('companySegments', 'collection'));


router.get(`/:${router._paramIdName}/companies/read`,
  router._can.hasActivity('admin'),
  middleware.getReadCompanies,
  router._send('companiesRead', 'collection'));

router.post(`/:${router._paramIdName}/companies/read/:companyid`,
  router._can.hasActivity('admin'),
  middleware.addReadCompany,
  router._send('companiesRead', 'collection'));

router.delete(`/:${router._paramIdName}/companies/read/:companyid`,
  router._can.hasActivity('admin'),
  middleware.removeReadCompany,
  router._send('companiesRead', 'collection'));

module.exports = router;