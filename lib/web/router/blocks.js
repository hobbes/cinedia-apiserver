'use strict';

var express = require('express'),
  router = express.Router({
    caseSensitive: true
  }),
  sendSeekable = require('send-seekable'),
  authentication = require('../middleware/authentication'),
  validation = require('../middleware/validation'),
  can = require('../middleware/can'),
  segment = require('../middleware/segment'),
  block = require('../middleware/block');

//read
router.get('/',
  validation.schema('loadBlockCollection', 'query'),
  authentication.restrict,
  authentication.limitToUserAssignedToCustomer,
  block.loadMany,
  block.sendMany);

router.get('/:blockid',
  authentication.restrict,
  block.loadOne,
  can.viewBlockItem,
  block.sendOne);

router.post('/:blockid/dcps',
  authentication.restrict,
  block.loadOne,
  can.renderDcp,
  block.dcp);

//block segment
router.get('/:blockid/segments',
  authentication.restrict,
  authentication.limitToUserAssignedToCustomer,
  segment.loadMany,
  can.viewBlockSegmentCollection,
  segment.sendMany);

//update block segment
router.put('/:blockid/segments/:segmentid/order',
  authentication.restrict,
  authentication.limitToUserAssignedToCustomer,
  block.loadOne,
  segment.loadOne,
  can.moveSujet,
  segment.updateSegmentOrder,
  segment.sendMany);

// download dcp
router.get('/:blockid/dcp',
  authentication.restrictWithPossibleQueryToken,
  block.loadOne,
  can.downloadBlock,
  sendSeekable,
  block.download);

module.exports = router;