'use strict';

var express = require('express'),
  router = express.Router({
    strict: true,
    caseSensitive: true
  }),
  authentication = require('../middleware/authentication'),
  error = require('../middleware/error'),
  version = require('../../../package.json').version;

router.get('/', function (req, res) {
  res.json({
    version: version
  });
});

router.use('/auth', require('./auth'));
router.use('/storage', require('./storage'));
router.use('/sujets', require('./sujets'));
router.use('/dateslots', require('./dateslots'));
router.use('/stages', authentication.restrict, require('./stages'));
router.use('/locations', authentication.restrict, require('./locations'));
router.use('/companies', authentication.restrict, require('./companies'));
router.use('/blocks', require('./blocks'));
router.use('/dcps', require('./dcps'));

router.use('/customers', authentication.restrict, require('./customers'));
router.use('/users', authentication.restrict, require('./users'));
router.use('/activities', authentication.restrict, require('./activities'));

router.use('/', require('./bullboard'));


router.use(error.catch404);
router.use(error.catchUnknownError);
router.use(error.handleApiError);

module.exports = router;