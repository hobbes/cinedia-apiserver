'use strict';

var express = require('express'),
  router = express.Router({
    caseSensitive: true
  }),
  validation = require('../middleware/validation'),
  authentication = require('../middleware/authentication'),
  dateslot = require('../middleware/dateslot');

router.get('/',
  authentication.restrict,
  validation.schema('queryGetDateslots', 'query'),
  dateslot.loadNow,
  dateslot.loadDaterange,
  dateslot.sendDaterange);

module.exports = router;