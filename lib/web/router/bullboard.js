'use strict';

const express = require('express'),
  router = express.Router({
    strict: true,
    caseSensitive: true
  }),
  {
    setQueues,
    UI
  } = require('bull-board'),
  queue = require('../../shared/queue'),
  can = require('../middleware/can'),
  auth = require('../middleware/authentication');

setQueues([
  queue.jobs,
  queue.results
]);

router.use('/adminqueue', auth.restrict);
router.use('/adminqueue', can.adminQueue);
router.use('/adminqueue', UI);

module.exports = router;