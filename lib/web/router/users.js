'use strict';

var createRouter = require('./generic'), 
  middleware = require('../middleware/user'),
  router = createRouter({
    name: 'user',
    middleware,
    authorization: {
      create: 'admin',
      find: 'admin',
      get: middleware.canGetUser,
      update: middleware.canUpdateUser,
      delete: 'admin'
    }
  });

router.post(`/:${router._paramIdName}/emails/welcome`,
  middleware.canSendUserWelcomeMail,
  middleware.sendUserWelcomeMail,
  (req, res, next) => {
    res.status(204).end();
  }
);

router.get(`/:${router._paramIdName}/activities`,
  middleware.canFindUserActivities,
  middleware.findUserActivities,
  router._send('activities', 'collection')
);

router.post(`/:${router._paramIdName}/activities/:activityid`,
  middleware.canEditUserActivities,
  middleware.addUserActivity,
  router._send('activities', 'collection')
);

router.delete(`/:${router._paramIdName}/activities/:activityid`,
  middleware.canEditUserActivities,
  middleware.deleteUserActivity,
  router._send('activities', 'collection')
);

module.exports = router;