'use strict';

var express = require('express'),
  mime = require('mime-types'),
  validation = require('../middleware/validation'),
  createUniqueStorageKey = require('../../shared/storage').newUniqueKey,
  storage = require('../storage'),
  router = express.Router({
    caseSensitive: true
  }),
  authentication = require('../middleware/authentication');

router.use(authentication.restrict);

router.post('/uploads',
  validation.schema('getSignedPutUrl', 'body'),
  function (req, res, next) {
    var body = req.body,
      mimeTypeBasedOnFilename = mime.lookup(body.filename) || 'application/octet-stream',
      extension = mime.extension(mimeTypeBasedOnFilename),
      newKey = createUniqueStorageKey(extension),
      url = storage.getSignedPUTUrl(newKey, mimeTypeBasedOnFilename, new Date(Date.now() + 5 * 60 * 1000));

    res.status(201).json({
      key: newKey,
      url: url,
      contentType: mimeTypeBasedOnFilename
    });
  });

module.exports = router;