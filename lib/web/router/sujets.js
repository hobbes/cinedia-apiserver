'use strict';

var express = require('express'),
  router = express.Router({
    caseSensitive: true
  }),
  sendSeekable = require('send-seekable'),
  authentication = require('../middleware/authentication'),
  validation = require('../middleware/validation'),
  can = require('../middleware/can'),
  sujet = require('../middleware/sujet'),
  schedule = require('../middleware/schedule'),
  file = require('../middleware/file'),
  stage = require('../middleware/stage'),
  dateslot = require('../middleware/dateslot');

//params autoload
router.param('sujetid', sujet.loadOne);
router.param('stageid', stage.get);
router.param('dateslotid', dateslot.loadOne);

router.use(authentication.restrict);

//create
router.post('/',
  validation.schema('newSujet', 'body'),
  authentication.limitToUserAssignedToCustomer,
  can.createSujet,
  sujet.create);

//read
router.get('/',
  validation.schema('queryGetSujets', 'query'),
  authentication.limitToUserAssignedToCustomer,
  sujet.loadMany,
  sujet.sendSujetCollection);

router.get('/:sujetid',
  can.viewSujetItem,
  sujet.sendSujetItem);

router.get('/:sujetid/progress',
  can.viewSujetItem,
  function (req, res, next) {
    res.sseSetup = function() {
      res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'X-Accel-Buffering': 'no'
      });
      res.flush();
    };

    res.sseSendData = function(data) {
      res.write(`data: ${JSON.stringify(data)}\n\n`);
      res.flush();
    };

    res.sseSendEventData = function(event, data) {
      res.write(`event: ${event}\n`);
      res.write(`data: ${JSON.stringify(data)}\n\n`);
      res.flush();
    };

    next();
  },
  sujet.sendProgress);

//preview images
router.get('/:sujetid/previews/:type',
  can.viewSujetItem,
  sujet.loadPreview,
  sendSeekable,
  file.send);

//read schedule
router.get('/:sujetid/schedule',
  validation.schema('loadSchedule', 'query'),
  schedule.load,
  can.viewSujetSchedule,
  schedule.send);

//report intermiediate error
router.post('/:sujetid/reports/errors/validation',
  can.reportValidationError,
  sujet.reportValidationError,
  sujet.sendSujetItem);

router.get('/:sujetid/sourcefile',
  can.getSourcefile,
  sendSeekable,
  sujet.getSourcefile);

router.put('/:sujetid/sourcefile',
  can.replaceSourcefile,
  sujet.replaceSourcefile,
  sujet.sendSujetItem);


//update name
router.put('/:sujetid/name', 
  validation.schema('updateSujet', 'body'),
  can.updateSujetItemName,
  sujet.updateName,
  sujet.sendSujetItem);

//delete
router.delete('/:sujetid',
  validation.schema('versionBody', 'body'),
  can.deleteSujetItem,
  sujet.del);

//book dateslots
router.put('/:sujetid/booking',
  validation.schema('loadSchedule', 'query'),
  validation.schema('dateslotBooking', 'body'),
  stage.getMultiple,
  dateslot.loadNow,
  dateslot.loadMultiple,
  can.bookDateslot,
  schedule.book,
  schedule.send);

//cancel dateslots
router.delete('/:sujetid/booking',
  validation.schema('loadSchedule', 'query'),
  validation.schema('dateslotBooking', 'body'),
  stage.getMultiple,
  dateslot.loadNow,
  dateslot.loadMultiple,
  can.bookDateslot,
  schedule.cancel,
  schedule.send);

module.exports = router;