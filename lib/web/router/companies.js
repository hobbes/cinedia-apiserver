'use strict';

const createRouter = require('./generic'), 
  middleware = require('../middleware/companies'),
  router = createRouter({
    name: 'company',
    middleware,
    authorization: {
      create: 'admin',
      find: 'admin',
      get: 'admin',
      update: 'admin',
      delete: 'admin'
    }
  });

router.get(`/:${router._paramIdName}/blockorder/:segmenttype`,
  router._can.hasActivity('admin'),
  middleware.getBlockOrder,
  router._send('blockorder', 'collection')
);
router.put(`/:${router._paramIdName}/blockorder/:segmenttype`,
  router._can.hasActivity('admin'),
  middleware.setBlockOrder,
  router._send('blockorder', 'collection')
);

module.exports = router;