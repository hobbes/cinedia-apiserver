'use strict';

const createRouter = require('./generic'), 
  middleware = require('../middleware/activities'),
  router = createRouter({
    name: 'activities',
    middleware,
    authorization: {
      find: 'admin',
      get: 'admin'
    },
    disabledRoutes: {
      create: true,
      update: true,
      delete: true
    }
  });

module.exports = router;