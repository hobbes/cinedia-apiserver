'use strict';

var express = require('express'),
  router = express.Router({
    caseSensitive: true
  }),
  validation = require('../middleware/validation'),
  can = require('../middleware/can'),
  authentication = require('../middleware/authentication'),
  dcps = require('../middleware/dcps');

router.use(authentication.restrict);

router.get('/',
  validation.schema('loadBlockCollection', 'query'),
  can.viewAllDcps,
  dcps.loadAll,
  dcps.sendAll);

module.exports = router;