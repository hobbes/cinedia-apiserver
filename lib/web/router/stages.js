'use strict';

const createRouter = require('./generic'), 
  middleware = require('../middleware/stage'),
  router = createRouter({
    name: 'stage',
    middleware,
    authorization: {
      create: 'admin',
      get: 'admin',
      update: 'admin',
      delete: 'admin'
    }
  });

module.exports = router;