'use strict';

var express = require('express'),
  router = express.Router({
    strict: true,
    caseSensitive: true
  }),
  validation = require('../middleware/validation'),
  auth = require('../middleware/authentication');

router.post('/check',
  auth.restrict,
  auth.check);

router.post('/logout', auth.logout);

router.post('/nopwd',
  validation.schema('emailRedirectpathBody', 'body'),
  auth.nopwdLogin);

router.post('/nopwd/return',
  validation.schema('tokenBody', 'body'),
  auth.nopwdLoginReturn);

router.post('/pwd',
  validation.schema('emailPasswordBody', 'body'),
  auth.pwdLogin);

router.post('/pwd/reset',
  validation.schema('emailRedirectpathBody', 'body'),
  auth.pwdreset);

router.post('/pwd/reset/return',
  validation.schema('tokenPasswordRepetitionBody', 'body'),
  auth.pwdresetreturn);
    
module.exports = router;