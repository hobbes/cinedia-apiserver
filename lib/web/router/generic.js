'use strict';

var express = require('express'),
  can = require('../middleware/can'),
  send = require('../middleware/util/helper').send;

module.exports = function (
  {
    name,
    middleware,
    disabledRoutes = {},
    validation = {},
    authorization = {}
  }
) {
  const paramName = `${name}id`,
    router = express.Router({
      caseSensitive: true
    }),
    createMiddleware = [
      middleware.create,
      send(name, 'item')
    ],
    findMiddleware = [
      middleware.find,
      send(name, 'collection')
    ],
    getMiddleware = [
      send(name, 'item')
    ],
    updateMiddleware = [
      middleware.update,
      send(name, 'item')
    ],
    deleteMiddleware = [
      can.assertSameVersion(name),
      middleware.delete
    ];

  if (authorization.create) {
    if (typeof authorization.create === 'string') {
      createMiddleware.unshift(can.hasActivity(authorization.create));
    } else {
      createMiddleware.unshift(authorization.create);
    }
  }
  if (authorization.find) {
    if (typeof authorization.find === 'string') {
      findMiddleware.unshift(can.hasActivity(authorization.find));
    } else {
      findMiddleware.unshift(authorization.find);
    }
  }
  if (authorization.get) {
    if (typeof authorization.get === 'string') {
      getMiddleware.unshift(can.hasActivity(authorization.get));
    } else {
      getMiddleware.unshift(authorization.get);
    }
  }
  if (authorization.update) {
    if (typeof authorization.update === 'string') {
      updateMiddleware.unshift(can.hasActivity(authorization.update));
    } else {
      updateMiddleware.unshift(authorization.update);
    }
  }
  if (authorization.delete) {
    if (typeof authorization.delete === 'string') {
      deleteMiddleware.unshift(can.hasActivity(authorization.delete));
    } else {
      deleteMiddleware.unshift(authorization.delete);
    }
  }

  //TODO
  //validation unshift middleware
  if (validation.create) {

  }
  if (validation.find) {
    
  }
  if (validation.get) {
    
  }
  if (validation.update) {
    
  }
  if (validation.delete) {
    
  }

  router.param(paramName, middleware.get);

  if (!disabledRoutes.create) {
    router.post('/', createMiddleware);
  }
  if (!disabledRoutes.find) {
    router.get('/', findMiddleware);
  }
  if (!disabledRoutes.get) {
    router.get(`/:${paramName}`, getMiddleware);
  }
  if (!disabledRoutes.update) {
    router.patch(`/:${paramName}`, updateMiddleware);
  }
  if (!disabledRoutes.delete) {
    router.delete(`/:${paramName}`, deleteMiddleware);
  }

  if (!router._paramIdName && !router._send && !router._can) {
    router._paramIdName = paramName;
    router._send = send;
    router._can = can;
  } else {
    throw new Error('router prop conflict with express');
  }

  return router;
};