'use strict';

const createRouter = require('./generic'), 
  middleware = require('../middleware/locations'),
  router = createRouter({
    name: 'location',
    middleware,
    authorization: {
      create: 'admin',
      find: 'admin',
      get: 'admin',
      update: 'admin',
      delete: 'admin'
    }
  });

module.exports = router;