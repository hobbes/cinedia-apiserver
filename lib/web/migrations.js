'use strict';

var pgp = require('pg-promise'),
  path = require('path'),
  db = require('./db'),
  QueryFile = pgp.QueryFile,
  enumSql = pgp.utils.enumSql,
  sql = enumSql(path.join(__dirname, '../migrations'), {
    recursive: true
  }, function(file) {
    return new QueryFile(file);
  });

module.exports = {
  up: function () {
    return db.none(sql.up);
  },
  down: function () {
    return db.none(sql.down);
  }
};