'use strict';

var createGenericMiddleware = require('./generic'),
  model = require('../models/activities'),
  resource = require('../resource/activities');

module.exports = createGenericMiddleware({
  name: 'activities',
  resource,
  model
});