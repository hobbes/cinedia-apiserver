'use strict';

var createGenericMiddleware = require('./generic'),
  model = require('../models/companies'),
  resource = require('../resource/companies'),
  middleware = createGenericMiddleware({
    name: 'company',
    resource,
    model
  });

function buildBlockOrderResource(model) {
  return {
    customer: {
      id: model.customerId,
      type: model.type,
      name: {
        full: model.name,
        short: model.nameshort
      }
    }
  };
}

middleware.getBlockOrder = function(req, res, next) {
  let companyId = req.params[middleware._paramIdName],
    segmentType = req.params.segmenttype;

  model.getBlockOrder(companyId, segmentType)
    .then((blockorderCollection) => {
      middleware._helper.setLocals(res, 'blockorder', 'collection', {
        model: blockorderCollection,
        resource: blockorderCollection.map(buildBlockOrderResource)
      });
      next();
    })
    .catch(next);
};

middleware.setBlockOrder = function(req, res, next) {
  let companyId = req.params[middleware._paramIdName],
    segmentType = req.params.segmenttype,
    newCustomerOrder = req.body;

  model.setBlockOrder(companyId, segmentType, newCustomerOrder)
    .then((blockorderCollection) => {
      middleware._helper.setLocals(res, 'blockorder', 'collection', {
        model: blockorderCollection,
        resource: blockorderCollection.map(buildBlockOrderResource)
      });
      next();
    })
    .catch(next);
};

module.exports = middleware;