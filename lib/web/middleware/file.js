'use strict';

var storage = require('../storage'),
  ClientError = require('../error').ClientError;

exports.send = function (req, res, next) {
  var storagefile = res.locals.storagefile,
    ifnonmatch = req.get('If-None-Match');

  if (!storagefile) {
    return next(ClientError({
      title: 'no preview yet',
      statusCode: 404
    }));
  }

  if (ifnonmatch === storagefile.etag) {
    res.status(304);
    res.set({
      'ETag': storagefile.etag,
      'Cache-Control': 'private, max-age=31536000'
    });
    return res.end();
  }

  storage.getReadStream(storagefile.key)
    .then(function ({sosReq, sosRes}) {
      var headers = sosRes.headers;

      req.on('close', () => {
        sosReq.abort();
      });

      if (headers['content-type'].startsWith('video/')) {
        res.set({
          'ETag': headers.etag,
          'Cache-Control': 'private, max-age=31536000'
        });
        res.sendSeekable(sosRes, {
          type: headers['content-type'],
          length: headers['content-length']
        });
      } else {
        res.set({
          'content-type': headers['content-type'],
          'content-length': headers['content-length'],
          'ETag': headers.etag,
          'Cache-Control': 'private, max-age=31536000'
        });
        sosRes.pipe(res);
      }
    })
    .catch(next);
};