'use strict';

var createGenericMiddleware = require('./generic'),
  model = require('../models/stage'),
  resource = require('../resource/stage'),
  middleware = createGenericMiddleware({
    name: 'stage',
    resource,
    model
  });

middleware.find = function (req, res, next) {
  var customerId = middleware._helper.getRequestCustomerId(res);

  if (customerId) {
    model.findByCostomerId(customerId)
      .then(function(modelCollection){
        middleware._helper.setLocals(res, 'stage', 'collection', {
          model: modelCollection,
          resource: resource.buildCollectionCustomerStages(modelCollection)
        });
        next();
      })
      .catch(next); 
  } else {
    model.find(req.query)
      .then(middleware._setLocalsCollection(req, res, next))
      .catch(next);
  }
};

middleware.getMultiple = function(req, res, next) {
  var customerId = middleware._helper.getRequestCustomerId(res),
    stageIds = req.body.stages;

  model.getMultiple(stageIds, customerId)
    .then(middleware._setLocalsCollection(req, res, next))
    .catch(next);
};

module.exports = middleware;