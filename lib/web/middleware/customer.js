'use strict';

const createGenericMiddleware = require('./generic'),
  model = require('../models/customer'),
  modelCompanies = require('../models/companies'),
  resource = require('../resource/customer'),
  resourceCompanies = require('../resource/companies'),
  middleware = createGenericMiddleware({
    name: 'customer',
    resource,
    model
  });

middleware.getCustomersReadAccess = function (req, res, next) {
  const customerId = req.params[middleware._paramIdName];

  model.findCustomersReadAccess(customerId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'customersReadAccess', 'collection', {
        model: modelCollection,
        resource: resource.buildCollection(modelCollection)
      });
      next();
    })
    .catch(next);
};
middleware.addCustomersReadAccess = function (req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    customerReadId = req.params.customerreadid;

  model.addCustomersReadAccess(customerId, customerReadId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'customersReadAccess', 'collection', {
        model: modelCollection,
        resource: resource.buildCollection(modelCollection)
      });
      next();
    })
    .catch(next);
};
middleware.removeCustomersReadAccess = function (req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    customerReadId = req.params.customerreadid;

  model.removeCustomersReadAccess(customerId, customerReadId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'customersReadAccess', 'collection', {
        model: modelCollection,
        resource: resource.buildCollection(modelCollection)
      });
      next();
    })
    .catch(next);
};

middleware.getCompanySegments =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName];

  modelCompanies.getCompanySegments(customerId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companySegments', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};
middleware.addCompanySegments =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    companyId = req.params.companyid;

  modelCompanies.addCompanySegments(customerId, companyId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companySegments', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};
middleware.removeCompanySegments =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    companyId = req.params.companyid;

  modelCompanies.removeCompanySegments(customerId, companyId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companySegments', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};




middleware.getReadCompanies =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName];

  modelCompanies.getReadCompanies(customerId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companiesRead', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};
middleware.addReadCompany =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    companyId = req.params.companyid;

  modelCompanies.addReadCompany(customerId, companyId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companiesRead', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};
middleware.removeReadCompany =  function(req, res, next) {
  const customerId = req.params[middleware._paramIdName],
    companyId = req.params.companyid;

  modelCompanies.removeReadCompany(customerId, companyId)
    .then((modelCollection) => {
      middleware._helper.setLocals(res, 'companiesRead', 'collection', {
        model: modelCollection,
        resource: resourceCompanies.buildCollection(modelCollection)
      });
      next();
    }).catch(next);
};

module.exports = middleware;