'use strict';

var ClientError = require('../error').ClientError,
  ModelNotFoundError = require('../models/error').ModelNotFoundError,
  blockModel = require('../models/block'),
  blockResource = require('../resource/block'),
  storage = require('../storage'),
  block = {};

block.loadMany = function (req, res, next) {
  var customerIdsReadAccess,
    stageIdsReadAccess,
    date = req.query.date;

  customerIdsReadAccess = res.locals.userItem.access.customers.read;
  stageIdsReadAccess = res.locals.userItem.access.stages.read;
  
  blockModel.find(date, customerIdsReadAccess, stageIdsReadAccess)
    .then(function (blocks) {
      res.locals.blockCollection = blocks;
      next();
    })
    .catch(next); 
};
block.sendMany = function (req, res) {
  res.status(200).json(blockResource.buildBlockCollection(res.locals));
};

block.loadOne = function (req, res, next) {
  var blockId = req.params.blockid;

  blockModel.get(blockId)
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no block with id: ' + blockId,
        statusCode: 404
      }));
    })
    .then(function (block) {
      res.locals.blockItem = block;
      next();
    })
    .catch(next);
};

block.sendOne = function (req, res) {
  res.status(200).json(blockResource.buildBlockItem(res.locals));
};

block.download = function (req, res, next) {
  var block = res.locals.blockItem;

  storage.getReadStream(block.dcpStoragekey, {})
    .then(({sosReq, sosRes}) => {
      var headers = sosRes.headers;

      req.on('close', () => {
        sosReq.abort();
      });

      res.set({
        'Cache-Control': 'no-cache',
        'Content-Disposition': `attachment; filename="${block.name}.zip"`
      });
      res.sendSeekable(sosRes, {
        type: headers['content-type'],
        length: headers['content-length']
      });
    });
};

block.dcp = function (req, res, next) {
  var blockId = req.params.blockid;    

  blockModel.renderBlock(blockId)
    .then(function (block) {
      res.locals.blockItem = block;

      res.status(202).json(blockResource.buildBlockItem(res.locals));
    })
    .catch(next);
};

module.exports = block;