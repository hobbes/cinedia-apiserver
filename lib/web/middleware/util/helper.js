'use strict';

var moment = require('moment');

exports.getRequestCustomerId = function (res) {
  return res.locals.userItem.customer.id;
};

exports.getRequestUser = function (res) {
  return res.locals.userItem;
};

exports.setLocals = function (res, name, type, data) {
  if (!res.locals.models) {
    res.locals.models = {};
  }
  if (!res.locals.resources) {
    res.locals.resources = {};
  }
  if (!res.locals.models[name]) {
    res.locals.models[name] = {};
  }
  if (!res.locals.resources[name]) {
    res.locals.resources[name] = {};
  }
  res.locals.models[name][type] = data.model;
  res.locals.resources[name][type] = data.resource;
};

exports.send = function (resourceName, resourceType) {
  return function (req, res) {
    let model = res.locals.models[resourceName][resourceType],
      resource = res.locals.resources[resourceName][resourceType];

    res.set('Last-Modified', moment(model.updatedAt).toString());
    res.status(200).json(resource);
  };
};