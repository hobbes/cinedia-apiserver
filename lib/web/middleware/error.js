'use strict';

var error = require('../error'),
  log = require('../../shared/logger'),
  ClientError = error.ClientError,
  WrappedServerError = error.WrappedServerError;

exports.catch404 = function (req, res, next) {
  next(ClientError({
    title: 'not found',
    statusCode: 404
  }));
};

exports.catchUnknownError = function (err, req, res, next) {
  if (err.statusCode) {
    return next(err);
  }
  log.error(err);
  return next(WrappedServerError(err, {
    title: 'unknown',
    statusCode: 500
  }));
};

exports.handleApiError = function (err, req, res, next) {
  res.status(err.statusCode).json({
    type: err.type,
    name: err.name,
    statusCode: err.statusCode,
    title: err.title,
    message: err.message
  });
};