'use strict';

var config = require('config'),
  storage = require('../storage'),
  mime = require('mime-types'),
  getRequestUser = require('./util/helper').getRequestUser,
  sujetModel = require('../models/sujet'),
  queue = require('../../shared/queue'),
  sujetResource = require('../resource/sujet'),
  log = require('../../shared/logger'),
  _ = require('lodash'),
  preview = require('../models/preview'),
  getRequestCustomerId = require('./util/helper').getRequestCustomerId,
  error = require('../error'),
  ModelNotFoundError = require('../models/error').ModelNotFoundError,
  ClientError = error.ClientError,
  defaultLimit = config.get('collection.limit'),
  sujet = {};

sujet.create = function(req, res, next) {
  var customerId = getRequestCustomerId(res);

  req.body.name = req.body.name.trim();
  sujetModel.create(req.body, customerId)
    .then(function(newSujet) {
      res.status(201).json(sujetResource.buildSujetItem({
        sujetItem: newSujet
      }));
    })
    .catch(next);
};

sujet.loadMany = function(req, res, next) {
  var customerId = getRequestCustomerId(res);

  if (!req.query.limit) {
    req.query.limit = defaultLimit;
  }
  sujetModel.findByCustomer(req.query, customerId)
    .then(function(sujets) {
      res.locals.sujetCollection = sujets;
      next();
    })
    .catch(next); 
};
sujet.sendSujetCollection = function(req, res) {
  var sujetCollection = sujetResource.buildSujetCollection(res.locals);

  res.status(200).json(sujetCollection);
};

sujet.loadOne = function(req, res, next, sujetId) {
  sujetModel.get(sujetId)
    .then(function(sujet) {
      res.locals.sujetItem = sujet;
      next();
    })
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no sujet with id: ' + sujetId,
        statusCode: 404
      }));
    })
    .catch(next);
};
sujet.sendSujetItem = function(req, res) {
  var sujetItem = sujetResource.buildSujetItem(res.locals);

  res.status(200).json(sujetItem);
};

sujet.sendProgress = function(req, res) {
  var sujetModel = res.locals.sujetItem,
    sourcefileValidationJobId = sujetModel.sourcefileValidationJobId,
    sourcefileValidationResultJobId = `${sourcefileValidationJobId}.result`,
    intermediateJobId = sujetModel.intermediateJobId,
    intermediateResultJobId = `${intermediateJobId}.result`;

  function createReportProgressFunction(progressType, jobIdFromDb, res) {
    return function(JobIDFromQueue, progress) {
      if (jobIdFromDb === JobIDFromQueue) {
        log.debug(`route progress sse ${progressType}, jobid: ${JobIDFromQueue}`, progress);
        res.sseSendEventData(progressType, progress);
      }
    };
  }
  
  if (sourcefileValidationJobId || intermediateJobId || sourcefileValidationResultJobId || intermediateResultJobId) {
    log.debug('setup sse');
    res.sseSetup();
  }

  if (sourcefileValidationJobId || sourcefileValidationResultJobId) {
    let reportProgress = createReportProgressFunction('sourcefilevalidation', sourcefileValidationJobId, res);

    queue.jobs.on('global:progress', reportProgress);
    req.on('close', () => {
      queue.jobs.removeListener('global:progress', reportProgress);
    });
  }

  if (intermediateJobId || intermediateResultJobId) {
    let reportProgress = createReportProgressFunction('intermediate', intermediateJobId, res);

    queue.jobs.on('global:progress', reportProgress);
    req.on('close', () => {
      queue.jobs.removeListener('global:progress', reportProgress);
    });
  }
};

sujet.updateName = function(req, res, next) {
  var customerId = getRequestCustomerId(res),
    sujetId = req.params.sujetid;

  sujetModel.updateName(sujetId, req.body.name.trim(), customerId)
    .then(function (updatedSujet) {
      res.locals.sujetItem = updatedSujet;
      next();
    })
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no sujet with id: ' + sujetId,
        statusCode: 404
      }));
    })
    .catch(next);
};

sujet.reportValidationError = function(req, res, next) {
  var appHost = config.get('apphost'),
    sujet = res.locals.sujetItem,
    user = getRequestUser(res),
    data = {
      user: {
        email: user.email
      },
      customer: {
        name: user.customer.name
      },
      sujet: {
        id: sujet.sujetId,
        name: sujet.name,
        url: `${appHost}/sujets/${sujet.sujetId}`,
        sourcefilekey: sujet.storageKeySourceFile
      }
    };

  sujetModel.reportValidationError(sujet.sujetId, data)
    .then(function(sujet) {
      res.locals.sujetItem = sujet;
      next();
    })
    .catch(next);
};

sujet.getSourcefile = function(req, res, next) {
  var sujet = res.locals.sujetItem;

  storage.getReadStream(sujet.storageKeySourceFile, {})
    .then(({sosReq, sosRes}) => {
      var headers = sosRes.headers,
        fileExtension = mime.extension(headers['content-type']);

      req.on('close', () => {
        sosReq.abort();
      });

      res.set({
        'ETag': headers.etag,
        'Cache-Control': 'private, max-age=31536000',
        'Content-Disposition': `attachment; filename="sourcefile ${sujet.sujetId} ${sujet.name}.${fileExtension}"`
      });
      res.sendSeekable(sosRes, {
        type: headers['content-type'],
        length: headers['content-length']
      });
    });
};

sujet.replaceSourcefile = function(req, res, next) {
  var sujet = res.locals.sujetItem,
    data = {
      type: sujet.type,
      filekey: req.body.filekey
    };

  if (sujet.type === 'dia') {
    data.duration = sujet.duration;
  }

  sujetModel.replaceSourceFile(sujet.sujetId, data)
    .then(function(sujet) {
      res.locals.sujetItem = sujet;
      next();
    })
    .catch(next);
};

sujet.del = function(req, res, next) {
  var customerId = getRequestCustomerId(res), 
    sujetId = req.params.sujetid;

  sujetModel.del(sujetId, customerId)
    .then(function () {
      res.status(204).end();
    })
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no sujet with id: ' + sujetId,
        statusCode: 404
      }));
    })
    .catch(next);
};

sujet.loadPreview = function(req, res, next) {
  var sujetId = req.params.sujetid,
    types = ['thumbnail', 'still', 'sequence', 'video'],
    type = 'thumbnail',
    mimeType = 'image/jpeg',
    maxSize = 1080; 

  if (req.params.type && _.includes(types, req.params.type)) {
    type = req.params.type;
  }

  if (req.query.size) {
    maxSize = req.query.size;
  }

  if (type === 'video') {
    mimeType = 'video/mp4';
  }

  preview.get(sujetId, type, mimeType, maxSize)
    .then(function(preview) {
      res.locals.storagefile = {
        key: preview.storagekey,
        etag: preview.etag
      };
      next();
    })
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no preview for sujet with id: ' + sujetId,
        statusCode: 404
      }));
    })
    .catch(next);
};

module.exports = sujet;