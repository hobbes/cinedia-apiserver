'use strict';

var _ = require('lodash'),
  error = require('../error'),
  getRequestUser = require('./util/helper').getRequestUser,
  ClientError = error.ClientError,
  moment = require('moment'),
  scheduleDateslots;

const SUJETS_CREATE = 'sujets.create',
  SUJETS_READ = 'sujets.read',
  SUJETS_NAME_UPDATE = 'sujets.name.update',
  SUJETS_ERROR_REPORT = 'sujets.name.update',
  SUJETS_DELETE = 'sujets.delete',
  SUJETS_SCHEDULE_READ = 'sujets.schedule.read',
  SUJET_SCHEDULE_UPDATE = 'sujets.schedule.update',
  BLOCKS_READ = 'blocks.read',
  BLOCKS_SEGMENTS_READ = 'blocks.segments.read',
  BLOCKS_SEGEMTNS_ORDER_UPDATE = 'blocks.segments.order.update',
  BLOCKS_DCPS_READ = 'blocks.dcps.read',
  ADMIN = 'admin';

function sameVersionOfItem(req, res, prop) {
  return res.locals[prop].version === req.body.version;
}

function hasActivity(user, activity) {
  return _.includes(user.activities, activity);
}

exports.hasActivity = function (activity) {
  return function (req, res, next) {
    var user = getRequestUser(res);

    if (hasActivity(user, activity)) {
      return next();
    }
    return next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  };
};

exports.assertSameVersion = function (prop) {
  return function (req, res, next) {
    if (res.locals.resources[prop].item.version === req.body.version) {
      return next();
    }
    return next(ClientError({
      title: 'version error',
      statusCode: 409
    }));
  };
};

exports.createSujet = function (req, res, next) {
  var user = getRequestUser(res),
    hasSujetCreateActivity = hasActivity(user, SUJETS_CREATE);

  if (hasSujetCreateActivity) {
    return next();
  }
  return next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

exports.viewSujetItem = function(req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    isOwner = sujet.customerId === user.customer.id,
    hasCustomerReadAccess = _.includes(user.access.customers.read, sujet.customerId),
    hasSujetReadActivity = hasActivity(user, SUJETS_READ);

  if (hasActivity(user, ADMIN)) {
    return next();
  }
  if (isOwner && hasSujetReadActivity) {
    return next();
  }
  if (hasCustomerReadAccess && hasSujetReadActivity) {
    return next();
  }
  return next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

exports.reportValidationError = function(req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    sameCustomer = sujet.customerId === user.customer.id,
    hasError = sujet.sourcefileValidationError,
    isReady = sujet.intermediateReady,
    errorReported = sujet.sourcefileValidationErrorReported,
    hasSujetErrorReportActivity = hasActivity(user, SUJETS_ERROR_REPORT);

  if (!sameCustomer) {
    return next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
  if (isReady) {
    return next(ClientError({
      title: 'sujet is ready, no error to report',
      statusCode: 400
    })); 
  }
  if (!hasError) {
    return next(ClientError({
      title: 'sujet has no error',
      statusCode: 400
    })); 
  }
  if (errorReported) {
    return next(ClientError({
      title: 'sujet error already reported',
      statusCode: 400
    })); 
  }
  if (hasSujetErrorReportActivity) {
    return next();
  }
  next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};
exports.getSourcefile = function(req, res, next) {
  var user = getRequestUser(res),
    hasAdminActivity = hasActivity(user, ADMIN);

  if (!hasAdminActivity) {
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }

  if (hasAdminActivity) {
    return next();
  }
};
exports.replaceSourcefile = function(req, res, next) {
  var user = getRequestUser(res),
    hasAdminActivity = hasActivity(user, ADMIN);

  if (!hasAdminActivity) {
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }

  if (hasAdminActivity) {
    return next();
  }
};

exports.viewSujetSchedule = function(req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    sameCustomer = sujet.customerId === user.customer.id,
    hasSujetScheduleReadActivity = hasActivity(user, SUJETS_SCHEDULE_READ);

  if (sameCustomer && hasSujetScheduleReadActivity) {
    next();
  } else {   
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
};

exports.updateSujetItemName = function (req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    sameVersion = sameVersionOfItem(req, res, 'sujetItem'),
    sameCustomer = sujet.customerId === user.customer.id,
    bookedPast = sujet.bookedPast,
    bookedCurrent = sujet.bookedCurrent,
    hasSujetNameUpdateActivity = hasActivity(user, SUJETS_NAME_UPDATE);

  if (!sameVersion) {
    return next(ClientError({
      title: 'version missmatch',
      statusCode: 409
    })); 
  }
  if (!sameCustomer) {
    return next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
  if (bookedPast || bookedCurrent) {
    return next(ClientError({
      title: 'sujet booked currently or in past',
      statusCode: 400
    })); 
  }
  if (hasSujetNameUpdateActivity) {
    return next();
  }
  next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

exports.deleteSujetItem = function (req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    sameCustomer = sujet.customerId === user.customer.id,
    booked = sujet.booked,
    sameVersion = sameVersionOfItem(req, res, 'sujetItem'),
    hasSujetDeleteActivity = hasActivity(user, SUJETS_DELETE);

  if (!sameVersion) {
    return next(ClientError({
      title: 'version missmatch',
      statusCode: 409
    })); 
  }
  if (!sameCustomer) {
    return next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
  if (booked) {
    return next(ClientError({
      title: 'sujet booked',
      statusCode: 400
    })); 
  }
  if (hasSujetDeleteActivity) {
    return next();
  }
  next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

scheduleDateslots = function (req, res, next) {
  var user = getRequestUser(res),
    sujet = res.locals.sujetItem,
    stages = res.locals.models.stage.collection,
    dateslots = res.locals.dateslotCollection,
    now = res.locals.now,
    sujetSourcefileReadyOrNotGuaranteedBookingPossible,
    sameCustomerId,
    stageAccess,
    bookingDateSlotsAreNotLocked,
    hasSujetScheduleUpdateActivity = hasActivity(user, SUJET_SCHEDULE_UPDATE);

  sujetSourcefileReadyOrNotGuaranteedBookingPossible = (sujet.sourcefileIsvalid || sujet.sourcefileValidationError && sujet.sourcefileValidationErrorReported);
  sameCustomerId = sujet.customerId === user.customer.id;
  stageAccess = stages.reduce(function (result, currentStage) {
    return result && _.includes(user.access.stages.write, currentStage.stageId);
  }, true);
  bookingDateSlotsAreNotLocked = dateslots.reduce(function(result, currentDateslot) {
    return result && stages.reduce(function (result, currentStage) {
      return result && moment(now.now)
        .isSameOrBefore(moment(currentDateslot.startdate).subtract(currentStage.company_lockoffsethours.hours, 'hours'));
    }, true);
  }, true);

  if (
    sujetSourcefileReadyOrNotGuaranteedBookingPossible &&
    sameCustomerId &&
    stageAccess &&
    bookingDateSlotsAreNotLocked &&
    hasSujetScheduleUpdateActivity
  ) {
    next();
  } else {
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
};
exports.bookDateslot = scheduleDateslots;
exports.cancelDateslot = scheduleDateslots;

exports.viewBlockItem = function (req, res, next) {
  var user = getRequestUser(res),
    block = res.locals.blockItem;

  if (_.includes(user.access.stages.read, block.stageId) && hasActivity(user, BLOCKS_READ)) {
    next();
  } else {
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
};

exports.viewBlockSegmentCollection = function (req, res, next) {
  var user = getRequestUser(res),
    segmentCollection = res.locals.segmentCollection;

  if (segmentCollection.length === 0) {
    return next();
  }

  if (segmentCollection[0].sujets.length > 0) {
    if (_.includes(user.access.stages.read, segmentCollection[0].sujets[0].stageId) && hasActivity(user, BLOCKS_SEGMENTS_READ)) {
      return next();
    }
  }

  return next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

exports.renderDcp = function (req, res, next) {
  var user = getRequestUser(res),
    block = res.locals.blockItem;

  if (!hasActivity(user, ADMIN)) {
    return next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));
  }
  if (block.dcpStatus === 0) {
    return next(ClientError({
      title: 'pending render',
      statusCode: 403
    })); 
  }
  return next();
};

exports.viewAllDcps = function (req, res, next) {
  var user = getRequestUser(res);

  if (hasActivity(user, ADMIN)) {
    return next();
  }
  return next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};

exports.downloadBlock = function(req, res, next) {
  let user = getRequestUser(res),
    block = res.locals.blockItem,
    sameVersion = (block.blockhash === block.dcpBlockhash),
    dcpReady = (block.dcpStatus === 1 && block.dcpStoragekey);

  if (user) {
    let userCanDownloadBlock = hasActivity(user, BLOCKS_DCPS_READ);

    if (!userCanDownloadBlock) {
      next(ClientError({
        title: 'authorization error',
        statusCode: 403
      }));
    }
    if (!sameVersion) {
      next(ClientError({
        title: 'version missmatch',
        statusCode: 409
      }));
    }
    if (!dcpReady) {
      next(ClientError({
        title: 'dcp not ready',
        statusCode: 400
      }));
    }
    if (userCanDownloadBlock && sameVersion && dcpReady) {
      next();
    }
  } else {
    let auth = res.locals.queryTokenAuth,
      blockAccess = (auth.blockId = block.blockId);

    if (!blockAccess) {
      next(ClientError({
        title: 'authorization error',
        statusCode: 403
      }));
    }
    if (!sameVersion) {
      next(ClientError({
        title: 'version missmatch',
        statusCode: 409
      }));
    }
    if (!dcpReady) {
      next(ClientError({
        title: 'dcp not ready',
        statusCode: 400
      }));
    }
    if (blockAccess && sameVersion && dcpReady) {
      next();
    }
  }
};

exports.assertBlockHash = function (req, res, next) {
  if (res.locals.blockItem.blockhash === req.body.hash) {
    return next();
  }
  return next(ClientError({
    title: 'version error',
    statusCode: 409
  }));  
};

exports.moveSujet = function (req, res, next) {
  var user = getRequestUser(res),
    block = res.locals.blockItem,
    segment = res.locals.segmentItem,
    userCanUpdateSegments = hasActivity(user, BLOCKS_SEGEMTNS_ORDER_UPDATE),
    userHasAccessToStage = _.includes(user.access.stages.write, block.stageId),
    userHasAccessToCustomerSegment = (segment.customer.id === user.customer.id);
    // sameVersion = (block.blockhash === req.body.hash);

  // if (!sameVersion) {
  //   return next(ClientError({
  //     title: 'version error',
  //     statusCode: 409
  //   }));
  // }
  if (
    userCanUpdateSegments &&
    userHasAccessToStage &&
    userHasAccessToCustomerSegment &&
    !block.locked
  ) {
    next();
  } else {
    next(ClientError({
      title: 'authorization error',
      statusCode: 403
    }));    
  }
};

exports.adminQueue = function(req, res, next) {
  var user = getRequestUser(res);

  if (hasActivity(user, ADMIN)) {
    return next();
  }
  return next(ClientError({
    title: 'authorization error',
    statusCode: 403
  }));
};


