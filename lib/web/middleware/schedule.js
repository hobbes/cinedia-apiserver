'use strict';

var scheduleModel = require('../models/schedule'),
  scheduleResource = require('../resource/schedule'),
  getRequestCustomerId = require('./util/helper').getRequestCustomerId,
  error = require('../error'),
  ModelNotFoundError = require('../models/error').ModelNotFoundError,
  DateSlotBookError = require('../models/error').DateSlotBookError,
  ClientError = error.ClientError,
  schedule = {};

schedule.load = function(req, res, next) {
  var customerId = getRequestCustomerId(res),
    sujetId = req.params.sujetid,
    query = req.query;

  scheduleModel.get(sujetId, customerId, query.startdate, query.enddate)
    .then(function(schedule) {
      res.locals.scheduleCollection = schedule;
      next();
    })
    .catch(ModelNotFoundError, function() {
      next(ClientError({
        title: 'no schedule for sujet with id: ' + sujetId,
        statusCode: 404
      }));
    })
    .catch(next);
};

schedule.send = function(req, res) {
  var scheduleItem = scheduleResource.buildScheduleCollection(res.locals);

  res.status(200).json(scheduleItem);
};

schedule.book = function(req, res, next) {
  var sujetId = req.params.sujetid,
    dateslotIds = req.body.dateslots,
    startdate = req.query.startdate,
    enddate = req.query.enddate,
    customerId = getRequestCustomerId(res);

  scheduleModel.bookDateslot(sujetId, dateslotIds, customerId, startdate, enddate)
    .then(function(schedule) {
      res.locals.scheduleCollection = schedule;
      next();
    })
    .catch(DateSlotBookError, (err) => {
      return next(ClientError({
        title: err.message,
        statusCode: 409
      }));
    })
    .catch(next);
};

schedule.cancel = function(req, res, next) {
  var sujetId = req.params.sujetid,
    dateslotIds = req.body.dateslots,
    startdate = req.query.startdate,
    enddate = req.query.enddate,
    customerId = getRequestCustomerId(res);

  scheduleModel.cancelDateslot(sujetId, dateslotIds, customerId, startdate, enddate)
    .then(function(schedule) {
      res.locals.scheduleCollection = schedule;
      next();
    })
    .catch(DateSlotBookError, (err) => {
      return next(ClientError({
        title: err.message,
        statusCode: 409
      }));
    })
    .catch(next);
};

module.exports = schedule;