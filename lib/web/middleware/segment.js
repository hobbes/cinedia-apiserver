'use strict';

var log = require('../../shared/logger'),
  segmentModel = require('../models/segment'),
  segmentResource = require('../resource/segment'),
  segment = {};

segment.loadMany = function (req, res, next) {
  var customerIdsReadAccess = res.locals.userItem.access.customers.read,
    blockId = req.params.blockid;
  
  segmentModel.getAllBlockSegments(blockId, customerIdsReadAccess)
    .then(function (segments) {
      log.debug('model segmentCollection: ', segments);
      res.locals.segmentCollection = segments;
      next();
    })
    .catch(next); 
};

segment.loadOne = function (req, res, next) {
  var blockId = req.params.blockid,
    segmentId = req.params.segmentid;
  
  segmentModel.getOneBlockSegment(blockId, segmentId)
    .then(function (segment) {
      log.debug('model segmentItem: ', segment);
      res.locals.segmentItem = segment;
      next();
    })
    .catch(next); 
};

segment.sendMany = function (req, res) {
  var segmentCollection = segmentResource.buildSegmentCollection(res.locals);
  log.debug('resource segmentCollection: ', segmentCollection);
  res.status(200).json(segmentCollection);
};

segment.updateSegmentOrder = function (req, res, next) {
  var customerIdsReadAccess = res.locals.userItem.access.customers.read,
    blockId = req.params.blockid,
    segmentId = req.params.segmentid,
    list = req.body.list;

  segmentModel.updateSegmentOrder(blockId, segmentId, list, customerIdsReadAccess)
    .then(function (segments) {
      res.locals.segmentCollection = segments;
      next();
    })
    .catch(next);
};

module.exports = segment;