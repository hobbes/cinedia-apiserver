'use strict';

var createGenericMiddleware = require('./generic'),
  model = require('../models/locations'),
  resource = require('../resource/locations'),
  middleware = createGenericMiddleware({
    name: 'location',
    resource,
    model
  });

module.exports = middleware;