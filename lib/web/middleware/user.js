'use strict';

var createGenericMiddleware = require('./generic'),
  config = require('config'),
  env = config.get('env'),
  name = config.get('name'),
  logger = require('../../shared/logger'),
  mailer = require('../mailer'),
  model = require('../models/user'),
  modelActivities = require('../models/activities'),
  resource = require('../resource/user'),
  resourceActivities = require('../resource/activities'),
  middleware = createGenericMiddleware({
    name: 'user',
    resource,
    model
  });
  
function hasAdminActivity(req, res, next) {
  if (middleware._userHasActivity(res, 'admin')) {
    return next();
  }
  return middleware._sendAuthError(next);
}

middleware.canGetUser = function(req, res, next) {
  if (middleware._userHasActivity(res, 'admin')) {
    return next();
  }
  if (middleware._getRequestUser(res).id === req.params[middleware._paramIdName]) {
    return next();
  }
  return middleware._sendAuthError(next);
};

middleware.canUpdateUser = function(req, res, next) {
  if (middleware._userHasActivity(res, 'admin')) {
    return next();
  }
  if (middleware._getRequestUser(res).id === req.params[middleware._paramIdName]
      && middleware._jsonPatchOnlyHasAllowedProperties(req.body, ['/getNotifications'])) {
    console.log('ownUser:', req.params[middleware._paramIdName]) 
    return next();
  }
  return middleware._sendAuthError(next);
};

middleware.canEditUserActivities = hasAdminActivity;
middleware.canFindUserActivities = hasAdminActivity;
middleware.canSendUserWelcomeMail = hasAdminActivity;

middleware.sendUserWelcomeMail = function (req, res, next) {
  let email = res.locals.models.user.item.email,
    apphost = config.get('apphost'),
    text = 
`Willkommen bei ${name}

Passwort festlegen: ${apphost}/login?redirect=/home&email=${email}#passwordreset

oder

einfach mit einer E-Mail einloggen: ${apphost}/login?redirect=/home&email=${email}#passwordless

Bei Fragen einfach auf diese E-Mail antworten.

freundliche Grüsse

${name}`;

  if (env === 'development' || env === 'test') {
    return logger.info('email: ' + text);
  }

  mailer.sendMail(email, text)
    .then(() => {
      next();
    });
};

middleware.findUserActivities = function(req, res, next) {
  let userId = req.params[middleware._paramIdName];

  modelActivities.findUserActivities(userId)
    .then(function(activitiesCollection){
      middleware._helper.setLocals(res, 'activities', 'collection', {
        model: activitiesCollection,
        resource: resourceActivities.buildCollection(activitiesCollection)
      });
      next();
    }).catch(next);
};

middleware.addUserActivity = function(req, res, next) {
  let userId = req.params[middleware._paramIdName],
    activityId = req.params.activityid;

  modelActivities.addUserActivity(userId, activityId)
    .then(function(activitiesCollection){
      middleware._helper.setLocals(res, 'activities', 'collection', {
        model: activitiesCollection,
        resource: resourceActivities.buildCollection(activitiesCollection)
      });
      next();
    }).catch(next);
};

middleware.deleteUserActivity = function(req, res, next) {
  let userId = req.params[middleware._paramIdName],
    activityId = req.params.activityid;

  modelActivities.deleteUserActivity(userId, activityId)
    .then(function(activitiesCollection){
      middleware._helper.setLocals(res, 'activities', 'collection', {
        model: activitiesCollection,
        resource: resourceActivities.buildCollection(activitiesCollection)
      });
      next();
    }).catch(next);
};

module.exports = middleware;