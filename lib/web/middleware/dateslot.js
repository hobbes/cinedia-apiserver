'use strict';

var lodash = require('lodash'),
  dateslotModel = require('../models/dateslot'),
  dateslotResource = require('../resource/dateslot'),
  error = require('../error'),
  ClientError = error.ClientError,
  dateslot = {};

dateslot.loadNow = function (req, res, next) {
  dateslotModel.now()
    .then(function (now) {
      res.locals.now = now;
      next();
    })
    .catch(next);
};

dateslot.loadOne = function (req, res, next, dateslotId) {
  dateslotModel.get(dateslotId)
    .then(function (dateslot) {
      if (!dateslot) {
        return next(ClientError({
          title: 'no dateslot with id: ' + dateslotId,
          statusCode: 404
        }));
      }
      res.locals.dateslotItem = dateslot;
      next();
    })
    .catch(next);
};

dateslot.loadMultiple = function (req, res, next) {
  var dateslots = Object.keys(req.body.dateslots).reduce((result, stageId) => {
    return lodash.union(result, req.body.dateslots[stageId]);
  }, []);

  dateslotModel.findMultiple(dateslots)
    .then(function (dateslots) {
      res.locals.dateslotCollection = dateslots;
      next();
    })
    .catch(next);
};

dateslot.loadDaterange = function (req, res, next) {
  var query = req.query;

  dateslotModel.find(query.startdate, query.enddate)
    .then(function (dateslots) {
      res.locals.dateslotCollection = dateslots;
      next();
    })
    .catch(next);
};

dateslot.sendDaterange = function(req, res) {
  res.status(200).json(dateslotResource.buildDateslotCollection(res.locals));
};

module.exports = dateslot;