'use strict';

var _ = require('lodash'), 
  rfc6902 = require('rfc6902'),
  config = require('config'),
  helper = require('./util/helper'),
  setLocals = helper.setLocals,
  error = require('../error'),
  ModelNotFoundError = require('../models/error').ModelNotFoundError,
  ClientError = error.ClientError,
  defaultLimit = config.get('collection.limit');

module.exports = function ({
  name,
  resource,
  model
}) {
  const paramIdName = `${name}id`;

  function setLocalsItem(req, res, next) {
    return function(modelItem) {
      setLocals(res, name, 'item', {
        model: modelItem,
        resource: resource.buildItem(modelItem)
      });
      next();
    };
  }

  function setLocalsCollection(req, res, next) {
    return function(modelCollection) {
      setLocals(res, name, 'collection', {
        model: modelCollection,
        resource: resource.buildCollection(modelCollection)
      });
      next();
    };
  }

  return {
    _paramIdName: paramIdName,
    _helper: helper,
    _setLocalsItem: setLocalsItem,
    _setLocalsCollection: setLocalsCollection,
    _userHasActivity(res, activity) {
      const user = helper.getRequestUser(res);
      return _.includes(user.activities, activity);
    },
    _jsonPatchOnlyHasAllowedProperties(body, props) {
      return body.reduce((acc, item) => {
        return acc && props.indexOf(item.path) !== -1;
      }, true);
    },
    _getRequestUser: helper.getRequestUser,
    _sendAuthError(next) {
      next(ClientError({
        title: 'authorization error',
        statusCode: 403
      }));
    },
    create(req, res, next) {
      const data = resource.destructItemCreate(req.body);

      model.create(data)
        .then(setLocalsItem(req, res, next))
        .catch(next);
    },
    find(req, res, next) {
      if (!req.query.limit) {
        req.query.limit = defaultLimit;
      }
      model.find(req.query)
        .then(setLocalsCollection(req, res, next))
        .catch(next); 
    },
    get(req, res, next, genericId) {
      model.get(genericId)
        .then(setLocalsItem(req, res, next))
        .catch(ModelNotFoundError, function() {
          next(ClientError({
            title: `no ${name} with id: ${genericId}`,
            statusCode: 404
          }));
        })
        .catch(next);
    },
    update(req, res, next) {
      let genericId = req.params[paramIdName],
        patch = req.body,
        generic = res.locals.resources[name].item,
        data;

      rfc6902.applyPatch(generic, patch);
      data = resource.destructItemUpdate(generic);

      Object.keys(data).forEach((prop) => {
        if (typeof data[prop] === 'string') {
          data[prop] = data[prop].trim();
        }
      });

      model.update(genericId, data)
        .then(setLocalsItem(req, res, next))
        .catch(next);
    },
    delete(req, res, next) {
      const genericId = req.params[paramIdName];

      model.delete(genericId)
        .then(() => {
          res.status(204).end();
        })
        .catch(ModelNotFoundError, function() {
          next(ClientError({
            title: `${name} with id ${genericId} not deleted`,
            statusCode: 404
          }));
        })
        .catch(next);
    }
  };
};