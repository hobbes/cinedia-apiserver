'use strict';

var ClientError = require('../error').ClientError,
  ready = ('true' == require('config').get('ready'));

exports.check = function (req, res, next) {
  if (ready) {
    return next();
  }
  return next(ClientError({
    title: 'maintenance',
    statusCode: 503
  }));
};