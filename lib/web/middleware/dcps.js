'use strict';

var blockModel = require('../models/block'),
  blockResource = require('../resource/block'),
  error = require('../error'),
  ClientError = error.ClientError,
  dcp = {};

dcp.loadAll = function(req, res, next) {
  var date = req.query.date;

  blockModel.findAll(date)
    .then(function(dcps){
      res.locals.blockCollection = dcps;
      next();
    })
    .catch(next); 
};
dcp.sendAll = function (req, res) {
  var blockCollection = blockResource.buildBlockCollection(res.locals);

  res.status(200).json(blockCollection);
};

module.exports = dcp;