'use strict';

var jwt = require('../jwt'),
  jwtVerify = jwt.verify,
  jwtSign = jwt.sign,
  moment = require('moment'),
  mailer = require('../mailer'),
  log = require('../../shared/logger'),
  error = require('../error'),
  ClientError = error.ClientError,
  ModelNotFoundError = require('../models/error').ModelNotFoundError,
  authentication = require('../models/authentication'),
  credential = require('credential'),
  config = require('config'),
  cookieExpiration = config.get('cookieExpiration'),
  apphost = config.get('apphost'),
  env = config.get('env'),
  pw = credential();

function throwPwHashNotSetClientError(pwhash) {
  if (pwhash.length === 0) {
    throw ClientError({
      title: 'passwordNotSet',
      statusCode: 401
    });
  }
  return pwhash;
}

function throwUserNotFoundClientError() {
  throw ClientError({
    title: 'userNotFound',
    statusCode: 404
  });
}

function searchUser(email) {
  return authentication.findUserByEmail(email)
    .catch(ModelNotFoundError, throwUserNotFoundClientError);
}

function jwtErrors(next) {
  return function jwtErrors(error) {
    if (error.name === 'TokenExpiredError') {
      next(ClientError({
        title: 'tokenExpired',
        statusCode: 400
      }));
    }
    if (error.name === 'JsonWebTokenError') {
      next(ClientError({
        title: 'tokenInvalid',
        statusCode: 400
      }));
    }
    throw error;
  };
}

function setCookie(res, longLivedToken) {
  res.cookie('token', longLivedToken, {
    secure: (env === 'production' || env === 'stage') ? true : false,
    expires: moment().add(cookieExpiration, 'hours').toDate(),
    httpOnly: true
  });
}

function sendUser(res) {
  return function (user) {
    return jwtSign(user)
      .then((token) => {
        setCookie(res, token);
        res.json(user);
      });
  };
}

exports.logout = function (req, res) {
  res.clearCookie('token');
  res.status(204).end();
};

exports.check = function (req, res) {
  res.status(200).json(res.locals.userItem);
};

exports.pwdLogin = function (req, res, next) {
  var email = req.body.email,
    password = req.body.password;

  authentication.getPwdHash(email)
    .catch(ModelNotFoundError, throwUserNotFoundClientError)
    .then(throwPwHashNotSetClientError)
    .then(function (pwhash) {
      return pw.verify(pwhash, password);
    })
    .then(function (isValid) {
      if (isValid) {
        return searchUser(email);
      } else {
        throw ClientError({
          title: 'passwordInvalid',
          statusCode: 400
        });
      }
    })
    .then(sendUser(res))
    .catch(next);
};

exports.pwdreset = function (req, res, next) {
  var email = req.body.email,
    redirect = req.body.redirect;

  authentication.findUserByEmail(email)
    .then(function (user) {
      return jwtSign({
        email: user.email
      }, {
        expiresIn: '15m'
      }).then(function (token) {
        var link = `${apphost}/login?email=${email}&token=${token}&redirect=${redirect}#passwordresetreturn`;

        if (env === 'development' || env === 'test') {
          return log.info('link: ' + link);
        } else {
          return mailer.sendNewPwMail(user.email, link);
        }
      });
    })
    .then(function () {
      res.status(204).end();
    })
    .catch(ModelNotFoundError, throwUserNotFoundClientError)
    .catch(next);
};

exports.pwdresetreturn = function (req, res, next) {
  var password = req.body.password,
    password2x = req.body.password2x,
    token = req.body.token;

  return jwtVerify(token)
    .then(function (shortLivedTokenData) {
      if (password !== password2x) {
        throw ClientError({
          title: 'passwordDoNotMatch',
          statusCode: 400
        });
      } else {
        return shortLivedTokenData;
      }
    }).then(function(shortLivedTokenData) {
      return pw.hash(password)
        .then(function (hash) {
          return authentication.setPwd(shortLivedTokenData.email, hash);
        })
        .then(function(){
          return searchUser(shortLivedTokenData.email);
        });
    })
    .then(sendUser(res))
    .catch(jwtErrors(next))
    .catch(next);
};

exports.nopwdLogin = function (req, res, next) {
  var email = req.body.email,
    redirect = req.body.redirect;

  searchUser(email)
    .then(function (user) {
      return jwtSign({
        email: user.email
      }, {
        expiresIn: '15m'
      }).then(function (token) {
        var link = `${apphost}/login?email=${email}&token=${token}&redirect=${redirect}#passwordlessreturn`;

        if (env === 'development' || env === 'test') {
          return log.info('link: ' + link);
        }

        return mailer.sendLoginMail(user.email, link);
      });
    })
    .then(function () {
      res.status(204).end();
    })
    .catch(next);
};
exports.nopwdLoginReturn = function (req, res, next) {
  var token = req.body.token;

  return jwtVerify(token).then(function (shortLivedToken) {
    return searchUser(shortLivedToken.email);
  }).then(sendUser(res))
    .catch(jwtErrors(next))
    .catch(next);
};

exports.limitToUserAssignedToCustomer = function (req, res, next) {
  if (res.locals.userItem.customer.id) {
    return next();
  }
  return next(ClientError({
    title: 'userIsNotAssignedToCustomer',
    statusCode: 400
  }));
};

function restrictWithQueryToken(req, res, next) {
  var queryToken = req.query.token;

  return jwtVerify(queryToken).then(function(tokenAuth){
    res.locals.queryTokenAuth = tokenAuth;
    next();
  }).catch(function(err) {
    if (err.name === 'TokenExpiredError') {
      res.clearCookie('token');
      throw ClientError({
        title: 'tokenExpired',
        statusCode: 401
      });
    }
    if (err.name === 'JsonWebTokenError') {
      res.clearCookie('token');
      throw ClientError({
        title: 'tokenInvalid',
        statusCode: 401
      });
    }
    throw err;
  }).catch(next);
}

function restrictWithCookieToken(req, res, next) {
  var cookieToken = req.cookies.token;

  if (!cookieToken) {
    return next(ClientError({
      title: 'tokenMissing',
      statusCode: 401
    }));
  }

  return jwtVerify(cookieToken).then(function(user){
    res.locals.userItem = user;
    next();
  }).catch(function(err) {
    if (err.name === 'TokenExpiredError') {
      res.clearCookie('token');
      throw ClientError({
        title: 'tokenExpired',
        statusCode: 401
      });
    }
    if (err.name === 'JsonWebTokenError') {
      res.clearCookie('token');
      throw ClientError({
        title: 'tokenInvalid',
        statusCode: 401
      });
    }
    throw err;
  }).catch(next);
}

exports.restrictWithPossibleQueryToken = function(req, res, next) {
  var queryToken = req.query.token;

  if (queryToken) {
    restrictWithQueryToken(req, res, next);
  } else {
    restrictWithCookieToken(req, res, next);
  }
};

exports.restrict = function (req, res, next) {
  restrictWithCookieToken(req, res, next);
};