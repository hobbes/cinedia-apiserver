var schemas = require('../schemas');

function errorResponse(schemaErrors) {
  var errors = schemaErrors.map(function(error) {
    return {
      path: error.dataPath,
      message: error.message
    };
  });
  return {
    status: 'failed',
    errors: errors
  };
}

exports.schema = function(schemaName, param) {
  return function(req, res, next) {
    var valid = schemas.validate(schemaName, req[param || 'body']);
    if (!valid) {
      return res.status(400).json(errorResponse(schemas.errors));
    } else {
      next();
    }
  };
};