'use strict';

var Promise = require('bluebird'),
  objectstorageConfig = require('config').get('objectstorage'),
  knox = require('knox'),
  ClientError = require('./error').ClientError,
  client = knox.createClient({
    key: objectstorageConfig.key,
    secret: objectstorageConfig.secret,
    bucket: objectstorageConfig.bucketname,
    endpoint: objectstorageConfig.url
  });

exports.listAllStorageItems = function () {
  function listBucket(marker) { 
    return new Promise(function (resolve, reject) {
      var opt = {};

      if (marker) {
        opt.marker = marker;
      }
      client.list(opt, function(err, data){
        if (err) {
          return reject(err);
        }
        if (!data) {
          return reject(new Error('no data'));
        }
        resolve(data);
      });
    });
  }

  function getPartialList(marker) {
    return listBucket(marker)
      .then((partialList) => {
        const sosItemList = partialList.Contents;

        if (partialList.IsTruncated) {
          return getPartialList(partialList.NextMarker)
            .then((nextPartialListSosItems) => {
              return sosItemList.concat(nextPartialListSosItems);
            });
        } else {
          return sosItemList;
        }
      });
  }

  return getPartialList();
};

exports.getSignedGETUrl = function (key, time, filename) {
  const options = {
    verb: 'GET'
  };

  if (filename) {
    options.extraHeaders = {
      'response-content-disposition': filename
    };
  }
  return client.signedUrl(key, time, options);
};

exports.getSignedPUTUrl = function (key, contentType, time) {
  return client.signedUrl(key, time, {
    verb: 'PUT',
    contentType: contentType
  });
};

exports.getReadStream = function(key, headers = {}) {
  return new Promise(function (resolve, reject) {
    let sosReq = client.getFile(key, headers, function(err, res) {
      if (err) {
        return reject(err);
      }
      if (res.statusCode === 404) {
        return reject(ClientError({
          title: 'no preview found',
          statusCode: 404
        }));
      }
      if (res.statusCode !== 200) {
        return reject(new Error(res.statusCode));
      }
      return resolve({
        sosReq,
        sosRes: res
      });
    });
  });
};

exports.deleteFiles = function(fileKeyArray) {
  return new Promise(function(resolve, reject) {
    client.deleteMultiple(fileKeyArray, function(err, res) {
      if (err) {
        return reject(err);
      }
      if (res.statusCode !== 200) {
        return reject(new Error(res.statusCode));
      }
      return resolve();
    });
  });
};