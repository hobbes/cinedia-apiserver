'use strict';

var Promise = require('bluebird'),
  moment = require('moment'),
  config = require('config'),
  taskresult = require('./models/taskresult'),
  mailer = require('./mailer'),
  log = require('../shared/logger'),
  resultHandlers = {},
  requests = {},
  additionalResultActions = {},
  sujetIntermediateSuccess,
  sujetIntermediateError,
  mailhost = config.get('mailservice.host');

const DIA_VALIDATE_TASKNAME = config.get('queue.jobnames.dia.validate'),
  DIA_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.dia.intermediate'),

  SPOTMUTE_VALIDATE_TASKNAME = config.get('queue.jobnames.spotmute.validate'),
  SPOTMUTE_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotmute.intermediate'),

  SPOTSOUND_VALIDATE_TASKNAME = config.get('queue.jobnames.spotsound.validate'),
  SPOTSOUND_INTERMEDIATE_TASKNAME = config.get('queue.jobnames.spotsound.intermediate'),

  BLOCK_DCP_TASKNAME = config.get('queue.jobnames.block.dcp.render');

sujetIntermediateSuccess = function (result) {
  return Promise.map(result.files, function(file) {
    if (file.type === 'intermediate') {
      return taskresult.addIntermediate(result.sujetId, {
        key: file.key,
        duration: result.metadata.duration
      });
    }
    if (file.type === 'preview') {
      return taskresult.addPreview(result.sujetId, {
        previewType: file.previewType,
        previewFormat: file.previewFormat,
        size: file.size,
        key: file.key,
        etag: file.etag
      });
    }
  });
};

sujetIntermediateError = function(result) {
  return taskresult.addIntermediateError(result.sujetId, result.error.type)
    .then(() => {
      let email = {
        From: `bot@${mailhost}`,
        To: `support@${mailhost}`,
        Subject: 'Intermediate Error',
        TextBody: `Intermediate Error, Sujet ID: ${result.sujetId}`
      };

      return mailer.sendGenericEmail(email);
    });
};

requests[DIA_VALIDATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: DIA_VALIDATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source,
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[DIA_VALIDATE_TASKNAME] = {
  success: function (result) {
    return taskresult.addValidationSuccess(result.sujetId);
  },
  error: function(result) {
    return taskresult.addValidationError(result.sujetId, result.error.type);
  }
};

requests[DIA_INTERMEDIATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: DIA_INTERMEDIATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source,
    duration: data.duration
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[DIA_INTERMEDIATE_TASKNAME] = {
  success: sujetIntermediateSuccess,
  error: sujetIntermediateError
};



requests[SPOTMUTE_VALIDATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: SPOTMUTE_VALIDATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source,
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[SPOTMUTE_VALIDATE_TASKNAME] = {
  success: function (result) {
    return taskresult.addValidationSuccess(result.sujetId);
  },
  error: function(result) {
    return taskresult.addValidationError(result.sujetId, result.error.type);
  }
};

requests[SPOTMUTE_INTERMEDIATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: SPOTMUTE_INTERMEDIATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[SPOTMUTE_INTERMEDIATE_TASKNAME] = {
  success: sujetIntermediateSuccess,
  error: sujetIntermediateError
};


requests[SPOTSOUND_VALIDATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: SPOTSOUND_VALIDATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source,
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[SPOTSOUND_VALIDATE_TASKNAME] = {
  success: function (result) {
    return taskresult.addValidationSuccess(result.sujetId);
  },
  error: function(result) {
    return taskresult.addValidationError(result.sujetId, result.error.type);
  }
};

requests[SPOTSOUND_INTERMEDIATE_TASKNAME] = function(data, additionalResultAction) {
  const task = {
    _name: SPOTSOUND_INTERMEDIATE_TASKNAME,
    sujetId: data.sujetId,
    source: data.source
  };

  if (additionalResultAction) {
    task._additionalResultAction = additionalResultAction;
  }
  log.debug(task);
  return task;
};
resultHandlers[SPOTSOUND_INTERMEDIATE_TASKNAME] = {
  success: sujetIntermediateSuccess,
  error: sujetIntermediateError
};


requests[BLOCK_DCP_TASKNAME] = function(data, additionalResultAction) {
  data._name = BLOCK_DCP_TASKNAME;

  if (additionalResultAction) {
    data._additionalResultAction = additionalResultAction;
  }

  return data;
};

resultHandlers[BLOCK_DCP_TASKNAME] = {
  success: function(result) {
    var blockId = result.blockId,
      blockhash = result.blockhash,
      dcpstoragekey;

    if (result.key) {
      dcpstoragekey = result.key;
    }
    if (result.Key) {
      dcpstoragekey = result.Key;
    }

    return taskresult.completeDcp(blockId, {
      key: dcpstoragekey,
      etag: result.etag,
      hash: blockhash
    }).then(function () {
      return taskresult.getNotificationEmails(blockId);
    }).then(function (emails) {
      let enddate = moment(result.blockinfo.enddate),
        enddateHumanreadable = enddate.format('DD.MM.YYYY'),
        blockUrl = result.blockinfo.blockUrl,
        downLoadUrl = result.blockinfo.downLoadUrl,
        kw = result.blockinfo.kw,
        stagename = result.blockinfo.stagename,
        title = `DCP bereit: KW${kw} ${stagename}`,
        message = `Das DCP der KW${kw} für den Saal ${stagename} ist bereit.

Klicken Sie auf den folgenden Link um das DCP im Webbrowser herunter zu laden:
Der Download ist möglich bis am: ${enddateHumanreadable}
${downLoadUrl}

Um den Block anzuschauen klicken Sie hier:
${blockUrl}`;

      return mailer.sendEmailBatch(emails,
        `bot@${mailhost}`, title, message);
    });
  },
  error: function(result) {
    var blockId = result.blockId;

    return taskresult.completeDcp(blockId)
      .then(() => {
        let email = {
          From: `bot@${mailhost}`,
          To: `support@${mailhost}`,
          Subject: 'Block Error',
          TextBody: `Block Error, Block ID: ${result.blockId}\n
URL: ${result.blockinfo.blockUrl}\n
KW: ${result.blockinfo.kw}\n
Company: ${result.blockinfo.stagename}\n
Stage: ${result.blockinfo.companyName}\n
`
        };

        return mailer.sendGenericEmail(email);
      });
  }
};

additionalResultActions.logResult = function(data) {
  log.info(data);
};

exports.resultHandlers = resultHandlers;
exports.requests = requests;
exports.additionalResultActions = additionalResultActions;