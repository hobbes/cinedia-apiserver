'use strict';

var Ajv = require('ajv'),
  jsYaml = require('js-yaml'),
  ajv = Ajv({
    allErrors: true,
    removeAdditional: 'all',
    coerceTypes: true,
    useDefaults: true
  }),
  fs = require('fs'),
  path = require('path'),
  schemaFolder = path.join(__dirname, '../../schema');

ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));

fs.readdirSync(schemaFolder).forEach(function (file) {
  var extname = path.extname(file),
    schemaFilename = path.basename(file, '.yaml');

  if (extname === '.yaml') {
    ajv.addSchema(jsYaml.load(fs.readFileSync(path.join(schemaFolder, file))), schemaFilename);
  }
});


module.exports = ajv;