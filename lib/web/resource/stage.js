'use strict';

const createResource = require('./generic'),
  paths = [
    '/name/full',
    '/name/short',
    '/location/id'
  ];

function buildItem(model) {
  return {
    id: model.stageId,
    name: {
      full: model.stageName,
      short: model.stageNameShort
    },
    location: {
      id: model.locationId
    },
    created: model.createdAt,
    version: model.version
  };
}

function destructItemCreate(resource) {
  return {
    stageName: resource.name.full,
    stageNameShort: resource.name.short,
    locationId: resource.location.id
  };
}
function destructItemUpdate(resource) {
  return {
    stageName: resource.name.full,
    stageNameShort: resource.name.short
  };
}

const resource = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate
});

resource.buildCollectionCustomerStages = function (modelCollection) {
  const result = {
    list: [],
    entities: {}
  };
  modelCollection.map((model) => {
    return {
      id: model.stageId,
      name: model.stageName,
      nameshort: model.stageNameShort,
      lockoffset: model.lockoffsethours,
      location: {
        id: model.locationId,
        name: model.locationName
      },
      company: {
        id: model.companyId,
        name: model.companyName,
        nameshort: model.companyNameShort
      },
      created: model.createdAt,
      version: model.version
    };
  }).forEach((resource) => {
    result.list.push(resource.id);
    result.entities[resource.id] = resource;
  });

  return result;
};

module.exports = resource;