'use strict';

const createResource = require('./generic'),
  paths = [
  '/name/full',
  '/name/short',
  '/type'
];

function buildItem(model) {
  return {
    id: model.customerId,
    name: {
      full: model.name,
      short: model.nameShort
    },
    type: model.type,
    created: model.createdAt,
    version: model.version
  };
}

function destructItemCreate(resource) {
  return {
    name: resource.name.full,
    nameShort: resource.name.short,
    type: resource.type
  };
}

const resource = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate: destructItemCreate
});

module.exports = resource;