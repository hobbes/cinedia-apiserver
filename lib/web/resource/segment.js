var apiroot = require('config').get('host');

function buildSegmentSujetResource(model) {
  var sujetItem = {
    id: model.sujetId,
    customerId: model.customerId,
    name: model.sujetName,
    type: model.sujetType,
    duration: model.sujetDuration,
    bookingGuaranteed: model.bookingGuaranteed,
    previews: {
      thumbnail: {
        href: `${apiroot}/sujets/${model.sujetId}/previews/thumbnail`
      },
      still: {
        small: {
          href: `${apiroot}/sujets/${model.sujetId}/previews/still?size=270`
        },
        medium: {
          href: `${apiroot}/sujets/${model.sujetId}/previews/still?size=540`
        },
        large: {
          href: `${apiroot}/sujets/${model.sujetId}/previews/still?size=1080`
        }
      }
    }
  };

  return sujetItem;
}

exports.buildSegmentCollection = function(models) {
  var result = {
    list: [],
    entities: {}
  };

  models.segmentCollection.map(function (segment) {
    segment.sujets = segment.sujets.map(buildSegmentSujetResource);
    return segment;
  }).forEach(function(item) {
    result.list.push(item.id);
    result.entities[item.id] = item;
  });

  return result;
};