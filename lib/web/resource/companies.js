'use strict';

const createResource = require('./generic'),
  paths = [
    '/name/full',
    '/name/short',
    'fps',
    'lockoffset'
  ];

function buildItem(model) {
  return {
    id: model.companyId,
    name: {
      full: model.companyName,
      short: model.companyNameShort
    },
    fps: model.fps,
    lockoffset: {
      hours: model.lockoffsethours.hours || 0
    },
    created: model.createdAt,
    version: model.version
  };
}

function destructItemCreate(resource) {
  return {
    companyName: resource.name.full,
    companyNameShort: resource.name.short,
    fps: resource.fps,
    lockoffsethours: resource.lockoffset
  };
}

function destructItemUpdate(resource) {
  return {
    companyName: resource.name.full,
    companyNameShort: resource.name.short,
    fps: resource.fps,
    lockoffsethours: resource.lockoffset  };
}

const resource = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate
});

module.exports = resource;