'use strict';

const createResource = require('./generic');

const paths = [];

function buildItem(model) {
  return {
    id: model.activityId,
    name: model.name
  };
}

function destructItemCreate(resource) {
  return {};
}

module.exports = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate: destructItemCreate
});