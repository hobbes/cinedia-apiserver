'use strict';

const createResource = require('./generic'),
  paths = [
    '/name/full',
    '/company/id'
  ];

function buildItem(model) {
  return {
    id: model.locationId,
    name: {
      full: model.locationName
    },
    company: {
      id: model.companyId
    },
    created: model.createdAt,
    version: model.version
  };
}

function destructItemCreate(resource) {
  return {
    locationName: resource.name.full,
    companyId: resource.company.id
  };
}
function destructItemUpdate(resource) {
  return {
    locationName: resource.name.full
  };
}

const resource = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate
});

module.exports = resource;