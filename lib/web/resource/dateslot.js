'use strict';

function buildDateslotResource(model) {
  return {
    id: model.dateslotId,
    kw: model.kw,
    startdate: model.startdate,
    enddate: model.enddate
  };
}

exports.buildDateslotCollection = function (models) {
  var result = {
    list: [],
    entities: {}
  };

  models.dateslotCollection.map(buildDateslotResource)
    .forEach(function(dateslotResource){
      result.list.push(dateslotResource.id);
      result.entities[dateslotResource.id] = dateslotResource;
    });

  return result;
};

exports.buildDateslotItem = function (models) {
  var dateslotItem = models.dateslotItem;

  return buildDateslotResource(dateslotItem);
};