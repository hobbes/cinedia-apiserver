'use strict';

exports.buildScheduleCollection = function(models) {
  var result = {
    list: [],
    entities: {}
  };

  function buildScheduleResource(model) {
    if (!result.entities[model.stageId]) {
      result.entities[model.stageId] = {
        id: model.stageId,
        name: model.stageName,
        slots: {}
      };
      result.list.push(model.stageId);
      result.entities[model.stageId].slots[model.dateslotId] = {
        booked: true
      };
    } else {
      result.entities[model.stageId].slots[model.dateslotId] = {
        booked: true
      };
    }
  }

  models.scheduleCollection.forEach(buildScheduleResource);

  return result;
};