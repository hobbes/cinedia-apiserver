'use strict';

var apiroot = require('config').get('host'),
  log = require('../../shared/logger');

function buildSujetItem(model) {
  var sujetHref = `${apiroot}/sujets/${model.sujetId}`,
    sujet = {
      id: model.sujetId,
      customerId: model.customerId,
      created: model.createdAt,
      duration: model.duration,
      version: model.version,
      booked: model.booked,
      bookedPast: model.bookedPast,
      bookedCurrent: model.bookedCurrent,
      bookedFuture: model.bookedFuture,
      name: model.name,
      type: model.type,
      previews: {
        thumbnail: {
          href: `${sujetHref}/previews/thumbnail`
        },
        still: {
          small: {
            href: `${sujetHref}/previews/still?size=270`
          },
          medium: {
            href: `${sujetHref}/previews/still?size=540`
          },
          large: {
            href: `${sujetHref}/previews/still?size=1080`
          }
        }
      },
      sourcefile: {
        validated: model.sourcefileValidated,
        isValid: model.sourcefileIsvalid,
        validation: {        
          error: model.sourcefileValidationError,
          errorReported: model.sourcefileValidationErrorReported,
          job: {
            id: model.sourcefileValidationJobId,
            progress: model.sourcefileValidationJobProgress
          }
        },
        href: `${sujetHref}/sourcefile`
      },
      intermediate: {
        ready: model.intermediateReady,
        error: model.intermediateError,
        job: {
          id: model.intermediateJobId,
          progress: model.intermediateJobProgress
        }
      },
      errormessage: model.error ? model.errormessage : ''
    };

  if (sujet.type !== 'spot_sound' || sujet.type !== 'spot_mute') {
    sujet.previews.video = {
      medium: {
        href: `${sujetHref}/previews/video?size=540`
      },
      large: {
        href: `${sujetHref}/previews/video?size=1080`
      }
    };
    sujet.previews.sequence = {
      href: `${sujetHref}/previews/sequence`
    };
  }
  log.debug(`sujetresource: ${sujet}`);
  return sujet;
}

exports.buildSujetCollection = function(models) {
  var result = {
    list: [],
    entities: {}
  };

  models.sujetCollection.map(buildSujetItem)
    .forEach(function(item) {
      result.list.push(item.id);
      result.entities[item.id] = item;
    });

  return result;
};

exports.buildSujetItem = function(models) {
  return buildSujetItem(models.sujetItem);
};
