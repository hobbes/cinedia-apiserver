'use strict';

var apiroot = require('config').get('host');

function buildBlockResource(blockModel) {
  var block = {
      id: blockModel.blockId,
      name: blockModel.name,
      hash: blockModel.blockhash,
      locked: blockModel.locked,
      lockDate: blockModel.lockDate,
      current: blockModel.current,
      outdated: blockModel.outdated,
      stage: {
        id: blockModel.stageId,
        name: blockModel.stageName,
        nameshort: blockModel.stageNameShort
      },
      location: {
        id: blockModel.locationId,
        name: blockModel.locationName
      },
      company: {
        id: blockModel.companyId,
        name: blockModel.companyName,
        nameshort: blockModel.companyNameShort
      },
      dateslot: {
        id: blockModel.dateslotId,
        kw: blockModel.kw,
        startdate: blockModel.startdate,
        enddate: blockModel.enddate
      }
    },
    dcp = {
      version: blockModel.dcpVersion,
      hash: blockModel.dcpBlockhash,
      href: `${apiroot}/blocks/${block.id}/dcp`,
      pending: false,
      error: false,
      ready: false
    };

  if (blockModel.dcpStatus === 0) {
    dcp.pending = true;
  }
  if (blockModel.dcpStatus === -1) {
    dcp.error = true;
  }
  if (blockModel.blockhash === blockModel.dcpBlockhash && !dcp.pending && !dcp.error) {
    dcp.ready = true;
  }
  block.dcp = dcp;

  return block;
}

exports.buildBlockItem = function (models) {
  var blockItem = models.blockItem,
    blockItemResource;

  blockItemResource = buildBlockResource(blockItem);
  return blockItemResource;
};

exports.buildBlockCollection = function(models) {
  var blockCollection = models.blockCollection,
    blockCollectionResource = {                            
      list: [],
      entities: {}
    };

  blockCollection.map(function(block){
    return buildBlockResource(block);
  }).forEach(function (block) {
    blockCollectionResource.list.push(block.id);
    blockCollectionResource.entities[block.id] = block;
  });

  return blockCollectionResource;
};


