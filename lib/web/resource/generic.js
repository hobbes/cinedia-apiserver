'use strict';

module.exports = function ({buildItem, destructItemCreate, destructItemUpdate}) {
  return {
    buildCollection(modelCollection) {
      const result = {
        list: [],
        entities: {}
      };
      modelCollection.map(buildItem)
        .forEach((resource) => {
          result.list.push(resource.id);
          result.entities[resource.id] = resource;
        });

      return result;
    },
    buildItem(modelItem) {
      return buildItem(modelItem);
    },
    destructItemCreate(resourceItem) {
      return destructItemCreate(resourceItem);
    },
    destructItemUpdate(resourceItem) {
      return destructItemUpdate(resourceItem);
    }
  };
};