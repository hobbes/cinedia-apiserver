'use strict';

const createResource = require('./generic');

const paths = [
  '/name/full',
  '/name/short',
  '/type'
];

function buildItem(model) {
  return {
    id: model.userId,
    name: {
      first: model.firstname,
      last: model.lastname
    },
    customer: {
      id: model.customerId
    },
    email: model.email,
    getNotifications: model.getEmails,
    created: model.createdAt,
    version: model.version
  };
}

function destructItemCreate(resource) {
  return {
    firstname: resource.name.first,
    lastname: resource.name.last,
    email: resource.email,
    customerId: resource.customer.id || null,
    getEmails: resource.getNotifications
  };
}

module.exports = createResource({
  buildItem,
  destructItemCreate,
  destructItemUpdate: destructItemCreate
});