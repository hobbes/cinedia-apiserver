'use strict';

var bluebird = require('bluebird'),
  monitor = require('pg-monitor'),
  initOptions = {
    promiseLib: bluebird
  },
  pgp = require('pg-promise')(initOptions),
  config = require('config'),
  db = config.get('db'),
  log = require('../shared/logger'),
  database = pgp({
    host: db.hostname,
    port: db.port,
    database: db.name,
    user: db.username,
    password: db.password,
    min: db.poolmin,
    max: db.poolmax
  });

monitor.setLog(function(msg, info) {
  info.display = false;
  log.debug(msg);
});

monitor.attach(initOptions);

module.exports = database;