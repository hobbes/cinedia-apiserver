'use strict';

var util = require('util'),
  log = require('../shared/logger'),
  queue = require('../shared/queue'),
  jobQueue = queue.jobs,
  resultQueue = queue.results,
  tasks = require('./tasks');

exports.newJob = function (jobname, jobdata, bullQueueJobOptions) {
  return jobQueue.add(jobname, tasks.requests[jobname](jobdata), bullQueueJobOptions)
    .then((job) => {
      log.debug(`job name: ${job.name} id: ${job.id} ` + util.inspect(job.data, {
        depth: null
      }));
      return job.id;
    });
};

resultQueue.process('*', function (job) {
  var additionalResultAction = job.data._additionalResultAction,
    resultType = job.data._error ? 'error' : 'success';

  return tasks.resultHandlers[job.name][resultType](job.data)
    .then(() => {
      job.progress(100);
      if (additionalResultAction) {
        if (tasks.additionalResultActions[additionalResultAction]) {
          return tasks.additionalResultActions[additionalResultAction](job.data);
        } else {
          log.error(`no additionalResultAction with name: ${additionalResultAction}`);
        }
      } else {
        return;
      }
    })
    .catch((error) => {
      log.error(error);
      throw error;
    });
});