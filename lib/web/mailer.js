'use strict';

var Promise = require('bluebird'),
  config = require('config'),
  postmark = require('postmark'),
  client = new postmark.Client(config.get('mailservice.key')),
  name = config.get('name'),
  mailhost = config.get('mailservice.host');

function sendEmail(postmarkOptions) {
  return new Promise(function(resolve, reject) {
    client.sendEmail(postmarkOptions, function(error, success) {
      if (error) {
        return reject(error.message);
      }
      resolve(success);
    });
  });
}

exports.sendGenericEmail = function(options) {
  return sendEmail(options);
};

exports.reportValidationError = function(data) {
  const text = 
`customer:
  name: ${data.customer.name}

sujet:
  id: ${data.sujet.id}
  name: ${data.sujet.name}
  url: ${data.sujet.url}
  sourcefile:
    key: ${data.sujet.sourcefilekey}
`;

  return sendEmail({
    'From': `bot@${mailhost}`, 
    'To': `support@${mailhost}`,
    'Subject': 'error report sujet validation ',
    'TextBody': text,
    'ReplyTo': data.user.email
  });
};

exports.sendLoginMail = function(to, link) {
  return new Promise(function(resolve, reject) {
    var text = 
`login ${name}:

Klicken Sie auf den folgenden Link um sich einzuloggen:

${link}

Sie können den Link auch kopieren und in Ihrem Browser einfügen.

${name}`;

    client.sendEmail({
      'From': `login@${mailhost}`, 
      'To': to, 
      'Subject': `${name} login`, 
      'TextBody': text
    }, function(error, success) {
      if (error) {
        return reject(error);
      }
      resolve(success);
    });
  });
};

exports.sendNewPwMail = function(to, link) {
  return new Promise(function(resolve, reject) {
    var text = `neues Passwort ${name}

Klicken Sie auf den folgenden Link um Ihr neues Passwort zu setzen: ${link}

${name}`;

    client.sendEmail({
      'From': `login@${mailhost}`, 
      'To': to, 
      'Subject': `neues Passwort ${name}`, 
      'TextBody': text
    }, function(error, success) {
      if (error) {
        return reject(error);
      }
      resolve(success);
    });
  });
};

exports.sendMail = function(to, txt) {
  return new Promise(function(resolve, reject) {
    client.sendEmail({
      From: `login@${mailhost}`, 
      To: to, 
      Subject: `${name} login`, 
      TextBody: txt
    }, function(error, success) {
      if (error) {
        return reject(error.message);
      }
      resolve(success);
    });
  });
};

exports.sendEmailBatch = function(to, from, subject, txt) {
  return new Promise(function(resolve, reject) {
    var messages = to.map(function(email) {
      return {
        From: from,
        To: email,
        Subject: subject,
        TextBody: txt,
        ReplyTo: `support@${mailhost}`
      };
    });
    client.sendEmailBatch(messages, function(error, success) {
      if (error) {
        return reject(error.message);
      }
      resolve(success);
    });
  });
};