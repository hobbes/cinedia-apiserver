'use strict';

var   config = require('config'),
  queue = require('./lib/shared/queue'),
  util = require('util'),
  handlers = require('./lib/worker/handlers'),
  log = require('./lib/shared/logger'),
  jobQueue = queue.jobs,
  resultQueue = queue.results,
  cleanUp = handlers.cleanUpOnStartup();

cleanUp.then(() => {
  log.debug(config);
  jobQueue.process('*', (job) => {
    let progress = 0;

    function reportProgress(newProgress) {
      let fixedNewProgress = Number(newProgress.toFixed());
      
      if (fixedNewProgress > progress) {
        progress = fixedNewProgress;
        job.progress(progress);
      }
    }

    function sendResultBack(job, resultdata) {
      resultdata._name = job.name;
      return resultQueue.add(job.name, resultdata)
        .then((resultJob) => {
          log.debug('returning result: ' + resultJob.id + ' ' + util.inspect(resultJob.data, {
            depth: null
          }));
          return resultdata;
        });
    }

    return handlers[job.name](job.data, reportProgress)
      .then((resultdata) => {
        return sendResultBack(job, resultdata);
      })
      .catch((error) => {
        log.error(error);
      });
  });
}).catch(log.error);