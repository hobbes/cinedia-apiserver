'use strict';

var Promise = require('bluebird'),
  moment = require('moment'),
  _ = require('lodash'),
  log = require('../lib/shared/logger'),
  config = require('config'),
  dbStorage = require('../lib/web/models/storage'),
  soStorage = require('../lib/web/storage'),
  getAll_DB_StorageKeys = dbStorage.getAllStorageKeys(),
  sosStorageItemList = soStorage.listAllStorageItems(),
  sosKeyAge = moment.duration(2, 'days'),
  sosKeysList = sosStorageItemList.then((sosItemList) => {
    return sosItemList.reduce(function(result, sosItem) {
      if (moment(sosItem.LastModified).isBefore(moment().subtract(sosKeyAge))) {
        result.push(sosItem.Key);
      }
      return result;
    }, []);
  });

function prompt(question) {
  return new Promise((resolve, reject) => {
    const { stdin, stdout } = process;

    stdin.resume();
    stdout.write(question);

    stdin.on('data', data => resolve(data.toString().trim()));
    stdin.on('error', err => reject(err));
  });
}

Promise.join(getAll_DB_StorageKeys, sosKeysList, function(dbKeys, sosKeys) {
  log.debug(`total ${dbKeys.length} keys in database.`);
  log.debug(`total ${sosKeys.length} keys in object storage.`);
  log.debug('keys from database:');
  log.debug(dbKeys);
  log.debug('keys from object storage:');
  log.debug(sosKeys);

  return _.difference(sosKeys, dbKeys);
}).then((keysToDelete) => {
  log.debug('keys to delete:');
  log.debug(keysToDelete);
  log.debug(`\nFound total ${keysToDelete.length} keys older than ${sosKeyAge.humanize()} to delete from space: ${config.get('objectstorage.bucketname')}`);

  return prompt('delete?\n- type "confirm" without the quotes to proceed.\n- type "no" or ctrl+c to cancel.\n')
    .then((userInput) => {
      if (userInput === 'confirm') {
        log.debug('deleting............');
        return soStorage.deleteFiles(keysToDelete).then(() => {
          log.debug('...deleted!');
        });
      } else {
        return;
      }
    });
}).catch(function (error) {
  log.error(error);
  process.exit(1);
}).finally(function () {
  log.debug('exiting...');
  process.exit(0);
});