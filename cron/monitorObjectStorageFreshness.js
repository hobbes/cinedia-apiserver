'use strict';

var Promise = require('bluebird'),
  moment = require('moment'),
  _ = require('lodash'),
  config = require('config'),
  log = require('../lib/shared/logger'),
  mailer = require('../lib/web/mailer'),
  dbStorage = require('../lib/web/models/storage'),
  soStorage = require('../lib/web/storage'),
  getAll_DB_StorageKeys = dbStorage.getAllStorageKeys(),
  sosStorageItemList = soStorage.listAllStorageItems(),
  sosKeyAge = moment.duration(2, 'days'),
  sosKeysList = sosStorageItemList.then((sosItemList) => {
    return sosItemList.reduce(function(result, sosItem) {
      if (moment(sosItem.LastModified).isBefore(moment().subtract(sosKeyAge))) {
        result.push(sosItem.Key);
      }
      return result;
    }, []);
  }),
  mailhost = config.get('mailservice.host');

log.info('start monitoring storage freshness');
Promise.join(getAll_DB_StorageKeys, sosKeysList, function(dbKeys, sosKeys) {
  const keysToDelete = _.difference(sosKeys, dbKeys),
    amount = keysToDelete.length;

  log.info(`end monitoring storage freshness: ${amount}`);
  return mailer.sendGenericEmail({
    'From': `bot@${mailhost}`, 
    'To': `support@${mailhost}`,
    'Subject': 'storage freshness report',
    'TextBody': `There are ${amount} unused keys older than ${sosKeyAge.humanize()} in the object storage.`
  });
}).catch(function (error) {
  log.error(error);
  return mailer.sendGenericEmail({
    'From': `bot@${mailhost}`, 
    'To': `support@${mailhost}`,
    'Subject': 'error storage freshness monitoring',
    'TextBody': `There was an error during the freshness check. Please check the logs for details.`
  });
}).finally(function () {
  process.exit(0);
});