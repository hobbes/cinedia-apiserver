'use strict';

var log = require('../lib/shared/logger'),
  mailer = require('../lib/web/mailer'),
  block = require('../lib/web/models/block'),
  config = require('config'),
  mailhost = config.get('mailservice.host');

log.info('start remove old dcp');
block.removeOldDcps()
  .then(function (deletedDcps) {
    log.info(`removed ${deletedDcps} dcps`);
    log.info('end remove old dcps');
  })
  .catch(function (error) {
    log.error(error);
    return mailer.sendGenericEmail({
      'From': `bot@${mailhost}`, 
      'To': `support@${mailhost}`,
      'Subject': 'error delete old dcps',
      'TextBody': `There was an error deleting old dcps. Please check the logs for details.`
    }).then(() => {
      process.exit(1);
    });
  })
  .finally(function () {
    process.exit(0);
  });