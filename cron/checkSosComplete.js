'use strict';

var Promise = require('bluebird'),
  _ = require('lodash'),
  log = require('../lib/shared/logger'),
  config = require('config'),
  dbStorage = require('../lib/web/models/storage'),
  soStorage = require('../lib/web/storage'),
  getAll_DB_StorageKeys = dbStorage.getAllStorageKeys(),
  sosStorageItemList = soStorage.listAllStorageItems(),
  sosKeysList = sosStorageItemList.then((sosItemList) => {
    return sosItemList.map((sosItem) => {
      return sosItem.Key;
    });
  });

Promise.join(getAll_DB_StorageKeys, sosKeysList, function(dbKeys, sosKeys) {
  log.info(`${dbKeys.length} keys in DB`);
  log.info(`${sosKeys.length} keys in SOS`);
  return _.difference(dbKeys, sosKeys);
}).then((missingKeysInSos) => {
  if (missingKeysInSos.length > 0) {
    log.info(`Found total ${missingKeysInSos.length} missing keys in ${config.get('objectstorage.bucketname')}`);
    log.info(missingKeysInSos);
  } else {
    log.info('sos complete');
  }
}).catch(function (error) {
  log.error(error);
  process.exit(1);
}).finally(function () {
  log.info('exiting...');
  process.exit(0);
});