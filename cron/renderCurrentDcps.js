'use strict';

var log = require('../lib/shared/logger'),
  mailer = require('../lib/web/mailer'),
  block = require('../lib/web/models/block'),
  config = require('config'),
  mailhost = config.get('mailservice.host');

log.info('start render dcp');

block.getCurrentRenderBlocks()
  .map(function (blockId) {
    log.info('started dcp with blockId: ' + blockId);
    return block.renderBlock(blockId)
      .catch(function (error) {
        log.error(error);
        log.error(error.stack);
      });
  })
  .then(function () {
    log.info('end render dcps');
  })
  .catch(function (error) {
    log.error(error);
    return mailer.sendGenericEmail({
      'From': `bot@${mailhost}`, 
      'To': `support@${mailhost}`,
      'Subject': 'error monitor dcp rendering',
      'TextBody': `There was an error during check if there are any dcp to render.
Please check the logs for details.`
    }).then(() => {
      process.exit(1);
    });
  })
  .finally(function () {
    process.exit(0);
  });