# Cinedia - Automatisierte Werbebuchung fürs Kino - REST Backend

Bei meiner Arbeit als Kinotechniker erstaunte mich der wöchentliche Aufwand, den wir für das Aufbereiten des Bild- und Tonmaterials der Kinowerbung benötigten. Auch das manuelle Einbuchen der Werbespots in die Show-Playlisten verschlang viel Zeit und war fehleranfällig. Eine effizientere und zuverlässigere Lösung musste her.
Mit dieser Motivation entwickelte ich Cinedia. Eine Plattform und Schnittstelle zwischen Werbetreibenden und Kinobetrieben, die den Ablauf der Buchung der Kinowerbung vereinfacht. Als Quereinsteiger eignete ich mir dabei das Wissen vom Programmieren in Javascript in NodeJS über das Design von relationalen Datenbanken in SQL bis hin zu DevOps-Aufgaben wie Deployment und Monitoring an.
Cinedia war von 2017 - 2019 bei den [Arthouse Kinos in Zürich](https://www.arthouse.ch/) und [Bildwurf](https://www.bildwurf.ch/) in einem Testbetrieb im Einsatz und hat sich bestens bewährt.

## Frontened
[https://gitlab.com/hobbes/cinedia-webapp](https://gitlab.com/hobbes/cinedia-webapp)

## Architektur
![Diagramm Architektur cinedia](docs/architektur.png "Architektur cinedia")