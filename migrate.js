'use strict';

const migrationHelper = require('./migrations/helper'),
  log = require('./lib/shared/logger');

migrationHelper.migrateToLatest()
  .then(({from, to}) => {
    if (from === to) {
      log.info(`✔️ nothing to migrate`);
    } else {
      log.info(`✔️ migrated from version ${from} to version: ${to}`);
    }
    process.exit(0); 
  })
  .catch((error) => {
    log.error(`❌ migration failed because of error: ${error}`);
    process.exit(1);    
  });