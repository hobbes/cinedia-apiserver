# config

## db access development
pgcli postgresql://nanobox:{pwd}@172.21.0.4/gonano

## env vars development
NODE_ENV
READY
SOS_KEY
SOS_SECRET
MAIL_KEY

## env vars stage/production
API_HOST
APP_HOST
HASHID_SALT
JWT_SECRET
LOG_LEVEL
MAIL_KEY
NODE_ENV
READY
SOS_KEY
SOS_SECRET
TMP_DIR


## pg_dump
PGPASSWORD=${DATA_POSTGRES_PASS} pg_dump -w -F c -O -U ${DATA_POSTGRES_USER} gonano |
gzip |
curl -T - ftps://cinedia-ftp-01.raluwa.info/backup_${HOSTNAME}_$(date -u +%Y.%m.%d_%H.%M.%S).sql.gz -s -k --disable-epsv --user ftp-01@cinedia.ch:${FTP_BACKUP_PWD01}


## pg_restore
curl ftps://cinedia-ftp-01.raluwa.info/backup/{backup_filename} -s --disable-epsv --insecure --user ftp-01@cinedia.ch:${FTP_BACKUP_PWD01} |
gunzip |
PGPASSWORD=${DATA_POSTGRES_PASS} pg_restore --single-transaction --username=${DATA_POSTGRES_USER} --clean --if-exists --no-password --format=custom --no-owner --dbname=gonano


## s3 backup sync
s3cmd sync --no-delete s3://storage.cinedia/ s3://backup.storage.cinedia/