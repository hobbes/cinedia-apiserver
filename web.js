'use strict';

var migrationHelper = require('./migrations/helper'),
  express = require('express'),
  expressEnforcesSsl = require('express-enforces-ssl'),
  helmet = require('helmet'),
  bodyParser = require('body-parser'),
  compress = require('compression'),
  corser = require('corser'),
  cookieParser = require('cookie-parser'),
  config = require('config'),
  apphost = config.get('apphost'),
  log = require('./lib/shared/logger'),
  maintenance = require('./lib/web/middleware/maintenance'),
  origins = [apphost, 'http://localhost:8081'],
  app = express();

app.disable('x-powered-by');
app.enable('trust proxy');
app.use(helmet());
if (config.get('env') === 'production' || config.get('env') === 'stage') {
  app.use(expressEnforcesSsl());
}
if (config.get('loglevel') === 'info') {
  app.use(require('morgan')('combined'));
}
if (config.get('loglevel') === 'debug') {
  app.use(require('morgan')('dev'));
}
app.use(maintenance.check);
app.use(cookieParser());
app.use(compress());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(corser.create({
  origins: origins,
  requestHeaders: corser.simpleRequestHeaders.concat(['X-Requested-With']),
  methods: corser.simpleMethods.concat(['PUT', 'DELETE', 'PATCH']),
  supportsCredentials: true
}));
app.use('/', require('./lib/web/router/api'));

migrationHelper.verifySchemaVersion()
  .then((versions) => {
    log.debug(`DB Version of code: ${versions.code}`);
    log.debug(`DB Version of DB schema: ${versions.db}`);
    app.listen(8080, '0.0.0.0', function () {
      log.debug('db schema up to date');
      log.debug(config);
      log.info('listening');
    });
  })
  .catch((error) => {
    log.error(error);
    process.exit(0);
  });