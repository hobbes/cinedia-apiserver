# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [10.1.0] - 2018-09-23
### Added
- replacement of sourcefile of sujets is now possible all the time (for admins)
- Support for png images. Transparency will be placed on a black background.

## [10.0.0] - 2018-09-13
### Added
- Initial deployment for betatest.